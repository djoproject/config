#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# is gsettings installed ?
if [[ -z "$(command -v gsettings)" ]]; then
    echo "WARNING gsettings not present.  skip."
    return 0
fi

SCHEMA="org.mate.pluma"
if ! gsettings list-schemas | grep -q ^$SCHEMA$ ; then
    echo "WARNING schema $SCHEMA does not exist.  skip."
    return 0
fi

while IFS= read -r line; do
    IFS=' '; read -ra splitted_line <<< "$line"; unset IFS;
    key="${splitted_line[0]}"
    value="${splitted_line[1]}"

    if ! original_value=$(gsettings get "$SCHEMA" "$key" 2> /dev/null); then
        echo "WARNING: unknown key '$key' for schame '$SCHEMA'"
        continue
    fi

    if [[ "$original_value" = "$value" ]]; then
        continue
    fi

    if ! gsettings set "$SCHEMA" "$key" "$value"; then
        echo "ERROR: failed to set key '$key' for schame '$SCHEMA'"
        return 1
    fi

    echo "INFO: key '$key' set for schame '$SCHEMA'."
done < "./settings"
