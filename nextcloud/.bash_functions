#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function _nc_ask_url {
    if [[ -z "$_NEXTCLOUD_URL" ]]; then
        local url
        read -rp 'nextcloud url: ' url
        echo "$url"
    else
        echo "$_NEXTCLOUD_URL"
    fi
}


function _nc_ask_user {
    if [[ -z "$_NEXTCLOUD_USER" ]]; then
        local user
        read -rp 'nextcloud user: ' user
        echo "$user"
    else
        echo "$_NEXTCLOUD_USER"
    fi
}


function _nc_ask_password {
    if [[ -n "$(command -v pass)" ]] && [[ $(pass find nextcloud | wc -l) -eq 2 ]]; then
        local password
        password=$(pass nextcloud)
        echo "$password"
    fi

    echo ""
}


function nc_mkdir {
    #
    #   Create a distant directory
    #   Arg 1 (mandatory): distant directory path
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "no directory name provided."
        return 1
    fi

    local directory url user password
    directory="$1"
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    local output_file
    output_file=$(mktemp)
    curl -sS -o "$output_file" -u "$user_string" -X MKCOL "$url/remote.php/dav/files/$user/$directory"

    if [[ -n "$(command -v nc_check_error)" ]]; then
        nc_check_error "$output_file"
    else
        cat "$output_file"
    fi

    if [[ -f "$output_file" ]]; then
        rm "$output_file"
    fi
}


function nc_put {
    #
    #   Upload a local file
    #   Arg 1 (mandatory): local file path
    #   Arg 2 (optional): distant directory, default is root directory
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "no local path provided."
        return 1
    fi

    local file_path file_name destination_path
    file_path="$1"
    file_name=$(basename "$1")

    if [[ -n "$2" ]]; then
        destination_path="$2/$file_name"
    else
        destination_path="$file_name"
    fi

    local url user password
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    local output_file
    output_file=$(mktemp)
    curl -sS -o "$output_file" -u "$user_string" -T "$file_path" "$url/remote.php/dav/files/$user/$destination_path"

    if [[ -n "$(command -v nc_check_error)" ]]; then
        nc_check_error "$output_file"
    else
        cat "$output_file"
    fi

    if [[ -f "$output_file" ]]; then
        rm "$output_file"
    fi
}


function nc_get {
    #
    #   Download a distant file
    #   Arg 1 (mandatory): distant file path
    #   Arg 2 (optional): local directory path
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "no distant path provided."
        return 1
    fi

    local distant_file_path base_name curl_option output_file
    distant_file_path="$1"
    base_name=$(basename "$distant_file_path")

    if [[ -n "$2" ]]; then
        if [[ ! ( -d "$2" ) ]]; then
            echo "ERROR: destination is not a directory" >&2
            return 1
        fi

        if [[ ! ( -w "$2" ) ]]; then
            echo "ERROR: no write permission on destination directory" >&2
            return 1
        fi

        curl_option="-o $2/$base_name"
        output_file="$2/$base_name"
    else
        curl_option="-O"
        output_file="$base_name"
    fi

    local url user password
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    curl -sS "$curl_option" -u "$user_string" "$url/remote.php/dav/files/$user/$distant_file_path"

    # manage output/error.  It will be stored into the output file.
    if [[ -n "$(command -v nc_check_error)" ]]; then
        nc_check_error "$output_file"
    fi
}


function nc_mv {
    #
    #   List a file or a directory
    #   Arg 1 (mandatory): distant source path
    #   Arg 2 (mandatory): distant destination path
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "no distant source path provided."
        return 1
    fi

    if [[ -z "$2" ]]; then
        echo "no distant destination path provided."
        return 1
    fi

    local url user password
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    local source_path destination_path output_file
    source_path="$url/remote.php/dav/files/$user/$1"
    destination_path="$url/remote.php/dav/files/$user/$2"
    output_file=$(mktemp)

    curl -sS -o "$output_file" -u "$user_string" -X MOVE --header "Destination: $destination_path" "$source_path"

    if [[ -n "$(command -v nc_check_error)" ]]; then
        nc_check_error "$output_file"
    else
        cat "$output_file"
    fi

    if [[ -f "$output_file" ]]; then
        rm "$output_file"
    fi
}


function nc_ls {
    #
    #   List a directory
    #   Arg 1 (optional): distant path to list, default is root directory
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    local distant_path url user password
    distant_path="$1"  # could be empty
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    local output_file
    output_file=$(mktemp)
    curl -sS -o "$output_file" -X PROPFIND -H "Depth: 1" -u "$user_string" "$url/remote.php/dav/files/$user/$distant_path"

    if [[ -n "$(command -v nc_print_ls)" ]]; then
        nc_print_ls "$output_file" "$distant_path"
    else
        cat "$output_file"
    fi

    if [[ -f "$output_file" ]]; then
        rm "$output_file"
    fi
}


function nc_rm {
    #
    #   Remove a file or a directory
    #   Arg 1 (mandatory): distant path to remove
    #

    if [[ -z "$(command -v curl)" ]]; then
        echo "WARNING: curl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "no distant path provided."
        return 1
    fi

    local distant_path url user password
    distant_path="$1"
    url=$(_nc_ask_url)
    user=$(_nc_ask_user)
    password=$(_nc_ask_password)

    local user_string
    if [[ -z "$password" ]]; then
        user_string="$user"
    else
        user_string="$user:$password"
    fi

    local output_file
    output_file=$(mktemp)
    curl -sS -o "$output_file" -X DELETE -u "$user_string" "$url/remote.php/dav/files/$user/$distant_path"

    if [[ -n "$(command -v nc_check_error)" ]]; then
        nc_check_error "$output_file"
    else
        cat "$output_file"
    fi

    if [[ -f "$output_file" ]]; then
        rm "$output_file"
    fi
}
alias nc_rmdir="nc_rm"

# TODO manage share
# https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/ocs-share-api.html#local-shares
