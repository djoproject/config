#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function pass_cipher {
    if [[ -z "$(command -v pass)" ]]; then
        echo "WARNING: pass does not seem to be installed"
        return 1
    fi

    local working_dir gpg_id_path

    if [[ -f ".gpg-id" ]]; then
        working_dir=.
        gpg_id_path=./.gpg-id
    elif [[ -n "$PASSWORD_STORE_DIR" ]]; then
        working_dir="$PASSWORD_STORE_DIR"
        gpg_id_path="$PASSWORD_STORE_DIR/.gpg-id"
    elif [[ -d "$HOME/.password-store" ]]; then
        working_dir="$HOME/.password-store"
        gpg_id_path="$HOME/.password-store/.gpg-id"
    else
        echo "ERROR: no password-store directory found." >&2
        return 1
    fi

    if [[ ! ( -f "$gpg_id_path" ) ]]; then
        echo "ERROR: .gpg-id not found in directory $working_dir" >&2
        return 1
    fi

    local ids
    ids=$(cat "$gpg_id_path")
    PASSWORD_STORE_DIR=$working_dir pass init "$ids"
}


function pass_insert {
    if [[ -z "$(command -v pass)" ]]; then
        echo "WARNING: pass does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v pwgen)" ]]; then
        echo "WARNING: pwgen does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "ERROR: no password name provided" >&2
        return 1
    fi

    local pass_path new_password login_name master_directory
    pass_path="$1"
    login_name="$2"
    master_directory="$(dirname "$pass_path")"

    if [[ "$master_directory" = "." ]]; then
        echo "ERROR: inserting in password store root directory is not allowed." >&2
        return 1
    fi

    # TODO create a mail alias

    new_password="$(pwgen -c -y -s 16 1)"

    local login_string
    if [[ -n "$login_name" ]]; then
        login_string="login: $login_name"
    fi

    pass insert -m "$pass_path" << EOF
$new_password
$login_string
EOF

    pass -c "$pass_path"
}


function pass_generate {
    if [[ -z "$(command -v pwgen)" ]]; then
        echo "WARNING: pwgen does not seem to be installed"
        return 1
    fi

    pwgen -c -y -s 16 1
}


function _compute_pass_list_path {
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        echo "$CUSTOM_CONFIG_DIRECTORY/settings/custom_pass_list"
    else
        echo "$HOME/.custom_pass_list"
    fi
}


function  _build_pass_list {
    local list_path default_pass_path selected_pass

    list_path="$(_compute_pass_list_path)"
    default_pass_path="$HOME/.password-store"
    selected_pass="$PASSWORD_STORE_DIR"

    # collect path from list
    declare -A path_list
    if [[ -f "$list_path" ]]; then
        local pass_path
        while IFS= read -r pass_path; do
            if ! [[ -d "$pass_path" ]]; then
                continue
            fi

            path_list["$pass_path"]=1
        done < "$list_path"
    fi

    if [[ -n "$selected_pass" ]]; then
        path_list["$selected_pass"]=1
    fi

    if [[ -d "$default_pass_path" ]]; then
        path_list["$default_pass_path"]=1

        if [[ -z "$selected_pass" ]]; then
            selected_pass="$default_pass_path"
        fi
    fi

    unset _PASS_PATH_LIST
    declare -ga _PASS_PATH_LIST

    while read -r path; do
        _PASS_PATH_LIST+=("$path")
    done < <(IFS=$'\n'; sort <<<"${!path_list[*]}"; unset IFS;)
}


function lspass {
    local selected_pass default_pass_path
    selected_pass="$PASSWORD_STORE_DIR"
    default_pass_path="$HOME/.password-store"

     _build_pass_list

    if [[ -z "$selected_pass" ]] && [[ -d "$default_pass_path" ]]; then
        selected_pass="$default_pass_path"
    fi

    local path
    for path in "${_PASS_PATH_LIST[@]}"; do
        if [[ "$selected_pass" = "$path" ]]; then
            echo "> $path"
        else
            echo "  $path"
        fi
    done
}


function pass_register {
    if [[ $# -eq 0 ]]; then
        echo "ERROR: need a path" >&2
        return 1
    fi

    local pass_path
    pass_path="$1"

    if ! [[ -d "$pass_path" ]]; then
        echo "ERROR: path does not exist or is not a directory: $pass_path" >&2
        return 1
    fi

    local default_pass_path
    default_pass_path="$HOME/.password-store"

    if [[ "$pass_path" = "$default_pass_path" ]]; then
        echo "WARNING: useless to register default path, aborting."
        return 0
    fi

     _build_pass_list

    local path
    for path in "${_PASS_PATH_LIST[@]}"; do
        if [[ "$path" = "$pass_path" ]]; then
            echo "WARNING: path is already registered, aborting."
            return 0
        fi
    done

    local list_path
    list_path="$(_compute_pass_list_path)"
    echo "$pass_path" >> "$list_path"
}


function pass_unregister {
    if [[ $# -eq 0 ]]; then
        echo "ERROR: need an index" >&2
        return 1
    fi

    if ! [[ "$1" =~ ^[0-9]*$ ]]; then
        echo "ERROR: index is not an integer." >&2
        return 1
    fi

    local index
    index="$1"

    _build_pass_list

    if [[ $index -ge ${#_PASS_PATH_LIST[@]} ]]; then
        echo "ERROR: index is out of range" >&2
        return 1
    fi

    local default_pass_path pass_path
    default_pass_path="$HOME/.password-store"
    pass_path="${_PASS_PATH_LIST[$index]}"


    if [[ "$pass_path" = "$default_pass_path" ]]; then
        echo "WARNING: useless to unregister default path, aborting."
        return 0
    fi

    # remove it from the list
    local list_path
    list_path="$(_compute_pass_list_path)"
      sed -i '\|^'"$pass_path"'$|d' "$list_path"

    # was it the path set ? if yes unset it
    if [[ "$pass_path" = "$PASSWORD_STORE_DIR" ]]; then
        local setting_path
        if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
            setting_path="$CUSTOM_CONFIG_DIRECTORY/settings/password_store_dir"
        else
            setting_path="$HOME/.password_store_dir"
        fi

        # remove variable for this terminal
        unset PASSWORD_STORE_DIR

        # remove permanent support for any new terminals
        if [[ -f "$setting_path" ]]; then
            rm -f "$setting_path"
        fi
    fi
}

function pass_set {
    if [[ $# -eq 0 ]]; then
        echo "ERROR: need an index" >&2
        return 1
    fi

    if ! [[ "$1" =~ ^[0-9]*$ ]]; then
        echo "ERROR: index is not an integer." >&2
        return 1
    fi

    local index
    index="$1"

    _build_pass_list

    if [[ $index -ge ${#_PASS_PATH_LIST[@]} ]]; then
        echo "ERROR: index is out of range" >&2
        return 1
    fi

    local default_pass_path  path_to_set
    default_pass_path="$HOME/.password-store"
    path_to_set="${_PASS_PATH_LIST[$index]}"

    # manage env + permanent
    local setting_path setting_parent_path

    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        setting_parent_path="$CUSTOM_CONFIG_DIRECTORY/settings"
        setting_path="$setting_parent_path/password_store_dir"
    else
        setting_parent_path="$HOME"
        setting_path="$setting_parent_path/.password_store_dir"
    fi

    if [[ "$path_to_set" = "$default_pass_path" ]]; then
        # remove variable for this terminal
        unset PASSWORD_STORE_DIR

        # remove permanent support for any new terminals
        if [[ -f "$setting_path" ]]; then
            rm -f "$setting_path"
        fi
    else
        # export variable for this terminal
        export PASSWORD_STORE_DIR
        PASSWORD_STORE_DIR="$path_to_set"

        # save variable for new terminals
        mkdir -p "$setting_parent_path"
        echo "$PASSWORD_STORE_DIR" > "$setting_path"
    fi
}
