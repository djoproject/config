#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CHECK_GPG_VERSION='gpg \(GnuPG\) ([0-9\.]*)'

function clean_then_exit {
    if [[ -z "$working_dir" ]]; then
        return
    fi

    if [[ ! ( -d "$working_dir" ) ]]; then
        return
    fi

    echo "### remove temporary directory ###"

    # remove tmp socket directory
    gpgconf --homedir "$working_dir" --remove-socketdir

    # remove the temporary keyring directory
    rm -rf "$working_dir" && echo "    DONE"
    echo
    exit 0
}


if [[ ! ( -f ./.gpg-id ) ]]; then
    echo "file .gpg-id does not exist, you must be in a pass directory to execute this script"
    exit 1
fi

### checking gpg binnary ######################################################

if [[ -n "$(command -v gpg2)" ]]; then
    gpg_exec=gpg2
elif [[ -n "$(command -v gpg)" ]]; then
    gpg_exec=gpg
else
    echo "GPG not installed on this system" >&2
    exit 1
fi

if [[ -z "$(command -v gpgconf)" ]]; then
    echo "WARNING: gpgconf does not seem to be installed"
    return 1
fi

if [[ $($gpg_exec --version) =~ $CHECK_GPG_VERSION ]]; then
    gpg_version=${BASH_REMATCH[1]}

    if [[ "${gpg_version:0:1}" != "2" ]]; then
        echo "this tool is design to work with GPG version 2, version found: $gpg_version"
        exit 1
    fi

else
    echo "Unknown GPG version"
    exit 1
fi

### MAIN ######################################################################

trap clean_then_exit EXIT

working_dir=$(mktemp -d --suffix _pass_test_files)

if [[ ! ( -d $working_dir ) ]]; then
    echo "failed to create temporary directory $working_dir"
    exit 1
fi

chmod 700 "$working_dir"

# creating the gpg data

if [[ -f ./.pubkeys.pgp ]]; then
    PUB_KEY_PATH=./.pubkeys.pgp
else
    echo "INFO: no .pubkeys.pgp file found, use local keyring instead."
    PUB_KEY_PATH="$working_dir/exported_pubkeys.pgp"
    $gpg_exec --export --output "$PUB_KEY_PATH"
fi

if [[ -f ./.trust.pgp ]]; then
    TRUST_PATH=./.trust.pgp
else
    echo "INFO: no .trust.pgp file found, use local trustdb instead."
    TRUST_PATH="$working_dir/exported_trust.pgp"
    $gpg_exec --export-ownertrust > "$TRUST_PATH"
fi

GNUPGHOME=$working_dir $gpg_exec --import < "$PUB_KEY_PATH" > /dev/null 2>&1
GNUPGHOME=$working_dir $gpg_exec --import-ownertrust < "$TRUST_PATH" > /dev/null 2>&1

# the following call has no real impact but it allow to clean error/warning
# streams in gpg, and be sure we'll be able to trust return value of the next
# calls
GNUPGHOME=$working_dir $gpg_exec -k > /dev/null 2>&1

# convert .gpg-id into a fingerprint list.
declare -a mandatory_keys

# howto read a file
# src: https://www.cyberciti.biz/faq/unix-howto-read-line-by-line-from-file/
while IFS= read -r user_id; do
    mapfile -t gpg_output < <(GNUPGHOME=$working_dir $gpg_exec --fingerprint --with-colons -k "$user_id")

    for line in "${gpg_output[@]}"; do
        line_type=$(echo "$line" | awk -F: '{print $1;}')

        if [[ "$line_type" != "fpr" ]]; then
            continue
        fi

        # only retains the first fingerprint, the one linked to the master
        # public key.
        first_fingerprint=$(echo "$line" | awk -F: '{print $10;}')
        mandatory_keys+=("$first_fingerprint")
        break
    done
done < ./.gpg-id

echo "INFO: keys to find: ${#mandatory_keys[@]}"
echo

# load used keys

problem_found=0

while read -r file_path; do
    echo "$file_path"

    key_count=0
    unknown_key_count=0
    declare -a master_key_list

    # collect keys used to encrypt the files
    while read -r sub_key_id; do
        # echo $sub_key_id
        key_count=$((key_count+1))

        if ! GNUPGHOME=$working_dir $gpg_exec -k "$sub_key_id" 2> /dev/null 1> /dev/null; then
            # XXX strangely, this could print a short version of the fingerprint.
            echo "    unknown key $sub_key_id used to encrypt"
            unknown_key_count=$((unknown_key_count+1))
            continue
        fi

        # get the master key fingerprint
        master_key_fingerprint=$(GNUPGHOME=$working_dir $gpg_exec --fingerprint --with-colons -k "$sub_key_id" | grep -m1 fpr | awk -F: '{print $10;}')
        master_key_list+=("$master_key_fingerprint")

    done < <(GNUPGHOME=$working_dir $gpg_exec --status-fd 1 --logger-fd 1 --attribute-fd 1 --list-packets --verbose "$file_path" | grep "keyid" | awk '{print $NF}')

    # check missing keys
    missing_key_count=0
    for needed_key in "${mandatory_keys[@]}"; do
        found=0
        for provided_key in "${master_key_list[@]}"; do
            if [[ "$needed_key" == "$provided_key" ]]; then
                found=1
                break
            fi
        done

        if [[ $found -eq 0 ]]; then
            echo "    missing key $needed_key"
            missing_key_count=$((missing_key_count+1))
            
        fi
    done

    problem_found=$((problem_found+unknown_key_count))
    problem_found=$((problem_found+missing_key_count))
    key_issue=$((unknown_key_count+missing_key_count))

    if [[ $key_issue -eq 0 ]]; then
        continue
    fi

    echo
    #echo "    key count: $key_count"
    echo "    unknown key count: $unknown_key_count"
    echo "    missing key count: $missing_key_count"
    echo ""
done < <(find . -name \*.gpg)

echo
echo "####################################"
echo "### TOTAL PROBLEM FOUND: $problem_found"
echo "####################################"
echo

if [[ $problem_found -eq 0 ]]; then
    exit 0
fi

exit 1
