#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

install_bin pass_test_config "" "Test configuration files of a password-store directory"
install_bin pass_test_files "" "Test password files of a password-store directory"

describe_function "pass_cipher" "Cipher every files existing into a password store with keys defined in configuration"
describe_function "pass_insert" "Insert a new element into password store and copy the generated password to clipboard"
describe_function "pass_generate" "Generate a password the same way as pass_insert but does not insert it"
describe_function "lspass" "List password-store registered"
describe_function "pass_register" "Register a password-store"
describe_function "pass_unregister" "Unregister a password-store"
describe_function "pass_set" "Set a registered password-store"

host_file="$HOME/.mozilla/native-messaging-hosts/passff.py"
if ! [[ -f "$host_file" ]]; then
    echo "WARNING: $host_file does not exist, skip patching."
    return 0
fi

if grep -q "^    # Patch to use custom config.$" "$host_file"; then
    # already patched
    return 0
fi

if ! raw_line=$(grep -n "^    # Set up subprocess params$" "$host_file"); then
    echo "ERROR: didn't find the marker in file to patch" >&2
    return 1
fi

line_number=$(echo "$raw_line" | head -n 1 | cut -d: -f1)
line_number=$((line_number-1))

sed -i "${line_number}a\
    # Patch to use custom config.
    path = os.path.join(env[\"HOME\"], \".custom_config/settings/password_store_dir\")
    if not os.path.exists(path):
        path = os.path.join(env[\"HOME\"], \".password_store_dir\")

        if not os.path.exists(path):
            path = None

    if path is not None:
        with open(path, 'r') as f:
            pass_path = f.read().strip()

        env[\"PASSWORD_STORE_DIR\"] = pass_path

" < "$host_file"
