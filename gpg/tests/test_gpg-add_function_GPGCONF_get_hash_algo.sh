#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"


function test_default {
    MISC_vercomp "$GPG_VERSION" "2.3.2"
    if [[ $? -eq 2 ]]; then
        [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]
    else
        [[ "$(GPGCONF_get_hash_algo)" = "sha256" ]]
    fi
}


function test_sha1 {
    echo "ssh-fingerprint-digest sha1" >> "$GNUPGHOME/gpg-agent.conf"

    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        if [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]; then
            SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
            export SKIP_REASON
            return 255
        else
            return 1
        fi
    fi

    [[ "$(GPGCONF_get_hash_algo)" = "sha1" ]]
}


function test_sha256 {
    echo "ssh-fingerprint-digest sha256" >> "$GNUPGHOME/gpg-agent.conf"

    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        if [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]; then
            SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
            export SKIP_REASON
            return 255
        else
            return 1
        fi
    fi

    [[ "$(GPGCONF_get_hash_algo)" = "sha256" ]]
}


function test_sha384 {
    echo "ssh-fingerprint-digest sha256" >> "$GNUPGHOME/gpg-agent.conf"

    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        if [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]; then
            SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
            export SKIP_REASON
            return 255
        else
            return 1
        fi
    fi

    [[ "$(GPGCONF_get_hash_algo)" = "sha256" ]]
}


function test_sha512 {
    echo "ssh-fingerprint-digest sha256" >> "$GNUPGHOME/gpg-agent.conf"

    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        if [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]; then
            SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
            export SKIP_REASON
            return 255
        else
            return 1
        fi
    fi

    [[ "$(GPGCONF_get_hash_algo)" = "sha256" ]]
}


function test_invalid {
    echo "ssh-fingerprint-digest toto" >> "$GNUPGHOME/gpg-agent.conf"
    [[ "$(GPGCONF_get_hash_algo)" = "md5" ]]
}


function main {
    local return_code=0

    utest "test_default" || return_code=1
    utest "test_sha1" || return_code=1
    utest "test_sha256" || return_code=1
    utest "test_sha384" || return_code=1
    utest "test_sha512" || return_code=1
    utest "test_invalid" || return_code=1

    return $return_code
}

main
