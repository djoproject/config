#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/basic.setup.functions.sh"

##############################################################################

function test_without_args {
    if ! result=$(GPG_get_algo_name_from_algo_id); then
        if [[ "$result" != "UNKN" ]]; then
            return 2
        fi

        return 0
    fi

    return 1
}


function test_with_empty_arg {
    if ! result=$(GPG_get_algo_name_from_algo_id ""); then
        if [[ "$result" != "UNKN" ]]; then
            return 2
        fi

        return 0
    fi

    return 1
}


function test1 {
    if ! result=$(GPG_get_algo_name_from_algo_id "1"); then
        return 1
    fi

    [[ "$result" = "RSA" ]]
}


function test2 {
    if ! result=$(GPG_get_algo_name_from_algo_id "2"); then
        return 1
    fi

    [[ "$result" = "DSA" ]]
}


function test3 {
    if ! result=$(GPG_get_algo_name_from_algo_id "3"); then
        return 1
    fi

    [[ "$result" = "DSA" ]]
}


function test4 {
    if ! result=$(GPG_get_algo_name_from_algo_id "4"); then
        return 1
    fi

    [[ "$result" = "RSA" ]]
}


function test5 {
    if ! result=$(GPG_get_algo_name_from_algo_id "5"); then
        return 1
    fi

    [[ "$result" = "ELG" ]]
}


function test6 {
    if ! result=$(GPG_get_algo_name_from_algo_id "6"); then
        return 1
    fi

    [[ "$result" = "RSA" ]]
}


function test7 {
    if ! result=$(GPG_get_algo_name_from_algo_id "7"); then
        return 1
    fi

    [[ "$result" = "DSA" ]]
}


function test8 {
    if ! result=$(GPG_get_algo_name_from_algo_id "8"); then
        return 1
    fi

    [[ "$result" = "RSA" ]]
}


function test9 {
    if ! result=$(GPG_get_algo_name_from_algo_id "9"); then
        return 1
    fi

    [[ "$result" = "ECC" ]]
}


function test10 {
    if ! result=$(GPG_get_algo_name_from_algo_id "10"); then
        return 1
    fi

    [[ "$result" = "ECC" ]]
}


function test11 {
    if ! result=$(GPG_get_algo_name_from_algo_id "11"); then
        return 1
    fi

    [[ "$result" = "ECC" ]]
}


function test12 {
    if ! result=$(GPG_get_algo_name_from_algo_id "12"); then
        return 1
    fi

    [[ "$result" = "ECC" ]]
}


function test13 {
    if ! result=$(GPG_get_algo_name_from_algo_id "13"); then
        return 1
    fi

    [[ "$result" = "EXIST" ]]
}


function test14 {
    if ! result=$(GPG_get_algo_name_from_algo_id "14"); then
        return 1
    fi

    [[ "$result" = "CARD" ]]
}


function test15 {
    if result=$(GPG_get_algo_name_from_algo_id "15"); then
        return 1
    fi

    [[ "$result" = "UNKN" ]]
}



function main {
    local return_code=0

    utest "test_without_args" || return_code=1
    utest "test_with_empty_arg" || return_code=1
    utest "test1" || return_code=1
    utest "test2" || return_code=1
    utest "test3" || return_code=1
    utest "test4" || return_code=1
    utest "test5" || return_code=1
    utest "test6" || return_code=1
    utest "test7" || return_code=1
    utest "test8" || return_code=1
    utest "test9" || return_code=1
    utest "test10" || return_code=1
    utest "test11" || return_code=1
    utest "test12" || return_code=1
    utest "test13" || return_code=1
    utest "test14" || return_code=1
    utest "test15" || return_code=1

    return $return_code
}

main
