#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function setup {
    # compute gpg binary
    if [[ -n "$(command -v gpg2)" ]]; then
        gpg_exec=gpg2
        export gpg_exec
    elif [[ -n "$(command -v gpg)" ]]; then
        gpg_exec=gpg
        export gpg_exec
    else
        echo "ERROR: GPG not installed on this system" >&2
        exit 1
    fi

    # compute GPG version
    local gpg_version_array
    mapfile -t gpg_version_array < <($gpg_exec --version)
    IFS=' '; read -ra gpg_version_array <<< "${gpg_version_array[0]}"; unset IFS;
    GPG_VERSION=${gpg_version_array[2]}
    export GPG_VERSION

    if ! [[ -d "$CURRENT_PATH/data/keyring1" ]]; then
        if ! ./data/create_keyring1.sh; then
            echo "ERROR: failled to generate keyring1." >&2
            exit 1
        fi
        mv keyring1 data
    elif [[ -f "$CURRENT_PATH/data/keyring1/version" ]]; then
        test_gpg_version="$(cat "$CURRENT_PATH/data/keyring1/version")"

        if [[ "$test_gpg_version" != "$GPG_VERSION" ]]; then
            echo "ERROR: gpg version used to create keying does not match." >&2
            exit 1
        fi
    fi

    if ! [[ -d "$CURRENT_PATH/data/keyring2" ]]; then
        if ! ./data/create_keyring2.sh; then
             echo "ERROR: failled to generate keyring2." >&2
             exit 1
        fi
        mv keyring2 data
    elif [[ -f "$CURRENT_PATH/data/keyring2/version" ]]; then
        test_gpg_version="$(cat "$CURRENT_PATH/data/keyring2/version")"

        if [[ "$test_gpg_version" != "$GPG_VERSION" ]]; then
            echo "ERROR: gpg version used to create keying does not match." >&2
            exit 1
        fi
    fi

    gpgadd_path="$CURRENT_PATH/../gpg-add"

    if [[ ! ( -f "$gpgadd_path" ) ]]; then
        echo "ERROR: didn't find gpg-add file." >&2
        exit 1
    fi

    GNUPGHOME=$(mktemp -d --suffix _gpg_sand_box)

    if [[ -n "$GPG_TEST_KEYRING_PATH" ]] && [[ -d "$GPG_TEST_KEYRING_PATH" ]]; then
        cp -r "$GPG_TEST_KEYRING_PATH/"* "$GNUPGHOME"
    else
        cp -r "$CURRENT_PATH/data/keyring1/"* "$GNUPGHOME"
    fi

    chmod 700 "$GNUPGHOME"
    export GNUPGHOME

    # init keyring
    $gpg_exec -k > /dev/null 2>&1

    # initialize socket directory
    gpgconf --create-socketdir 2> /dev/null

    # kickstart gpg-agent
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    # source gpg-add with null op
    # shellcheck source=/dev/null
    source "$gpgadd_path" -n

    if [[ -f "$GNUPGHOME/variables.sh" ]]; then
        # shellcheck disable=SC1090,SC1091
        source "$GNUPGHOME/variables.sh"
    fi
}


function teardown {
    # remove socket directory
    if ! gpgconf --homedir "$GNUPGHOME" --remove-socketdir 2> /dev/null; then
        gpg-connect-agent --homedir "$GNUPGHOME" "KILLAGENT" /bye > /dev/null 2>&1
    fi

    rm -rf "$GNUPGHOME"

    unset GNUPGHOME
}
