#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

############################################################################## 

function missing_args {
    ! GPG_get_primary_key_fingerprint_for_userid 2> /dev/null
}


function empty_args {
    ! GPG_get_primary_key_fingerprint_for_userid "" 2> /dev/null
}


function unkown_userid {
    GPG_get_primary_key_fingerprint_for_userid "ploooop"
    [[ $? -eq 2 ]]
}


function too_vague_userid {
    ! GPG_get_primary_key_fingerprint_for_userid "test.com" 2> /dev/null
}


function success {
    local data
    if ! data=$(GPG_get_primary_key_fingerprint_for_userid "test@test.com"); then
        return 3
    fi

    [[ "$data" = "$MASTER_KEY_1_FGP" ]]
}


function main {
    local return_code=0

    utest "missing_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "unkown_userid" || return_code=1
    utest "too_vague_userid" || return_code=1
    utest "success" || return_code=1

    return $return_code
}

main
