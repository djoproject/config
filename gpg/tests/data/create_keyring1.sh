#!/usr/bin/env bash

function refresh_data {
    unset fingerprints
    unset keygrips
    declare -ga fingerprints
    declare -ga keygrips

    while read -r line; do
        IFS=':'; read -ra parsed_line <<< "$line"; unset IFS;
        # Field 1 - Type of record
        record_type=${parsed_line[0]}

        if [[ "$record_type" = "fpr" ]]; then
            # Field 10 - User-ID
            value=${parsed_line[9]}
            fingerprints+=("$value")
        elif [[ "$record_type" = "grp" ]]; then
            # Field 10 - User-ID (keygrip in this case)
            value=${parsed_line[9]}
            keygrips+=("$value")
        fi
    done < <($gpg_exec -K --with-colons --with-fingerprint --with-fingerprint --with-keygrip)
}


if [[ -n "$(command -v gpg2)" ]]; then
    gpg_exec="gpg2 --no-tty"
elif [[ -n "$(command -v gpg)" ]]; then
    gpg_exec="gpg --no-tty"
else
    echo "ERROR: GPG not installed on this system" >&2
    exit 1
fi

# Compute current version
mapfile -t gpg_version_array < <($gpg_exec --version)
IFS=' '; read -ra gpg_version_array <<< "${gpg_version_array[0]}"; unset IFS;
gpg_version=${gpg_version_array[2]}

GNUPGHOME="./keyring1"

if [[ -d "$GNUPGHOME" ]]; then
    echo "WARNING: directory $GNUPGHOME already exists."
    exit 0
fi

if ! mkdir "$GNUPGHOME"; then
    echo "ERROR: failed to create $GNUPGHOME directory." >&2
    exit 1
fi


echo "#### Creation of keyring 1..."

chmod 700 "$GNUPGHOME"
export GNUPGHOME

echo "$gpg_version" > "$GNUPGHOME/version"
echo "allow-loopback-pinentry" >> "$GNUPGHOME/gpg-agent.conf"
echo "enable-ssh-support" >> "$GNUPGHOME/gpg-agent.conf"

echo "##### init keyring"
$gpg_exec -k > /dev/null 2>&1

echo "#####create first key"
$gpg_exec --batch --gen-key <<EOF
%no-protection
Key-Type:1
Key-Length:1024
Key-Usage:sign
Subkey-Type:1
Subkey-Usage:auth
Subkey-Length:1024
Name-Real: test@test.com
Name-Comment: test@test.com
Name-Email: test@test.com
Expire-Date:0
EOF

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to generate key (1)" >&2
    exit 1
fi

refresh_data

echo "##### extract master key fgp"
master_key_fgp="${fingerprints[0]}"

if [[ -z "$master_key_fgp" ]]; then
    echo "ERROR: failed to retrieve master key." >&2
    exit 1
fi
    

echo "##### need to add a sign key"
{ echo addkey; echo 4; echo 1024; echo 0; echo save; } | $gpg_exec --expert --command-fd=0 --status-fd=1 --pinentry-mode=loopback --passphrase "" --edit-key "$master_key_fgp"

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to generate subkey (1)" >&2
    exit 1
fi

echo "##### need to add a enc key"
{ echo addkey; echo 6; echo 1024; echo 0; echo save; } | $gpg_exec --expert --command-fd=0 --status-fd=1 --pinentry-mode=loopback --passphrase "" --edit-key "$master_key_fgp"

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to generate subkey (2)" >&2
    exit 1
fi



echo "##### create another subkey"
{ echo addkey; echo 4; echo 1024; echo 0; echo save; } | $gpg_exec --expert --command-fd=0 --status-fd=1 --pinentry-mode=loopback --passphrase "" --edit-key "$master_key_fgp"

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to generate detached key" >&2
    exit 1
fi

echo "##### get its keygrip"
refresh_data
detach_keygrip="${keygrips[-1]}"

# remove the public part
{ echo key 2; echo delkey; echo "y"; echo save; } | $gpg_exec --expert --command-fd=0 --status-fd=1 --edit-key "$master_key_fgp"

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to detach subkey" >&2
    exit 1
fi



echo "##### create second key"
$gpg_exec --batch --gen-key <<EOF
%no-protection
Key-Type:1
Key-Length:1024
Key-Usage:sign
Subkey-Type:1
Subkey-Usage:encrypt
Subkey-Length:1024
Name-Real: test2
Name-Comment: test2
Name-Email: test2@test.com
Expire-Date:0
EOF

# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
    echo "ERROR: failed to generate key (2)" >&2
    exit 1
fi

echo "##### generate variables.sh file"
echo "#!/usr/bin/env bash" > "$GNUPGHOME/variables.sh"
refresh_data
last_inc=${#fingerprints[@]}
last_inc=$((last_inc-1))
key_index=0
subkey_index=0
for i in $(seq 0 $last_inc); do
    if [[ $i -eq 0 ]]; then
        {
            echo "export  MASTER_KEY_1_FGP"
            echo "MASTER_KEY_1_FGP=${fingerprints[i]}"
            echo "export MASTER_KEY_1_KGP"
            echo "MASTER_KEY_1_KGP=${keygrips[i]}"
        } >> "$GNUPGHOME/variables.sh"

        key_index=1
        subkey_index=1
    elif [[ $i -eq 4 ]]; then
        {
            echo "export MASTER_KEY_2_FGP"
            echo "MASTER_KEY_2_FGP=${fingerprints[i]}"
            echo "export MASTER_KEY_2_KGP"
            echo "MASTER_KEY_2_KGP=${keygrips[i]}"
        } >> "$GNUPGHOME/variables.sh"

        key_index=2
        subkey_index=1
    else
        {
            echo "export SUBKEY_${key_index}_${subkey_index}_FGP"
            echo "SUBKEY_${key_index}_${subkey_index}_FGP=${fingerprints[i]}"
            echo "export SUBKEY_${key_index}_${subkey_index}_KGP"
            echo "SUBKEY_${key_index}_${subkey_index}_KGP=${keygrips[i]}"
        } >> "$GNUPGHOME/variables.sh"

        subkey_index=$((subkey_index+1))
    fi
    echo "" >> "$GNUPGHOME/variables.sh"
done

echo "export DETACHED_KEYGRIP" >> "$GNUPGHOME/variables.sh"
echo "DETACHED_KEYGRIP=$detach_keygrip" >> "$GNUPGHOME/variables.sh"

echo "$detach_keygrip" > "$GNUPGHOME/sshcontrol"
