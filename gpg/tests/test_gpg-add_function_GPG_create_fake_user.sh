#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"
export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring2/"

############################################################################## 

function success {
    local count

    # shellcheck disable=SC2154
    count=$($gpg_exec --with-colons -K | grep -c "^sec")

    if [[ $count -ne 1 ]]; then
        return 2
    fi

    GPG_create_fake_user

    GPG_trust_db

    count=$($gpg_exec --with-colons -K | grep -c "^sec")
    if [[ $count -ne 2 ]]; then
        return 2
    fi
}


function double_create {
    local count
    count=$($gpg_exec --with-colons -K | grep -c "^sec")

    if [[ $count -ne 1 ]]; then
        return 2
    fi

    GPG_create_fake_user
    GPG_trust_db
    GPG_create_fake_user
    GPG_trust_db

    count=$($gpg_exec --with-colons -K | grep -c "^sec")
    if [[ $count -ne 3 ]]; then
        return 2
    fi
}


function main {
    local return_code=0

    utest "success" || return_code=1
    utest "double_create" || return_code=1

    return $return_code
}

main
