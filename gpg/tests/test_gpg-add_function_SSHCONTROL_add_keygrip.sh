#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/sshcontrol.setup.functions.sh"

###############################################################################

function test_zero_args {
    # execution
    if ! SSHCONTROL_add_keygrip 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_four_args {
    # execution
    if ! SSHCONTROL_add_keygrip "aaaa" "uuuu" "sss" "ttt" 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_five_args_but_one_is_empty {
    # execution
    if ! SSHCONTROL_add_keygrip "aaaa" "" "sss" "ttt" "uuu" 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_valid {
    # execution
    if ! SSHCONTROL_add_keygrip "keygrip" "DSA" "datadata" "keygrip - md5" "keygrip - sha256"; then
        return 1        
    fi

    if [[ $(tail -n 1 "$WORKFILE" | head -n 1) != "datadata" ]]; then
        return 2
    fi

    if [[ $(tail -n 2 "$WORKFILE" | head -n 1) != "#                keygrip - sha256" ]]; then
        return 3
    fi

    if [[ $(tail -n 3 "$WORKFILE" | head -n 1) != "# Fingerprints:  keygrip - md5" ]]; then
        return 4
    fi

    if [[ $(tail -n 4 "$WORKFILE" | head -n 1) != "# DSA key added on: "* ]]; then
        return 5
    fi

    if [[ $(tail -n 5 "$WORKFILE" | head -n 1) != "" ]]; then
        return 6
    fi

    return 0
}


function main {
    local return_code=0

    utest "test_zero_args" || return_code=1
    utest "test_four_args" || return_code=1
    utest "test_five_args_but_one_is_empty" || return_code=1
    utest "test_valid" || return_code=1

    return $return_code
}

main
