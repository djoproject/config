#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

##############################################################################

function test_without_args {
    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint 2> /dev/null); then
        if [[ "$result" != "" ]]; then
            return 2
        fi

        return 0
    fi

    return 1
}


function test_with_empty_arg {
    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "" 2> /dev/null); then
        if [[ "$result" != "" ]]; then
            return 2
        fi

        return 0
    fi

    return 1
}


function test_existing_md5_fingerprint {
    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    MISC_vercomp "$GPG_VERSION" "2.3.2"
    if [[ $? -eq 2 ]]; then
        if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
            return 2
        fi
    else
        if ! [[ "$fgp" =~ SHA256:[a-zA-Z0-9:+\/]{43} ]]; then
            return 2
        fi
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_existing_md5_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest md5" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
        return 2
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_existing_sha1_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha1" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    MISC_vercomp "$GPG_VERSION" "2.2.15"
    if [[ $? -eq 2 ]]; then
        if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
            return 3
        fi
    else
        if ! [[ "$fgp" =~ SHA1:[a-zA-Z0-9\/\+]{27} ]]; then
            return 2
        fi
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_existing_sha256_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha256" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    MISC_vercomp "$GPG_VERSION" "2.2.15"
    if [[ $? -eq 2 ]]; then
        if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
            return 3
        fi
    else
        if ! [[ "$fgp" =~ SHA256:[a-zA-Z0-9\/\+]{43} ]]; then
            return 2
        fi
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_existing_sha384_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha384" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    MISC_vercomp "$GPG_VERSION" "2.2.15"
    if [[ $? -eq 2 ]]; then
        if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
            return 3
        fi
    else
        if ! [[ "$fgp" =~ SHA384:[a-zA-Z0-9\/\+]{64} ]]; then
            return 2
        fi
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_existing_sha512_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha512" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    local gpg_output fgp result
    IFS=' '; read -ra gpg_output <<< "$(gpg-connect-agent "keyinfo --ssh-fpr $SUBKEY_1_1_KGP" /bye)"; unset IFS;
    fgp="${gpg_output[8]}"

    MISC_vercomp "$GPG_VERSION" "2.2.15"
    if [[ $? -eq 2 ]]; then
        if ! [[ "$fgp" =~ (MD5:)?[a-z0-9:]{47} ]]; then
            return 3
        fi
    else
        if ! [[ "$fgp" =~ SHA512:[a-zA-Z0-9\/\+]{86} ]]; then
            return 2
        fi
    fi

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "$fgp"); then
        return 4
    fi

    [[ "$result" = "$SUBKEY_1_1_KGP" ]]
}


function test_unexistant_md5_fingerprint {
    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "MD5:6b:fc:19:25:de:df:af:ac:a2:6f:10:7b:30:b6:6d:ff"); then
        return 0
    fi

    return 1
}


function test_unexistant_md5_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest md5" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "MD5:6b:fc:19:25:de:df:af:ac:a2:6f:10:7b:30:b6:6d:ff"); then
        return 0
    fi

    return 1
}


function test_unexistant_sha1_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha1" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "SHA1:WYgVwvSj+abIMPNMdv7deRN7m1B"); then
        return 0
    fi

    return 1
}


function test_unexistant_sha256_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha256" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "SHA256:KWzLjDaVR75Vf/tAjArBm10gR68qAMJG96QjJa8AFFo"); then
        return 0
    fi

    return 1
}


function test_unexistant_sha384_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha384" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "SHA384:9x33AsgR+VVpAW8n7hDsNng2s+nDsq91wY5p0vSAVkrPsvvPnuGHelm8RFbFtn95"); then
        return 0
    fi

    return 1
}


function test_unexistant_sha512_fingerprint_and_ssh-fingerprint-digest_set {
    MISC_vercomp "$GPG_VERSION" "2.1.22"
    if [[ $? -eq 2 ]]; then
        SKIP_REASON="ssh-fingerprint-digest is not managed before gpg version 2.1.22, current version is $GPG_VERSION"
        export SKIP_REASON
        return 255
    fi

    echo "ssh-fingerprint-digest sha512" >> "$GNUPGHOME/gpg-agent.conf"

    gpg-connect-agent "KILLAGENT" /bye > /dev/null 2>&1
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    if ! result=$(GPG_AGENT_get_keygrip_from_fingerprint "SHA512:xDfoQNtOKSwalUltR3h/Cy7zTHESzBnz32xHiaEd8sHAF3VJAZeeh9BiDQQNE+Cv4srYLa/flG0A5mmYKTmShQ"); then
        return 0
    fi

    return 1
}


function main {
    local return_code=0

    utest "test_without_args" || return_code=1
    utest "test_with_empty_arg" || return_code=1

    utest "test_existing_md5_fingerprint" || return_code=1
    utest "test_existing_md5_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1
    utest "test_existing_sha1_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1
    utest "test_existing_sha256_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1
    utest "test_existing_sha384_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1
    utest "test_existing_sha512_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1

    utest "test_unexistant_md5_fingerprint" || return_code=1
    utest "test_unexistant_md5_fingerprint_and_ssh-fingerprint-digest_set" || return_code=1

    return $return_code
}

main
