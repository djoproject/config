#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

##############################################################################

function no_keygrip {
    echo "" > "$GNUPGHOME/sshcontrol"

    GPG_AGENT_get_sshcontrol_keygrips

    # shellcheck disable=SC2154
    [[ ${#keygrips[@]} -eq 0 ]]
}


function existing_keygrip {
    GPG_AGENT_get_sshcontrol_keygrips

    if [[ ${#keygrips[@]} -ne 1 ]]; then
        return 2
    fi

    if [[ ${keygrips[0]} != "$DETACHED_KEYGRIP" ]]; then
        return 3
    fi

    return 0
}


function skip_missing_keys {
    GPG_AGENT_get_sshcontrol_keygrips
    if [[ ${#keygrips[@]} -ne 1 ]]; then
        return 2
    fi

    # insert a missing key inside sshcontrol
    echo "" >> "$GNUPGHOME/sshcontrol"
    echo "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" >> "$GNUPGHOME/sshcontrol"
    
    GPG_AGENT_get_sshcontrol_keygrips
    [[ ${#keygrips[@]} -eq 1 ]]
}


function main {
    local return_code=0

    utest "no_keygrip" || return_code=1
    utest "existing_keygrip" || return_code=1
    utest "skip_missing_keys" || return_code=1

    return $return_code
}

main
