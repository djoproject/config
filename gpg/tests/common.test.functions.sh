#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


function utest {
    # SETUP
    if [[ -n "$(command -v setup)" ]]; then
        if ! setup; then
            echo -e "$1: \e[33mSKIP (setup failed)\e[0m"
            return 1
        fi
    fi

    # TEST
    return_value=0
    "$1"
    case $? in
        0)
            echo -e "$1: \e[32mSUCCESS\e[0m"
            ;;
        254)
            echo -e "$1: \e[33mWARNING\e[0m"
            ;;
        255)
            if [[ -z "$SKIP_REASON" ]]; then
                echo -e "$1: \e[33mSKIPPED\e[0m"
            else
                echo -e "$1: \e[33mSKIPPED: $SKIP_REASON\e[0m"
                unset SKIP_REASON
            fi
            ;;
        *)
            echo -e "$1: \e[31mFAILURE ($?)\e[0m"
            return_value=1
            ;;
    esac

    # TEARDOWN
    if [[ -n "$(command -v teardown)" ]]; then
        teardown
    fi

    return $return_value
}
