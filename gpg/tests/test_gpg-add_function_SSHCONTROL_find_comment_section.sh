#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/sshcontrol.setup.functions.sh"

###############################################################################

function missing_args {
    ! SSHCONTROL_find_comment_section "5" 2> /dev/null
}


function empty_args {
    ! SSHCONTROL_find_comment_section "" "${SSHCONTROL_SPECIFIC_LINES_VERSION1[@]}" 2> /dev/null
}


function failure {
    cat >"$WORKFILE" << EOF
# toto

# plo
#
cjfwelijfwli
EOF

value="$(SSHCONTROL_find_comment_section "5" "${SSHCONTROL_SPECIFIC_LINES_VERSION1[@]}")"
    [[ $value -eq 5 ]]
}

function version1_success {
    cat >"$WORKFILE" << EOF
#
# RSA key added on: 2020-03-29 16:43:49
# MD5 Fingerprint: MD5:be:29:94:8e:0f:3e:28:32:53:e8:b9:56:ef:84:5d:aa
36CC86204612C69DB3511710C39390C52E54B06C 0
EOF

    value="$(SSHCONTROL_find_comment_section "4" "${SSHCONTROL_SPECIFIC_LINES_VERSION1[@]}")"
    [[ $value -eq 2 ]]
}


function version2_success {
    cat >"$WORKFILE" << EOF
#
# RSA key added on: 2020-03-29 16:43:49
# Fingerprints:  MD5:be:29:94:8e:0f:3e:28:32:53:e8:b9:56:ef:84:5d:aa
#                SHA256:zZLZNGoWVgg/4Rh05P121BrVTlCi5Bg1FksVszTHON8
36CC86204612C69DB3511710C39390C52E54B06C 0
EOF

    value="$(SSHCONTROL_find_comment_section "5" "${SSHCONTROL_SPECIFIC_LINES_VERSION2[@]}")"
    [[ $value -eq 2 ]]
}


function main {
    local return_code=0

    utest "missing_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "failure" || return_code=1
    utest "version1_success" || return_code=1
    utest "version2_success" || return_code=1

    return $return_code
}

main
