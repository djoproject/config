#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"
export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring2/"

############################################################################## 

function too_few_args {
    ! GPG_delete_public_subkey 2> /dev/null
}


function empty_args {
    ! GPG_delete_public_subkey "" "plop" 2> /dev/null
}


function unexistent_primary_key {
    GPG_delete_public_subkey "CE0663AE2B20620AA68248966FDE86B4F8B0FFFF" "5"
    [[ $? -eq 2 ]]
}


function unexistent_key_id {
    GPG_delete_public_subkey "$MASTER_KEY_1_FGP" "5"
}


function success {
    local count

    # shellcheck disable=SC2154
    count=$($gpg_exec --with-colons -K | grep -c ssb)

    if [[ $count -ne 1 ]]; then
        return 3
    fi

    if ! GPG_delete_public_subkey "$MASTER_KEY_1_FGP" "1"; then
        return 4
    fi

    count=$($gpg_exec --with-colons -K | grep -c ssb)

    if [[ $count -ne 0 ]]; then
        return 5
    fi
}


function main {
    local return_code=0

    utest "too_few_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "unexistent_primary_key" || return_code=1
    utest "unexistent_key_id" || return_code=1
    utest "success" || return_code=1

    return $return_code
}

main
