#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/basic.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

###############################################################################


function missing_args {
    ! MISC_check_args
}


function no_args {
    MISC_check_args "plop" 0
}



function not_enough_args {
    ! MISC_check_args "plop" 2 "aa" 2> /dev/null
}


function empty_args {
    ! MISC_check_args "plop" 2 "" "aa" 2> /dev/null
}


function success {
    MISC_check_args "plop" 2 "bb" "aa"
}


function main {
    local return_code=0

    utest "missing_args" 2> /dev/null || return_code=1
    utest "no_args" || return_code=1
    utest "not_enough_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "success" || return_code=1

    return $return_code
}

main
