#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

##############################################################################

function test_zero_args {
    # execution
    if ! GPG_AGENT_get_fingerprint_from_keygrip 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_one_args {
    # execution
    if ! GPG_AGENT_get_fingerprint_from_keygrip "aaaa" 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_two_args_but_one_is_empty {
    # execution
    if ! GPG_AGENT_get_fingerprint_from_keygrip "" "uuuu" 2> /dev/null; then
        return 0
    fi

    return 1
}


function existing_keygrip_md5 {
    if ! rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "md5"); then
        return 2
    fi

    [[ "$rhash" =~ (MD5:)?[a-z0-9:]{47} ]]
}


function existing_keygrip_sha1 {
    rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "sha1")
    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        [[ "$rhash" = "SHA1:not_supported" ]]
    else
        [[ "$rhash" =~ SHA1:[a-zA-Z0-9\/\+]{27} ]]
    fi
}


function existing_keygrip_sha256 {
    rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "sha256")
    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        [[ "$rhash" = "SHA256:not_supported" ]]
    else
        [[ "$rhash" =~ SHA256:[a-zA-Z0-9\/\+]{43} ]]
    fi
}


function existing_keygrip_sha384 {
    rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "sha384")
    # Not implemented so far (ver 2.2.27)
    [[ "$rhash" = "SHA384:not_supported" ]]
}


function existing_keygrip_sha512 {
    rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "sha512")
    # Not implemented so far (ver 2.2.27)
    [[ "$rhash" = "SHA512:not_supported" ]]
}


function existing_keygrip_unknown_hash {
    # I hope no one will ever call a hash algorithm toto99...
    if ! rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "$DETACHED_KEYGRIP" "toto99"); then
        [[ "$rhash" = "TOTO99:not_supported" ]]
        return
    fi

    return 22
}


function missing_keygrip_md5 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "md5" 2>&1); then
        return 1
    fi

    if [[ "$rhash" != *"fingeprint not found"* ]]; then
        return 2
    fi

    return 0
}


function missing_keygrip_sha1 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "sha1" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA1:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"fingeprint not found"* ]]; then
            return 2
        fi
    fi

    return 0
}


function missing_keygrip_sha256 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "sha256" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA256:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"fingeprint not found"* ]]; then
            return 2
        fi
    fi

    return 0
}


function missing_keygrip_sha384 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "sha384" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA384:not_supported" ]]
}


function missing_keygrip_sha512 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "sha512" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA512:not_supported" ]]
}


function missing_keygrip_unknown_hash {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54B06D" "toto99" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "TOTO99:not_supported" ]]
}


function invalid_keygrip_md5 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "md5" 2>&1); then
        echo "$rhash"
        return 1
    fi

    if [[ "$rhash" != *"invalid keygrip"* ]]; then
        return 2
    fi

    return 0
}


function invalid_keygrip_sha1 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "sha1" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA1:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"invalid keygrip"* ]]; then
            return 2
        fi
    fi

    return 0
}


function invalid_keygrip_sha256 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "sha256" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA256:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"invalid keygrip"* ]]; then
            return 2
        fi
    fi

    return 0
}


function invalid_keygrip_sha384 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "sha384" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA384:not_supported" ]]
}


function invalid_keygrip_sha512 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "sha512" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA512:not_supported" ]]
}


function invalid_keygrip_unknown_hash {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "36CC86204612C69DB3511710C39390C52E54BXXX" "toto99" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "TOTO99:not_supported" ]]
}


function invalid_length_keygrip_md5 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAA" "md5" 2>&1); then
        return 1
    fi

    if [[ "$rhash" != *"invalid keygrip length"* ]]; then
        return 2
    fi

    return 0
}


function invalid_length_keygrip_sha1 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAAA" "sha1" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA1:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"invalid keygrip length"* ]]; then
            return 2
        fi
    fi

    return 0
}


function invalid_length_keygrip_sha256 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAAA" "sha256" 2>&1); then
        return 1
    fi

    MISC_vercomp "$GPG_VERSION" "2.2.15"

    if [[ $? -eq 2 ]] ; then
        if [[ "$rhash" != "SHA256:not_supported" ]]; then
            return 3
        fi
    else
        if [[ "$rhash" != *"invalid keygrip length"* ]]; then
            return 2
        fi
    fi

    return 0
}


function invalid_length_keygrip_sha384 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAAA" "sha384" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA384:not_supported" ]]
}


function invalid_length_keygrip_sha512 {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAAA" "sha512" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "SHA512:not_supported" ]]
}


function invalid_length_keygrip_unknown_hash {
    if rhash=$(GPG_AGENT_get_fingerprint_from_keygrip "AAAAA" "toto99" 2>&1); then
        return 2
    fi

    [[ "$rhash" = "TOTO99:not_supported" ]]
}

# TODO should make these tests with md5/sha1/sha256/sha512 in gpg-agent.conf
#   that would do a lot of test...

function main {
    local return_code=0

    utest "test_zero_args" || return_code=1
    utest "test_one_args" || return_code=1
    utest "test_two_args_but_one_is_empty" || return_code=1

    utest "existing_keygrip_md5" || return_code=1
    utest "existing_keygrip_sha1" || return_code=1
    utest "existing_keygrip_sha256" || return_code=1
    utest "existing_keygrip_sha384" || return_code=1
    utest "existing_keygrip_sha512" || return_code=1
    utest "existing_keygrip_unknown_hash" || return_code=1

    utest "missing_keygrip_md5" || return_code=1
    utest "missing_keygrip_sha1" || return_code=1
    utest "missing_keygrip_sha256" || return_code=1
    utest "missing_keygrip_sha384" || return_code=1
    utest "missing_keygrip_sha512" || return_code=1
    utest "missing_keygrip_unknown_hash" || return_code=1

    utest "invalid_keygrip_md5" || return_code=1
    utest "invalid_keygrip_sha1" || return_code=1
    utest "invalid_keygrip_sha256" || return_code=1
    utest "invalid_keygrip_sha384" || return_code=1
    utest "invalid_keygrip_sha512" || return_code=1
    utest "invalid_keygrip_unknown_hash" || return_code=1

    utest "invalid_length_keygrip_md5" || return_code=1
    utest "invalid_length_keygrip_sha1" || return_code=1
    utest "invalid_length_keygrip_sha256" || return_code=1
    utest "invalid_length_keygrip_sha384" || return_code=1
    utest "invalid_length_keygrip_sha512" || return_code=1
    utest "invalid_length_keygrip_unknown_hash" || return_code=1

    return $return_code
}

main
