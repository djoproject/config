#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/sshcontrol.setup.functions.sh"

##############################################################################

function test_without_args {
    if ! result=$(SSHCONTROL_remove_keygrip 2> /dev/null); then
        return 0
    fi

    return 1
}


function test_with_empty_arg {
    if ! result=$(SSHCONTROL_remove_keygrip "" 2> /dev/null); then
        return 0
    fi

    return 1
}


function test_sshcontrol_does_not_exist {
    rm -rf "$WORKFILE"
    SSHCONTROL_remove_keygrip "36CC86204612C69DB3511710C39390C52E54B06C"
    [[ $? -eq 2 ]]
}


function test_keygrip_not_in_sshcontrol {
    SSHCONTROL_remove_keygrip "36CC86204612C69DB3511710C39390C52E54B06C"
    [[ $? -eq 2 ]]
}


function test_normal_case {
    cat >"$WORKFILE" << EOF


# RSA key added on: 2020-03-29 16:43:49
# Fingerprints:  MD5:be:29:94:8e:0f:3e:28:32:53:e8:b9:56:ef:84:5d:aa
#                SHA256:zZLZNGoWVgg/4Rh05P121BrVTlCi5Bg1FksVszTHON8
36CC86204612C69DB3511710C39390C52E54B06C 0
EOF

    result=$(wc -l < "$WORKFILE")
    if [[ $result -eq 0 ]]; then
        return 1
    fi

    if ! SSHCONTROL_remove_keygrip "36CC86204612C69DB3511710C39390C52E54B06C"; then
        return 2
    fi

    result=$(wc -l < "$WORKFILE")
    if [[ $result -ne 0 ]]; then
        return 3
    fi
}


function test_normal_case_without_empty_line {
    cat >"$WORKFILE" << EOF
#
# RSA key added on: 2020-03-29 16:43:49
# Fingerprints:  MD5:be:29:94:8e:0f:3e:28:32:53:e8:b9:56:ef:84:5d:aa
#                SHA256:zZLZNGoWVgg/4Rh05P121BrVTlCi5Bg1FksVszTHON8
36CC86204612C69DB3511710C39390C52E54B06C 0
EOF

    result=$(wc -l < "$WORKFILE")
    if [[ $result -eq 0 ]]; then
        return 1
    fi

    if ! SSHCONTROL_remove_keygrip "36CC86204612C69DB3511710C39390C52E54B06C"; then
        return 2
    fi

    result=$(wc -l < "$WORKFILE")
    if [[ $result -ne 1 ]]; then
        return 3
    fi
}


function test_with_abnormal_comment {
    cat >"$WORKFILE" << EOF
#

# RSA key added on: 2020-03-29 16:43:49
# lfejrljrelkfe.rkfrejlgkj
#                SHA256:zZLZNGoWVgg/4Rh05P121BrVTlCi5Bg1FksVszTHON8
36CC86204612C69DB3511710C39390C52E54B06C 0
EOF

    result=$(wc -l < "$WORKFILE")
    if [[ $result -eq 0 ]]; then
        return 1
    fi

    if ! SSHCONTROL_remove_keygrip "36CC86204612C69DB3511710C39390C52E54B06C"; then
        return 2
    fi

    result=$(wc -l < "$WORKFILE")
    if [[ $result -ne 4 ]]; then
        return 3
    fi
}


function main {
    local return_code=0

    utest "test_without_args" || return_code=1
    utest "test_with_empty_arg" || return_code=1
    utest "test_sshcontrol_does_not_exist" || return_code=1
    utest "test_keygrip_not_in_sshcontrol" || return_code=1
    utest "test_normal_case" || return_code=1
    utest "test_normal_case_without_empty_line" || return_code=1
    utest "test_with_abnormal_comment" || return_code=1

    return $return_code
}

main
