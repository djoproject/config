#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

##############################################################################

function test_without_args {
    if ! GPG_AGENT_delete_private_key_for_keygrip 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_with_empty_arg {
    if ! GPG_AGENT_delete_private_key_for_keygrip "" 2> /dev/null; then
        return 0
    fi

    return 1
}


function test_existing_key {
    if [[ ! ( -f "$GNUPGHOME/private-keys-v1.d/$DETACHED_KEYGRIP.key" ) ]]; then
        return 1
    fi

    if ! GPG_AGENT_delete_private_key_for_keygrip "$DETACHED_KEYGRIP"; then
        return 2
    fi

    [[ ! ( -f "$GNUPGHOME/private-keys-v1.d/$DETACHED_KEYGRIP.key" ) ]]
}


function test_unexisting_key {
    if [[ -f "$GNUPGHOME/private-keys-v1.d/36CC86204612C69DB3511710C39390C52E54BFFF.key" ]]; then
        return 1
    fi

    if GPG_AGENT_delete_private_key_for_keygrip "36CC86204612C69DB3511710C39390C52E54BFFF" 2> /dev/null; then
        return 2
    fi

    [[ ! ( -f "$GNUPGHOME/private-keys-v1.d/36CC86204612C69DB3511710C39390C52E54BFFF.key" ) ]]
}


function test_invalid_key {
    if GPG_AGENT_delete_private_key_for_keygrip "36CC86204612C69DB3511710C39390C52E54BZZZ" 2> /dev/null; then
        return 1
    fi
}


function test_too_short_key {
    if GPG_AGENT_delete_private_key_for_keygrip "aaaa" 2> /dev/null; then
        return 1
    fi
}


function main {
    local return_code=0

    utest "test_without_args" || return_code=1
    utest "test_with_empty_arg" || return_code=1
    utest "test_existing_key" || return_code=1
    utest "test_unexisting_key" || return_code=1
    utest "test_invalid_key" || return_code=1
    utest "test_too_short_key" || return_code=1

    return $return_code
}

main
