#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

##############################################################################

function noargs {
    GPG_get_keygrip_data_from_userid 1

    # shellcheck disable=SC2154
    for kgp in "${keygrips_data[@]}"; do
        IFS=';'; read -ra keygrip_data_splitted <<< "$kgp"; unset IFS;
        if [[ "${keygrip_data_splitted[0]}" != "$SUBKEY_1_1_KGP" ]]; then
            return 1
        fi

        if [[ "${keygrip_data_splitted[1]}" != "RSA" ]]; then
            return 2
        fi

        if [[ "${keygrip_data_splitted[2]}" != "1024" ]]; then
            return 3
        fi

        if [[ "${keygrip_data_splitted[3]}" != "$SUBKEY_1_1_FGP" ]]; then
            return 4
        fi
    done

    [[ ${#keygrips_data[@]} -ne 0 ]]
}


function test_without_gpgid {
    GPG_get_keygrip_data_from_userid 1

    for kgp in "${keygrips_data[@]}"; do
        IFS=';'; read -ra keygrip_data_splitted <<< "$kgp"; unset IFS;
        if [[ "${keygrip_data_splitted[0]}" != "$SUBKEY_1_1_KGP" ]]; then
            return 1
        fi

        if [[ "${keygrip_data_splitted[1]}" != "RSA" ]]; then
            return 2
        fi

        if [[ "${keygrip_data_splitted[2]}" != "1024" ]]; then
            return 3
        fi

        if [[ "${keygrip_data_splitted[3]}" != "$SUBKEY_1_1_FGP" ]]; then
            return 4
        fi
    done

    # shellcheck disable=SC2154
    [[ ${#keygrips_data[@]} -ne 0 ]]
}


function test_with_gpgid {
    GPG_get_keygrip_data_from_userid 1 "test@test.com"

    for kgp in "${keygrips_data[@]}"; do
        IFS=';'; read -ra keygrip_data_splitted <<< "$kgp"; unset IFS;
        if [[ "${keygrip_data_splitted[0]}" != "$SUBKEY_1_1_KGP" ]]; then
            return 1
        fi

        if [[ "${keygrip_data_splitted[1]}" != "RSA" ]]; then
            return 2
        fi

        if [[ "${keygrip_data_splitted[2]}" != "1024" ]]; then
            return 3
        fi

        if [[ "${keygrip_data_splitted[3]}" != "$SUBKEY_1_1_FGP" ]]; then
            return 4
        fi
    done

    # shellcheck disable=SC2154
    [[ ${#keygrips_data[@]} -ne 0 ]]
}


function test_with_unknown_gpgid {
    GPG_get_keygrip_data_from_userid 1 "plop@test.com"

    # shellcheck disable=SC2154
    [[ ${#keygrips_data[@]} -eq 0 ]]
}


function main {
    local return_code=0

    utest "noargs" || return_code=1
    utest "test_without_gpgid" || return_code=1
    utest "test_with_gpgid" || return_code=1
    utest "test_with_unknown_gpgid" || return_code=1

    return $return_code
}

main
