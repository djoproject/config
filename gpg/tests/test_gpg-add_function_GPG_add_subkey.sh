#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"
export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring2/"

############################################################################## 

function too_few_args {
    if GPG_add_subkey 2> /dev/null; then
        return 1
    fi

    return 0
}


function empty_args {
    if GPG_add_subkey "" "plop" 2> /dev/null; then
        return 1
    fi

    return 0
}


function succes {
    local count

    # shellcheck disable=SC2154
    count=$($gpg_exec -K --with-colons --with-keygrip | grep -c "$DETACHED_KEYGRIP")
    if ! [[ $count -eq 0 ]]; then
        return 2
    fi

    if ! GPG_add_subkey "$MASTER_KEY_1_FGP" "$DETACHED_KEYGRIP"; then
        return 1
    fi

    count=$($gpg_exec -K --with-colons --with-keygrip | grep -c "$DETACHED_KEYGRIP")
    if ! [[ $count -eq 1 ]]; then
        return 3
    fi

    return 0
}


function already_attached {
    local count

    count=$($gpg_exec -K --with-colons --with-keygrip | grep -c "$DETACHED_KEYGRIP")
    if ! [[ $count -eq 0 ]]; then
        return 2
    fi

    if ! GPG_add_subkey "$MASTER_KEY_1_FGP" "$DETACHED_KEYGRIP"; then
        return 1
    fi

    count=$($gpg_exec -K --with-colons --with-keygrip | grep -c "$DETACHED_KEYGRIP")
    if ! [[ $count -eq 1 ]]; then
        return 3
    fi

    GPG_add_subkey "$MASTER_KEY_1_FGP" "$DETACHED_KEYGRIP"

    count=$($gpg_exec -K --with-colons --with-keygrip | grep -c "$DETACHED_KEYGRIP")
    if ! [[ $count -eq 1 ]]; then
        return 4
    fi
}


function main {
    if [[ -z "$(command -v expect)" ]]; then
        echo -e "*: \e[33mSKIP (expect is missing)\e[0m"
        exit 1
    fi

    local return_code=0

    utest "too_few_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "succes" || return_code=1
    utest "already_attached" || return_code=1

    return $return_code
}

main
