#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"

############################################################################## 

function already_exist {
    local count

    # shellcheck disable=SC2154
    count=$($gpg_exec -K --with-colons "gpg-add@import.ssh" | grep -c sec)

    if [[ $count -ne 1 ]]; then
        return 3
    fi

    local data
    data=$(GPG_get_or_create_fake_user)

    if [[ -z "$data" ]]; then
        return 5
    fi

    count=$($gpg_exec -K --with-colons "gpg-add@import.ssh" | grep -c sec)

    if [[ $count -ne 1 ]]; then
        return 4
    fi
}


function does_not_exist {
    local count
    count=$($gpg_exec -K --with-colons "gpg-add@import.ssh" 2> /dev/null | grep -c sec)

    if [[ $count -ne 0 ]]; then
        return 3
    fi

    local data
    data=$(GPG_get_or_create_fake_user)

    count=$($gpg_exec -K --with-colons "gpg-add@import.ssh" | grep -c sec)

    if [[ $count -ne 1 ]]; then
        return 4
    fi
       
}


function main {
    local return_code=0

    export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring2/"
    utest "already_exist" || return_code=1

    export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring1/"
    utest "does_not_exist" || return_code=1

    return $return_code
}

main
