#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/gpg.setup.functions.sh"

# shellcheck disable=SC1090,SC1091
source "$CURRENT_PATH/common.test.functions.sh"
export GPG_TEST_KEYRING_PATH="$CURRENT_PATH/data/keyring2/"

############################################################################## 

function missing_args {
    ! GPG_delete_uid 2> /dev/null
}


function empty_args {
    ! GPG_delete_uid "" "toto" 2> /dev/null
}


function not_found {
    GPG_delete_uid "$MASTER_KEY_1_FGP" "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
}


function success {
    local count
    
    # shellcheck disable=SC2154
    count=$($gpg_exec --with-colons -K | grep "^uid" | grep -c EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE)

    if [[ $count -ne 1 ]]; then
        return 3
    fi

    if ! GPG_delete_uid "$MASTER_KEY_1_FGP" "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"; then
        return 4
    fi

    GPG_trust_db
    count=$($gpg_exec --with-colons -K | grep "^uid" | grep -c EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE)

    if [[ $count -ne 0 ]]; then
        return 5
    fi

    return 0
}


function main {
    local return_code=0

    utest "missing_args" || return_code=1
    utest "empty_args" || return_code=1
    utest "not_found" || return_code=1
    utest "success" || return_code=1

    return $return_code
}

main
