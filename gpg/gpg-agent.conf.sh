#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [[ -z "$(command -v realpath)" ]]; then
    echo "ERROR: realpath does not seem to be installed"
    exit 1
fi

echo "# https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html"
echo ""
echo "# if at least n seconds have been spent between two gpg-agent access, ask password"
echo "# default value: 600"
echo "default-cache-ttl 900"
echo ""
echo "# if at least n seconds have been spent since the gpg-agent has started, ask password"
echo "# default value: 7200"
echo "max-cache-ttl 3600"
echo ""
echo "# default value: 1800"
echo "default-cache-ttl-ssh 2700"
echo ""
echo "# default value: 7200"
echo "max-cache-ttl-ssh 3600"
echo ""
echo "# supposed to tell to the gpg-agent to set the var SSH_AUTH_SOCK"
echo "# in practice, does not look to work"
echo "enable-ssh-support"
echo ""
echo "# define hash printed by pinentry"
echo "# and also has printed by KEYINFO since 2.2.15"
echo "ssh-fingerprint-digest sha256"


if [[ -n "$(command -v pinentry-gtk-2)" ]]; then
    pinentry_path=$(command -v pinentry-gtk-2)
    pinentry_path=$(realpath "$pinentry_path")
    echo ""
    echo "# define pinentry program"
    echo "pinentry-program $pinentry_path"
elif [[ -n "$(command -v pinentry-curses)" ]]; then
    pinentry_path=$(command -v pinentry-curses)
    pinentry_path=$(realpath "$pinentry_path")
    echo ""
    echo "# define pinentry program"
    echo "pinentry-program $pinentry_path"
else
    echo "WARNING: use of the default pinentry that is not pinentry-gtk-2 nor pinentry-curses." >&2
fi
