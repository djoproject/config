#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# does it exist ?
if [[ -n "$(command -v gpg)" ]]; then
    mapfile -t gpg_version_array < <(gpg --version)
    IFS=' '; read -ra gpg_version_array <<< "${gpg_version_array[0]}"; unset IFS;
    gpg_version=${gpg_version_array[2]}

    # if both version 1 and 2 are installed, force version 2
    if [[ $gpg_version = "1."* ]] && [[ -n "$(command -v gpg2)" ]]; then
        alias gpg="gpg2"
    fi

    unset gpg_version
    unset gpg_version_array
elif [[ -n "$(command -v gpg2)" ]]; then
    alias gpg="gpg2"
fi

# compute definitive version
if [[ -n "$(command -v gpg)" ]]; then
    mapfile -t gpg_version_array < <(gpg --version)
    IFS=' '; read -ra gpg_version_array <<< "${gpg_version_array[0]}"; unset IFS;
    export GPG_VERSION=${gpg_version_array[2]}
    unset gpg_version_array
fi
