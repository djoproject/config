#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sys import exit
from sys import version_info


def read_file(path):
    keys = {}
    key_indexes = []
    content = []
    index = 0

    with open(original_file, 'w') as configfile:
        for line in configfile:
            line = line.rstrip()

            if len(line) == 0:
                index += 1
                continue

            content.append(line)
            line = line.lstrip()

            if len(line) == 0 or line[0] == "#":
                index += 1
                continue

            splitted_line = line.split()
            key = splitted_line[0]

            if key in keys:
                print("ERROR multiple key '{0}'".format(key))
                exit(1)

            keys[key] = (index, splitted_line[1:])
            key_indexes.append(index)
            index +=1

        return keys, content


def main(original_file, new_file):
    original_keys, original_key_indexes, original_content = read_file(original_file)
    new_keys, new_key_indexes, new_content = read_file(original_file)

    updated_content = []

    # TODO copy original_keys that does not exist in new_keys

    # TODO update value of key existing in both, with new_value
    #   XXX what happen if the file is patched several times ?
    #       IDEA 1: only update when value is different.  okay but not enough, what about comment ?

    # TODO insert missing keys from new_keys


if __name__ == "__main__":
    import argparse
    assert version_info >= (3, 2), "Script requires Python 3.2+."

    parser = argparse.ArgumentParser(description="Patch an existing config file")
    parser.add_argument('original_file', type=str, help='the existing file to patch')
    parser.add_argument('new_file', type=str, help='the new file to merge into the existing one')
    args = parser.parse_args()

    exit(main(args.original_file, args.new_file))
