#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ssh support was added since 1.9.16
# src: https://gnupg.org/download/release_notes.html
# src: https://github.com/gpg/gnupg/blob/master/NEWS
_FIRST_GPG_VERSION_WITH_SSH="1.9.16"


function vercomp {
    if [[ "$1" == "$2" ]]; then
        return 0
    fi

    local i ver1 ver2
    IFS='.'; read -ra ver1 <<< "$1"; unset IFS;
    IFS='.'; read -ra ver2 <<< "$2"; unset IFS;

    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++)); do
        ver1[i]=0
    done

    for ((i=0; i<${#ver1[@]}; i++)); do
        # fill empty fields in ver2 with zeros
        if [[ -z ${ver2[i]} ]]; then
            ver2[i]=0
        fi

        if ((10#${ver1[i]} > 10#${ver2[i]})); then
            return 1
        fi

        if ((10#${ver1[i]} < 10#${ver2[i]})); then
            return 2
        fi
    done
    return 0
}


function _set_socket_permanent {
    local setting_path
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        setting_path="$CUSTOM_CONFIG_DIRECTORY/settings"
        mkdir -p "$setting_path"
        setting_path="$setting_path/last_ssh_socket"  # not an hidden file
    else
        setting_path="$HOME/.last_ssh_socket"  # an hidden file because in HOME
    fi

    echo "$SSH_AUTH_SOCK" > "$setting_path"
}


function _set_pid_permanent {
    local setting_path
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        setting_path="$CUSTOM_CONFIG_DIRECTORY/settings"
        mkdir -p "$setting_path"
        setting_path="$setting_path/last_ssh_agent_pid"  # not an hidden file
    else
        setting_path="$HOME/.last_ssh_agent_pid"  # an hidden file because in HOME
    fi

    if [[ -z "$SSH_AGENT_PID" ]]; then
        if [[ -f "$setting_path" ]]; then
            rm "$setting_path"
        fi

        return
    fi

    echo "$SSH_AGENT_PID" > "$setting_path"
}


function _generate_gpg_ssh_agent_path {
    # careful about:
    #   * socket files only exist when agent is running.
    #   * socket files dissappear when agent is killed.
    #   * socket files may be created inside $HOME/.gnupg if not supervised
    #
    # This function tries to manage with all the specs listed.

    if [[ -z "$(command -v gpg-connect-agent)" ]]; then
        echo ""
        echo "ERROR: gpg does not seem installed." >&2
        return 1
    fi

    # The following call was added in gp 2.0.5.
    # Nothing is useful with gpgconf before gpg 2.0.10
    local gpg_ssh_auth_sock gpg_ssh_auth_sock_array
    gpg_ssh_auth_sock=$(gpg-connect-agent "getinfo ssh_socket_name" /bye 2> /dev/null | head -n 1)

    # gpg-connect-agent does not return a value different from zero if an error
    # occurs.  It always return 0.
    if [[ "$gpg_ssh_auth_sock" = "ERR"* ]]; then
        echo ""
        echo "$gpg_ssh_auth_sock" >&2
        return 1
    fi

    IFS=' '; read -ra gpg_ssh_auth_sock_array <<< "$gpg_ssh_auth_sock"; unset IFS;
    echo "${gpg_ssh_auth_sock_array[1]}"
    return 0
}


function _is_gpg_agent_set {
    #
    #   return 0 if a gpg-agent socket is set, 1 otherwise
    #   <!> it does not tell if the agent is running or not.
    #

    if [[ -z "$SSH_AUTH_SOCK" ]]; then
        return 1
    fi

    local gpg_ssh_auth_sock
    gpg_ssh_auth_sock=$(_generate_gpg_ssh_agent_path)

    # okay so it wasn't possible to retrieve an existing or wanted socket
    # but if the SSH_AUTH_SOCK contains a gpg socket name, that means
    # the user want gpg-agent to run.
    if [[ -n "$gpg_ssh_auth_sock" ]]; then
        [[ "$SSH_AUTH_SOCK" = "$gpg_ssh_auth_sock" ]]
    else
        [[ "$SSH_AUTH_SOCK" = *"S.gpg-agent.ssh" ]]
    fi
}


function agent_reset_behavior {
    local setting_path
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        # not an hidden file
        # don't care if settings directory does not exist yet.
        setting_path="$CUSTOM_CONFIG_DIRECTORY/settings/last_ssh_socket"
    else
        # an hidden file because in HOME
        setting_path="$HOME/.last_ssh_socket"
    fi

    if [[ -f "$setting_path" ]]; then
        rm -f "$setting_path"
    fi
}


function agent_ssh {
    # Is there an argument ?
    # Is there a gnome-keyring agent ?
    # Is there a OSX ssh-agent ?
    # look after any other ssh agent

    local gpg_ssh_auth_sock
    gpg_ssh_auth_sock=$(_generate_gpg_ssh_agent_path)

    if [[ -n "$1" ]]; then
        if [[ "$1" = "$gpg_ssh_auth_sock" ]] || [[ "$1" = *"S.gpg-agent.ssh" ]]; then
            echo "ERROR: a gpg socket agent was provided." >&2
            return 1
        fi

        if ! [[ -S "$1" ]]; then
            echo "ERROR: provided path is not a valid unix socket" >&2
            return 1
        fi

        export SSH_AUTH_SOCK="$1"
        _set_socket_permanent
        return 0
    fi

    # is there already a ssh agent running ?
    # must be different from gpg-agent
    if [[ -n "$SSH_AUTH_SOCK" ]] && [[ "$SSH_AUTH_SOCK" != "$gpg_ssh_auth_sock" ]]; then
        echo "ssh-agent already set"
        _set_socket_permanent
        return 0
    fi

    # is there a gnome-keyring running ?
    local gnome_keyring_socket
    gnome_keyring_socket="/run/user/$(id -u)/keyring/ssh"
    # gnome_keyring_ssh_agent_socket="/run/user/$(id -u)/keyring/.ssh"
    if [[ -S "$gnome_keyring_socket" ]]; then
        export SSH_AUTH_SOCK=$gnome_keyring_socket
        _set_socket_permanent
        return 0
    fi

    # is there an OSX keyring running ?
    # /private/tmp/com.apple.launchd.*/Listeners
    shopt -s nullglob
    local sock osx_socket
    for sock in /private/tmp/com.apple.launchd.*/Listeners; do
        if [[ -n "$osx_socket" ]]; then
            echo "ERROR: too many OSX ssh-agent candidates" >&2
            return 1
        fi

        osx_socket="$sock"
    done
    shopt -u nullglob

    if [[ -n "$osx_socket" ]]; then
        export SSH_AUTH_SOCK="$osx_socket"
        _set_socket_permanent
        return 0
    fi

    local netstat_argument
    if [[ "$OSTYPE" = "darwin"* ]]; then
        netstat_argument="-f unix"
    else
        netstat_argument="-lx"
    fi

    ## look after other candidate
    local agents
    declare -a agents

    if [[ -z "$(command -v netstat)" ]]; then
        echo "WARNING: netstat does not seem to be installed"
    else
        # the .ssh is the hidden ssh-agent used by gnome-keyring, do not use it
        local gnome_keyring_hiddent_socket
        gnome_keyring_hiddent_socket="/run/user/$(id -u)/keyring/.ssh"

        while read -r sock; do
            if [[ -z "$sock" ]]; then
                continue
            fi

            # need read and write access to be a valid candidate
            if [[ ! ( -r "$sock" ) ]] || [[ ! ( -w "$sock" ) ]]; then
                continue
            fi

            # skip master gpg ssh socket
            if [[ "$sock" = "$gpg_ssh_auth_sock" ]]; then
                continue
            fi

            # skip every remaining gpg-agent ssh socket
            if [[ "$sock" = *"S.gpg-agent.ssh" ]]; then
                continue
            fi

            # skip hidden gnome ssh-agent socket
            if [[ "$sock" = "$gnome_keyring_hiddent_socket" ]]; then
                continue
            fi

            # if root, skip socket of other users.
            if [[ $(id -u) -eq 0 ]] && [[ $(stat -c '%U' "$sock") != "root" ]] && [[ $(stat -c '%G' "$sock") != "root" ]]; then
                continue
            fi

            agents+=("$sock")
        done < <(netstat "$netstat_argument" | awk '{ print $NF }' | grep ssh)
    fi

    if [[ ${#agents[@]} -eq 0 ]]; then
        # if no ssh_agent is running, start a new one
        echo "starting ssh-agent..."

        if ! result=$(ssh-agent -s); then
            return_code="$?"
            echo "ERROR: failed to start ssh agent" >&2
            return $return_code
        fi

        eval "$result"
        _set_pid_permanent
        _set_socket_permanent

        return 0
    elif [[ ${#agents[@]} -eq 1 ]]; then
        export SSH_AUTH_SOCK="${agents[0]}"
        _set_socket_permanent
        return 0
    fi

    echo "ERROR: too many ssh-agent candidates" >&2
    return 1
}


function agent_gpg {
    # https://www.gnupg.org/documentation/manuals/gnupg/Agent-Options.html
    # HINT 1: GPG does not allow several agent to run on the same keyring (not like ssh)
    # HINT 2: ssh support is always enabled inside GPG for newest version.
    #   On new version: enable-ssh-support just means the variable
    #   SSH_AUTH_SOCK is set or not
    #   On old version: enable-ssh-support ensure the ssh socket creation
    #   inside ~/.gnupg or GNUPGHOME/.

    # this call looks after the gpg agent-ssh-socket path if there is one
    # configured.  it don't care about the configuration (e.g gnome-keyring
    # or no)
    # It does not care if gpg-agent is running or not.

    if [[ -z "$(command -v gpg-agent)" ]]; then
        echo "WARNING: gpg-agent does not seem to be installed"
        return 1
    fi

    if [[ -z "$GPG_VERSION" ]]; then
        echo "WARNING: no gpg version available, skip test"
    else
        vercomp "$GPG_VERSION" "$_FIRST_GPG_VERSION_WITH_SSH"

        if [[ $? -eq 2 ]]; then
            echo "ERROR: this version of gpg does not manage ssh support." >&2
            return 1
        fi
    fi

    if [[ $# -gt 0 ]]; then
        echo "WARNING: not possible to pick another gpg-agent by arguments."
        echo "To do so, override global variable GNUPGHOME."
        return
    fi

    # kick start the gpg-agent to be sure unix socket are created
    # and also set TTY and X11 information to agent
    if [[ -n "$(command -v gpg-connect-agent)" ]]; then
        gpg-connect-agent updatestartuptty /bye > /dev/null
    fi

    local gpg_ssh_auth_sock
    gpg_ssh_auth_sock=$(_generate_gpg_ssh_agent_path)

    if [[ -z "$gpg_ssh_auth_sock" ]]; then
        echo "ERROR: no agent-ssh-socket found" >&2
        return 1

        # At this point it could be interesting to look if gpg-agent is running
        # or trying to start it.  But in every observed cases, gpg-agent has
        # always started by itself, even when it was indirectly contacted.
        # So no need to start it.
    fi

    # is the gpg-agent already selected ?
    if [[ "$SSH_AUTH_SOCK" = "$gpg_ssh_auth_sock" ]]; then
        echo "INFO: gpg-agent was already set"
    fi

    export SSH_AUTH_SOCK="$gpg_ssh_auth_sock"
    _set_socket_permanent
    return 0
}


function lsagent {
    if [[ -z "$(command -v netstat)" ]]; then
        echo "WARNING: netstat does not seem to be installed"
        return 1
    fi

    local netstat_argument
    if [[ "$OSTYPE" = "darwin"* ]]; then
        netstat_argument="-f unix"
    else
        netstat_argument="-lx"
    fi

    local sock
    while read -r sock; do
        # need read and write access to be a valid candidate
        if [[ ! ( -r "$sock" ) ]] || [[ ! ( -w "$sock" ) ]]; then
            continue
        fi

        # CASE 1
        # until now, almost every observed unix socket with ssh in the name
        # was a ssh-agent
        #
        # CASE 2
        # MacOS socket (it does not have ssh in the name)
        
        if [[ "$sock" != *"ssh"* ]] && [[ "$sock" != /private/tmp/com.apple.launchd.*/Listeners ]]; then
            continue
        fi

        if [[ "$sock" = "$SSH_AUTH_SOCK" ]]; then
            echo "> $sock"
        else
            echo "  $sock"
        fi
    done < <(netstat "$netstat_argument" | awk '{ print $NF }' | sort -u)
}


function agent_stop {
    if _is_gpg_agent_set; then
        if [[ -n "$(command -v gpg-connect-agent)" ]]; then
            gpg-connect-agent killagent /bye > /dev/null
            return
        fi
    elif [[ -n "$(command -v ssh-agent)" ]]; then
        if [[ -z "$SSH_AGENT_PID" ]]; then
            echo "ERROR: variable SSH_AGENT_PID is not defined, no way to kill ssh-agent if there is one." >&2
            return 1
        fi

        local agent_output
        if ! agent_output=$(ssh-agent -k); then
            echo "ERROR: failed to stop ssh agent" >&2
            return
        fi
        eval "$agent_output"
        _set_pid_permanent
        return 0
    fi

    echo "ERROR: have no way to kill an agent" >&2
    return 1
}


function agent_reload {
    if _is_gpg_agent_set; then
        if [[ -n "$(command -v gpg-connect-agent)" ]]; then
            ## Version 1
            # gpg-connect-agent reloadagent /bye > /dev/null
            # gpg-connect-agent updatestartuptty /bye > /dev/null

            # Version 2: it looks like the reloadagent does not completly
            # reload the agent in case of trouble, so kill it.
            gpg-connect-agent killagent /bye > /dev/null
            gpg-connect-agent updatestartuptty /bye > /dev/null

            return
        fi
    elif [[ -n "$(command -v ssh-agent)" ]]; then
        if [[ -z "$SSH_AGENT_PID" ]]; then
            echo "ERROR: variable SSH_AGENT_PID is not defined, no way to reload ssh-agent if there is one." >&2
            return 1
        fi

        local agent_output
        if ! agent_output=$(ssh-agent -k); then
            echo "ERROR: failed to stop ssh agent" >&2
            return
        fi

        eval "$agent_output"

        if ! agent_output=$(ssh-agent -s); then
            echo "ERROR: failed to start ssh agent" >&2
            _set_pid_permanent
            return
        fi

        eval "$agent_output"
        _set_pid_permanent
        return 0
    fi

    echo "ERROR: have no way to reload an agent" >&2
    return 1
}


export _SSH_ADD_PATH
_SSH_ADD_PATH="$(command -v ssh-add)"

function agent_start {
    # kick start the gpg-agent to be sure unix socket are created
    # and also set TTY and X11 information to agent
    if _is_gpg_agent_set; then
        if [[ -n "$(command -v gpg-connect-agent)" ]]; then
            gpg-connect-agent updatestartuptty /bye > /dev/null
            return
        fi
    elif [[ -n "$_SSH_ADD_PATH" ]]; then
        # this will only work to wake up agent when managed by gnome-keyring
        # it won't start a new ssh-agent.
        local result
        result=$($_SSH_ADD_PATH -l -q 2>&1)

        if [[ "$result" = "Could not open a connection to your authentication agent." ]]; then
            echo "WARNING: use agent_ssh to start a new ssh-agent."
        fi

        return 0
    fi

    echo "ERROR: have no way to start an agent" >&2
    return 1
}


function ssh-add {
    if [[ -z "$SSH_AUTH_SOCK" ]]; then
        echo "WARNING: variable SSH_AUTH_SOCK is not set. Please pick an agent"
        echo "with agent_gpg or agent_ssh."
        return
    fi

    if _is_gpg_agent_set && [[ -n "$(command -v gpg-add)" ]]; then
        gpg-add "$@"
        return
    fi

    # ssh-add works both with ssh-agent and gpg-agent.
    if [[ -n "$_SSH_ADD_PATH" ]]; then
        $_SSH_ADD_PATH "$@"
        return
    fi

    echo "ERROR: fail to find a tool to add ssh keys." >&2
    return 1
}

function gpg_update_tty {
    if [[ -z "$(command -v gpg-connect-agent)" ]]; then
        echo "WARNING: gpg-connect-agent does not seem to be installed"
        return 1
    fi

    gpg-connect-agent updatestartuptty /bye > /dev/null
}

###############################################################################
### BACKUP SUPPORT ############################################################
###############################################################################

function _gpg_backup_compute_path {
    local create_dir backup_path
    if [[ -z "$1" ]]; then
        create_dir=1
    else
        create_dir=$1
    fi

    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        backup_path="$CUSTOM_CONFIG_DIRECTORY/settings/gnupg_backup"
    else
        backup_path="$HOME/.gnupg_backup"
    fi

    if [[ $create_dir -eq 1 ]] && [[ ! ( -d "$backup_path" ) ]]; then
        mkdir -p "$backup_path"
    fi

    echo "$backup_path"
}


function gpg_backup {
    if [[ -z "$(command -v tar)" ]]; then
        echo "WARNING: tar does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    if [[ "$(tar --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: tar is not the GNU tar."
        return 1
    fi

    local backup_path home_path dir_name

    if [[ -n "$GNUPGHOME" ]]; then
        local temp_home
        temp_home=$(realpath "$GNUPGHOME")
        home_path=$(dirname "$temp_home")
        dir_name=$(basename "$temp_home")
    else
        home_path="$HOME"
        dir_name=".gnupg"
    fi

    if [[ -n "$1" ]]; then
        backup_path="$1"
    else
        backup_path=$(_gpg_backup_compute_path)

        if [[ ! ( -d $backup_path ) ]]; then
            echo "ERROR: backup directory does not exist." >&2
            return 1
        fi

        # backup_path must be writtable
        if [[ ! ( -w $backup_path ) ]]; then
            echo "ERROR: backup directory is not writtable." >&2
            return 1
        fi

        backup_path="$backup_path/$(date '+%d%b%Y_%H%M%S')"
    fi

    echo "directory to backup: $home_path/$dir_name"
    echo "archive path: $backup_path.tar.gz"
    tar zcf "$backup_path.tar.gz" --warning=no-file-ignored -C "$home_path" "$dir_name"
}


function gpg_restore {
    if [[ -z "$(command -v tar)" ]]; then
        echo "WARNING: tar does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    if [[ "$(tar --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: tar is not the GNU tar."
        return 1
    fi

    local backup_path

    if [[ -n "$1" ]] && [[ -e "$1" ]]; then
        if [[ ! ( -f "$1" ) ]] || [[ ! ( -r "$1" ) ]]; then
            echo "ERROR: '$1' is not a file" >&2
            return 1
        fi

        if [[ ! ( -r "$1" ) ]]; then
            echo "ERROR: '$1' is not readable" >&2
            return 1
        fi

        backup_path="$1"
    else
        backup_path=$(_gpg_backup_compute_path)

        if [[ ! ( -d $backup_path ) ]]; then
            echo "ERROR: backup directory does not exist." >&2
            return 1
        fi

        # backup_path must be readable
        if [[ ! ( -r $backup_path ) ]]; then
            echo "ERROR: backup directory is not readable." >&2
            return 1
        fi

        if [[ -n "$1" ]]; then
            if [[ ! ( -f "$backup_path/$1" ) ]]; then
                echo "ERROR: unknown file $1" >&2
                return 1
            fi
            backup_path="$backup_path/$1"
        else
            # default: use the latest archive

            local backup_file
            # shellcheck disable=SC2012
            backup_file=$(ls -t "$backup_path" | head -n 1)

            if [[ -z "$backup_file" ]]; then
                echo "WARNING: no backup file found."
                return 0
            fi

            backup_path="$backup_path/$backup_file"
        fi
    fi
    backup_path=$(realpath "$backup_path")

    local home_path dir_name

    if [[ -n "$GNUPGHOME" ]]; then
        local temp_home
        temp_home=$(realpath "$GNUPGHOME")
        home_path=$(dirname "$temp_home")
        dir_name=$(basename "$temp_home")
    else
        home_path="$HOME"
        dir_name=".gnupg"
    fi

    echo "will use the following backup: $backup_path"

    if [[ -d "$home_path/$dir_name" ]]; then
        echo "$home_path/$dir_name exists, this directory will be overwritten"
        echo "and all its content will be lost."
        read -p "Are you sure you want to continue? [y/n]" -n 1 -r
        echo    # (optional) move to a new line
        if [[ ! ( $REPLY =~ ^[Yy]$ ) ]]; then
            return 0
        fi
    fi

    # check if dir_name exists inside the archive.
    local tar_content
    tar_content=$(tar --list --no-recursion --file="$backup_path" "$dir_name" 2> /dev/null)

    if [[ -z "$tar_content" ]]; then
        echo "ERROR: directory $dir_name is not present inside archive $backup_path" >&2
        return 1
    fi

    # stop agent
    # no need to set homedir, it will use global var GNUPGHOME
    if [[ -n "$(command -v gpg-connect-agent)" ]]; then
        gpg-connect-agent killagent /bye > /dev/null 2>&1
    fi

    rm -rf "${home_path:?}/${dir_name}"
    tar -zxf "$backup_path" -C "$home_path"

    # start agent
    # no need to set homedir, it will use global var GNUPGHOME
    if [[ -n "$(command -v gpg-connect-agent)" ]]; then
        gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1
    fi
}


function lsgpgbackup {
    local backup_path
    backup_path=$(_gpg_backup_compute_path 0)

    if [[ ! ( -d $backup_path ) ]]; then
        return 0
    fi

    ls -1 "$backup_path"
}

###############################################################################
### SANDBOX SUPPORT ###########################################################
###############################################################################

function _generate_gnuphome_override_path {
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        # not an hidden file
        # don't care if settings directory does not exist yet.
        echo "$CUSTOM_CONFIG_DIRECTORY/settings/gnupghome"
    else
        # an hidden file because in HOME
        echo "$HOME/.gnupghome"
    fi
}


function _generate_previous_socket_path {
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        # not an hidden file
        # don't care if settings directory does not exist yet.
        echo "$CUSTOM_CONFIG_DIRECTORY/settings/previous_ssh_socket"
    else
        # an hidden file because in HOME
        echo "$HOME/.previous_ssh_socket"
    fi
}


function _gpg_sandbox_start_usage {
    cat << EOF
Usage: ${0##*/} [-hcs]
Start a gpg sandbox keyring stored inside /tmp/

    -h          display this help and exit
    -c          copy configuration files from $HOME/.gnupg
    -k          copy existing public keys from $HOME/.gnupg
    -p          copy existing public and private keys from $HOME/.gnupg
    -l          local sandobox for this terminal only
EOF
}


function _remove_local_sandbox {
    if [[ -z "$GNUPGHOME" ]]; then
        return 0
    fi

    if [[ "$GNUPGHOME" != "/tmp/"* ]]; then
        return 0
    fi

    local parent_path
    parent_path=$(dirname "$GNUPGHOME")

    if [[ "$parent_path" != *"_gpg_sand_box" ]]; then
        return 0
    fi

    rm -rf "$parent_path"
}


function gpg_sandbox_start {
    if [[ -z "$(command -v gpg)" ]]; then
        echo "WARNING: gpg does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v gpg-connect-agent)" ]]; then
        echo "WARNING: gpg-connect-agent does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v gpgconf)" ]]; then
        echo "WARNING: gpgconf does not seem to be installed"
        return 1
    fi

    local opt copy_conf copy_keyring copy_private_keys local_sandbox

    copy_conf=0
    copy_keyring=0
    copy_private_keys=0
    local_sandbox=0
    OPTIND=1
    while getopts hckpl opt; do
        case $opt in
            h)
                _gpg_sandbox_start_usage
                return 0
                ;;
            c)  copy_conf=1
                ;;
            k)  copy_keyring=1
                ;;
            p)  copy_keyring=1
                copy_private_keys=1
                ;;
            l)  local_sandbox=1
                ;;
            *)
                _gpg_sandbox_start_usage >&2
                return 1
                ;;
        esac
    done
    shift "$((OPTIND-1))"

    # try to run a new global sandbox and another one is already running ?
    if [[ $local_sandbox -eq 0 ]] && [[ -n "$GNUPGHOME" ]]; then
        echo "WARNING: variable GNUPGHOME is not empty.  aborting."
        return 1
    fi

    if [[ $_gpg_sandbox_local = 1 ]]; then
        echo "WARNING: a local sandbox is already running in this terminal.  aborting."
        return 1
    fi

    if [[ -z "$(command -v gpg)" ]]; then
        echo "WARNING: gpg does not seem to be installed."
        return 1
    fi

    if [[ $local_sandbox = 1 ]]; then
        _gpg_sandbox_local=1
    fi

    # create a temp directory
    local temp_directory
    temp_directory=$(mktemp -d --suffix _gpg_sand_box)

    if [[ ! ( -d $temp_directory ) ]]; then
        echo "failed to create temporary directory $temp_directory"
        return 1
    fi

    # with local sandbox, set a trap to remove the sandbox when terminal close
    if [[ $local_sandbox -eq 1 ]]; then
        trap _remove_local_sandbox EXIT 
    fi

    mkdir "$temp_directory/.gnupg"
    temp_directory="$temp_directory/.gnupg"
    chmod 700 "$temp_directory"

    # copying configuration
    if [[ $copy_conf -eq 1 ]]; then
        if [[ -f "$HOME/.gnupg/gpg.conf" ]]; then
            cp "$HOME/.gnupg/gpg.conf" "$temp_directory"
        fi

        if [[ -f "$HOME/.gnupg/gpg-agent.conf" ]]; then
            cp "$HOME/.gnupg/gpg-agent.conf" "$temp_directory"
        fi
    else
        # on old gpg version, it does not create the ssh socket without this
        # line of configuration.
        echo "enable-ssh-support" > "$temp_directory/gpg-agent.conf"
    fi

    # copying the keyring
    if [[ $copy_keyring -eq 1 ]]; then
        if [[ $copy_private_keys -eq 1 ]]; then
            # copy public/private keys
            gpg --export-secret-keys --output "$temp_directory/exported_keys.pgp"
            GNUPGHOME=$temp_directory gpg --import < "$temp_directory/exported_keys.pgp" > /dev/null 2>&1
            rm "$temp_directory/exported_keys.pgp"
        fi

        # copy public keys
        gpg --export --output "$temp_directory/exported_keys.pgp"
        GNUPGHOME=$temp_directory gpg --import < "$temp_directory/exported_keys.pgp" > /dev/null 2>&1
        rm "$temp_directory/exported_keys.pgp"

        # copy trust db
        gpg --export-ownertrust > "$temp_directory/exported_trust.gpg"
        GNUPGHOME=$temp_directory gpg --import-ownertrust < "$temp_directory/exported_trust.gpg" > /dev/null 2>&1
        rm "$temp_directory/exported_trust.gpg"
    fi

    # if local sandbox and GNUPGHOME is alredy set, save it inside a variable
    if [[ -n "$GNUPGHOME" ]] && [[ $local_sandbox -eq 1 ]]; then
        _gpg_sandbox_previous_gnupghome="$GNUPGHOME"
    fi

    # override GNUPGHOME
    # from now the sandbox is enabled.
    export GNUPGHOME
    GNUPGHOME="$temp_directory"

    # initialize keyring inside
    gpg -k > /dev/null 2>&1

    # initialize socket directory
    gpgconf --create-socketdir 2> /dev/null
    # --create-socketdir does not exist on old version, because socket are
    # always created inside GNUPGHOME directory.
    # socket will be created by the first gpg-connect-agent call.

    # kickstart gpg-agent
    gpg-connect-agent updatestartuptty /bye > /dev/null 2>&1

    # store the temp directory path inside a config file if global sanbox
    if [[ $local_sandbox -eq 0 ]]; then
        local gnupghome_override_path
        gnupghome_override_path=$(_generate_gnuphome_override_path)
        echo "$temp_directory" > "$gnupghome_override_path"
    fi

    # save previous ssh socket
    if [[ -n "$SSH_AUTH_SOCK" ]]; then
        if [[ $local_sandbox -eq 0 ]]; then
            local previous_socket_path
            previous_socket_path="$(_generate_previous_socket_path)"
            echo "$SSH_AUTH_SOCK" > "$previous_socket_path"
        else
            _gpg_sandbox_previous_ssh_auth_sock="$SSH_AUTH_SOCK"
        fi
    fi

    # set sanbox ssh socket
    local gpg_ssh_auth_sock
    gpg_ssh_auth_sock=$(gpgconf --list-dirs | grep "agent-ssh-socket" | awk -F: '{print $2;}')

    if [[ -n "$gpg_ssh_auth_sock" ]]; then
        export SSH_AUTH_SOCK
        SSH_AUTH_SOCK="$gpg_ssh_auth_sock"

        if [[ $local_sandbox -eq 0 ]]; then
            _set_socket_permanent
        fi
    elif [[ -S "$temp_directory/S.gpg-agent.ssh" ]]; then
        export SSH_AUTH_SOCK
        SSH_AUTH_SOCK="$temp_directory/S.gpg-agent.ssh"
        if [[ $local_sandbox -eq 0 ]]; then
            _set_socket_permanent
        fi
    else
        echo "WARNING: ssh support not enabled, didn't find the socket file."
    fi
}


function gpg_sandbox_stop {
    if [[ -z "$(command -v gpgconf)" ]]; then
        echo "WARNING: gpgconf does not seem to be installed."
        return 1
    fi

    # is there a sandbox running ?
    if [[ -z "$GNUPGHOME" ]]; then
        echo "WARNING: variable GNUPGHOME is empty.  aborting."
        return 0
    fi

    if [[ "$GNUPGHOME" != *"/tmp/"* ]]; then
        echo "WARNING: variable GNUPGHOME does not contain a /tmp/ path. aborting."
        return 0
    fi

    local parent_path
    parent_path=$(dirname "$GNUPGHOME")

    if [[ "$parent_path" != *"_gpg_sand_box" ]]; then
        echo "WARNING: the current gpg sandbox wasn't create with this tool. aborting."
        return 0
    fi

    # remove socket directory and stop the agent
    if ! gpgconf --homedir "$GNUPGHOME" --remove-socketdir 2> /dev/null; then
        # old gpg version does not manage remove-socketdir action, just kill
        # the agent.
        gpg-connect-agent --homedir "$GNUPGHOME" "KILLAGENT" /bye
    fi

    rm -rf "$parent_path"
    unset GNUPGHOME

    if [[ $_gpg_sandbox_local -eq 1 ]]; then
        trap - EXIT

        # restore previous GNUPGHOME
        if [[ -n "$_gpg_sandbox_previous_gnupghome" ]]; then
            GNUPGHOME="$_gpg_sandbox_previous_gnupghome"
            export GNUPGHOME
        fi
        unset _gpg_sandbox_previous_gnupghome

        # restore previous SSH_AUTH_SOCK
        if [[ -n "$_gpg_sandbox_previous_ssh_auth_sock" ]]; then
            SSH_AUTH_SOCK="$_gpg_sandbox_previous_ssh_auth_sock"
            export SSH_AUTH_SOCK
        fi
        unset _gpg_sandbox_previous_ssh_auth_sock
        unset _gpg_sandbox_local
    else
        # remove config file storing the temp path
        local gnupghome_override_path
        gnupghome_override_path=$(_generate_gnuphome_override_path)

        if [[ -f "$gnupghome_override_path" ]]; then
            rm  "$gnupghome_override_path"
        fi

        # restore previous SSH_AUTH_SOCK if it was overriden by gpg_sandbox_start
        local previous_socket_path
        previous_socket_path="$(_generate_previous_socket_path)"

        if [[ -f "$previous_socket_path" ]]; then
            local previous_socket
            previous_socket=$(cat "$previous_socket_path")

            if [[ -S "$previous_socket" ]]; then
                export SSH_AUTH_SOCK
                SSH_AUTH_SOCK="$previous_socket"
                _set_socket_permanent
            fi

            rm "$previous_socket_path"
        fi
    fi
}

###############################################################################
### CARD SUPPORT ##############################################################
###############################################################################

function gpg_card_fetch {
    {
        echo "fetch"
        echo "quit"
    } | gpg --command-fd 0 --edit-card
}
