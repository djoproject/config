#!/usr/bin/env bash

# Copyright (C) 2021 Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


function docker_ip {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    # no need of sudo for this one
    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$@"
}


function docker_ash () {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    $DOCKER_EXEC run -it --rm alpine /bin/ash
}


function docker_explore () {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    local docker_id
    docker_id="$1"

    if [[ -z "$docker_id" ]]; then
        echo "ERROR: no image id provided." >&2
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    docker exec -t -i "$docker_id" /bin/ash
}


function docker_attach () {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    local docker_id
    docker_id="$1"

    if [[ -z "$docker_id" ]]; then
        echo "ERROR: no image id provided." >&2
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    docker attach --sig-proxy=false "$docker_id"
}


function docker_children () {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    local image_id
    image_id="$1"

    if [[ -z "$image_id" ]]; then
        echo "ERROR: no image id provided." >&2
        return 1
    fi

    if ! [[ "$image_id" =~ ^[0-9a-f]{12}$ ]]; then
        echo "ERROR: not an image id: $image_id" >&2
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    local child_image_id
    for child_image_id in $($DOCKER_EXEC images -q); do
        if [[ "$image_id" = "$child_image_id" ]]; then
            continue
        fi

        if $DOCKER_EXEC history "$child_image_id" | grep -q "$image_id"; then
            local tags
            tags="$($DOCKER_EXEC inspect --format '{{join .RepoTags ", "}}' "$child_image_id")"
            echo "$child_image_id $tags"
        fi
    done
}


function docker_login {
    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    if [[ -z "$(command -v pass)" ]]; then
        echo "WARNING: pass does not seem to be installed"
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    if ! pass show www/ | grep -q "hub.docker.com"; then
        echo "WARNING: no password www/hub.docker.com found"
    fi

    local username password first_line
    first_line=1
    while read -r line; do
        if [[ $first_line -eq 1 ]]; then
            password="$line"
            first_line=0
        else
            if [[ "$line" = "login: "* ]]; then
                username="${line:7}"
                break
            fi
        fi
    done < <(pass "www/hub.docker.com")

    if [[ -z "$username" ]] || [[ -z "$password" ]]; then
        echo "ERROR: username or password is empty"
    fi

    $DOCKER_EXEC login -u "$username" -p "$password" 2> /dev/null
}


function docker_logout {
    # does not really give more process, this function goal is just to stay
    # consistent with function names.

    if [[ -z "$(command -v docker)" ]]; then
        echo "WARNING: docker does not seem to be installed"
        return 1
    fi

    local DOCKER_EXEC
    if groups | grep -q docker; then
        DOCKER_EXEC="docker"
    else
        echo "WARNING: curent user not in docker group."
        DOCKER_EXEC="sudo docker"
    fi

    $DOCKER_EXEC logout
}
