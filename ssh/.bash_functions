#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


function s {
    if [[ -z "$(command -v ssh)" ]]; then
        echo "WARNING: ssh does not seem to be installed"
        return 1
    fi

    ssh "$@"
}


function ssh_range {
    if [[ -z "$(command -v ssh)" ]]; then
        echo "WARNING: ssh does not seem to be installed"
        return 1
    fi

    if [[ "$#" -lt 3 ]]; then
        echo "ERROR: missing args." >&2
        echo "USAGE: ${0##*/} START_RANGE END_RANGE TARGET"
        return 1
    fi

    start="$1"
    end="$2"
    target="$3"

    if [[ $end -lt $start ]]; then
        echo "ERROR: end range must be equal of bigger than start." >&2
        return
    fi

    declare -a ssh_args
    for i in $(seq "$start" "$end") ;do
        ssh_args+=("-L $i:localhost:$i")
    done

    ssh -N "${ssh_args[@]}" "$target"
}
