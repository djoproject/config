#!/usr/bin/env bash

# shellcheck disable=SC2120
function get_main_branch_name {
    local path

    if [[ $# -eq 0 ]]; then
        path="."
    else
        path="$1"
    fi

    if git -C "$path" branch | grep -q "main"; then
        echo "main"
        return 0
    fi

    if git -C "$path" branch | grep -q "master"; then
        echo "master"
        return 0
    fi

    return 1
}


function git_checkout_main {
    local main_branch
    if ! main_branch="$(get_main_branch_name)"; then
        echo "ERROR: no main branch" >&2
        return 1
    fi

    git checkout "$main_branch"
}
alias g_co_main='git_checkout_main'
alias g_co_master='git_checkout_main'


function br {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    branch=$(git branch --show-current 2>&1)

    if [[ "$branch" = *"unknown option"* ]]; then
        # if no comit yet on master, git print a warning indicating the command is ambiguous
        branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)
    fi

    # if not on a branch, print commit hash
    if [[ "$branch" = "HEAD" ]] || [[ -z "$branch" ]]; then
        branch=$(git rev-parse --short HEAD)
    fi

    if [[ -n "$branch" ]]; then
        echo "$branch"
        return 0
    fi

    return 1
}


function g {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    git "$@"
}


function gi {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    git "$@"
}


function git_status {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    local parent_path git_status

    while read -r parent_path; do
        git_status=$(git -C "$parent_path" status)

        if [[ "$parent_path" = "." ]]; then
            parent_path="<current>"
        else
            parent_path=${parent_path:2}
        fi

        if echo "$git_status" | grep -q "have diverged"; then
            echo -e "$parent_path: \e[31mremote and local branches have diverged\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Your branch is ahead of "; then
            echo -e "$parent_path: \e[31mcommit(s) to push\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Changes to be committed"; then
            echo -e "$parent_path: \e[31mnot committed changes\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Changes not staged"; then
            echo -e "$parent_path: \e[33mnot staged changes\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Untracked files"; then
            echo -e "$parent_path: \e[33muntracked files\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Your branch is up to date"; then
            echo -e "$parent_path: \e[32mclean\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "HEAD detached at"; then
            echo -e "$parent_path: \e[31m$(echo "$git_status" | head -n 1)\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Your branch is behind"; then
            if echo "$git_status" | grep -q "and can be fast-forwarded"; then
                echo -e "$parent_path: \e[33mbehind branch (fast forwardable)\e[0m"
            else
                echo -e "$parent_path: \e[31mbehind branch (diverged)\e[0m"
            fi
            continue
        fi

        if echo "$git_status" | grep -q "nothing to commit, working tree clean"; then
            echo -e "$parent_path: \e[33mon local branch\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "No commits yet"; then
            echo -e "$parent_path: \e[32mno commits yet\e[0m"
            continue
        fi

        echo -e "$parent_path: \e[31mUNKOWN STATE\e[0m"
    done < <(find . -type d,l -name ".git" -printf '%h;%d\n' | sort -n | awk -F ';' '{print $1}')
}
alias g_status='git_status'
alias g_st='git_status'


function git_pull {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    local original_parent_path parent_path git_status branch_to_use

    if [[ -n "$1" ]]; then
        branch_to_use="$1"
        echo "INFO: will use branch $branch_to_use in place of branch master."
    fi

    while read -r original_parent_path; do
        if [[ "$original_parent_path" = "." ]]; then
            parent_path="<current>"
        else
            parent_path=${original_parent_path:2}
        fi

        echo -n "$parent_path: "

        if [[ -z "$branch_to_use" ]]; then
            if ! branch_to_use="$(get_main_branch_name)"; then
                echo -e "\e[33mno main branch for project\e[0m"
                continue
            fi
        fi

        # is it on branch_to_use ?
        branch_name=$(git -C "$original_parent_path" rev-parse --abbrev-ref HEAD 2> /dev/null)
        if [[ ! ( "$branch_name" = "$branch_to_use" ) ]]; then
            echo -e "\e[33mnot on $branch_to_use branch\e[0m"
            continue
        fi

        git_status=$(git -C "$original_parent_path" status)

        if echo "$git_status" | grep -q "Your branch is ahead of "; then
            echo -e "\e[33mcommit(s) to push\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Changes to be committed"; then
            echo -e "\e[33mnot committed changes\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "Changes not staged"; then
            echo -e "\e[33mnot staged changes\e[0m"
            continue
        fi

        if echo "$git_status" | grep -q "have diverged"; then
            echo -e "\e[31mremote and local branches have diverged\e[0m"
            continue
        fi

        # fast forward only, the goal is to avoid unecessary merge commit when
        # a remote branch has been force pushed.
        git_output=$(git -C "$original_parent_path" pull --ff-only 2>&1)
        ret_code=$?

        if [[ $ret_code -eq 0 ]]; then
            if [[ "$git_output" = *"Already up to date."* ]]; then
                echo -e "nothing pulled"
            else
                echo -e "\e[32mdone\e[0m"
            fi
        else
            echo -e "\e[31merror $ret_code\e[0m"
            echo
            echo "$git_output"
            echo
        fi
    done < <(find . -type d,l -name ".git" -printf '%h;%d\n' | sort -n | awk -F ';' '{print $1}')
}
alias g_pull='git_pull'

function git_fetch {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    local original_parent_path

    while read -r original_parent_path; do
        if [[ "$original_parent_path" = "." ]]; then
            parent_path="<current>"
        else
            parent_path=${original_parent_path:2}
        fi

        echo "$parent_path"

        git -C "$original_parent_path" fetch 2> /dev/null >&2

    done < <(find . -type d,l -name ".git" -printf '%h;%d\n' | sort -n | awk -F ';' '{print $1}')
}
alias g_fetch='git_fetch'
alias g_f='git_fetch'

function git_checkout {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "ERROR: no branch provided" >&2
        return 1
    fi

    local branch_to_use
    branch_to_use="$1"

    while read -r original_parent_path; do
        if [[ "$original_parent_path" = "." ]]; then
            parent_path="<current>"
        else
            parent_path=${original_parent_path:2}
        fi

        echo -n "$parent_path: "

        if [[ -z "$(git -C "$original_parent_path" branch --list "$branch_to_use")" ]] && [[ -z "$(git -C "$original_parent_path" branch --remote --list "origin/$branch_to_use")" ]]; then
            echo -e "\e[31munknown branch\e[0m"
            continue
        fi

        local current_branch
        current_branch=$(git -C "$original_parent_path" branch --show-current)

        if [[ "$current_branch" = "$branch_to_use" ]]; then
            echo "already on it"
            continue
        fi

        local git_status
        git_status=$(git -C "$original_parent_path" status)

        if echo "$git_status" | grep -q "Changes to be committed"; then
            echo -e "\e[33mnot committed changes\e[0m"
            continue
        fi

        if ! git -C "$original_parent_path" checkout "$branch_to_use" 2>/dev/null >&2; then
            echo -e "\e[31munexpected error\e[0m"
        else
            echo -e "\e[32msuccess\e[0m"
        fi
    done < <(find . -type d,l -name ".git" -printf '%h;%d\n' | sort -n | awk -F ';' '{print $1}')
}
alias g_checkout='git_checkout'
alias g_co='git_checkout'


declare -ga _GIT_BRANCH_COLORS
_GIT_BRANCH_COLORS+=("31")
_GIT_BRANCH_COLORS+=("32")
_GIT_BRANCH_COLORS+=("33")
_GIT_BRANCH_COLORS+=("34")
_GIT_BRANCH_COLORS+=("35")
_GIT_BRANCH_COLORS+=("36")
_GIT_BRANCH_COLORS+=("90")
_GIT_BRANCH_COLORS+=("91")
_GIT_BRANCH_COLORS+=("92")
_GIT_BRANCH_COLORS+=("93")
_GIT_BRANCH_COLORS+=("94")
_GIT_BRANCH_COLORS+=("95")
#put greys at the end
_GIT_BRANCH_COLORS+=("37")
_GIT_BRANCH_COLORS+=("96")


function git_branch {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    declare -a available_colors
    available_colors+=("${_GIT_BRANCH_COLORS[@]}")

    declare -A used_colors

    while read -r original_parent_path; do
        if [[ "$original_parent_path" = "." ]]; then
            parent_path="<current>"
        else
            parent_path=${original_parent_path:2}
        fi

        branch=$(GIT_DIR="$original_parent_path/.git" br)

        echo -n "$parent_path: "
        if [[ -n "${used_colors[$branch]}" ]]; then
            echo -e "\e[${used_colors[$branch]}m$branch\e[0m"
            continue
        fi

        if [[ ${#available_colors[@]} -eq 0 ]]; then
            echo "$branch"
            continue
        fi

        local picked_color
        picked_color="${available_colors[0]}"
        available_colors=("${available_colors[@]:1}")
        used_colors["$branch"]="$picked_color"
        echo -e "\e[${picked_color}m$branch\e[0m"
    done < <(find . -type d,l -name ".git" -printf '%h;%d\n' | sort -n | awk -F ';' '{print $1}')
}
alias g_branch='git_branch'
alias g_br='git_branch'


function _git_push_help {
cat << EOF
usage: ${0##*/} [options]
Options:
  -h            Show this help message
  -f            Force push
EOF
}


function git_push {
    local args
    declare -a args

    while getopts ":hf" opt; do
        case $opt in
            h)
                _git_push_help
                return 0
                ;;
            f)  args+=("-f")
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _git_push_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _git_push_help >&2
                return 1
                ;;
        esac
    done

    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    if git status 2>&1 | grep -q "not a git repository"; then
        echo "ERROR: not a git repository" >&2
        return 1
    fi

    local branch
    branch=$(br)

    # Branches forbidden for push
    for forbidden_branch in "${GIT_FORBIDEN_BRANCHES_FOR_PUSH[@]}"; do
        if [[ "$branch" = "$forbidden_branch" ]]; then
            echo "ERROR: not allowed to use this commande on branch $forbidden_branch." >&2
            return 1
        fi
    done

    # can not be a remote branch
    if [[ "$branch" = "origin/"* ]]; then
        echo "ERROR: not allowed to use this commande on a remote branch." >&2
        return 1
    fi

    # local branch must exist
    if ! git show-ref --verify --quiet "refs/heads/$branch"; then
        echo "ERROR: not on a local branch." >&2
        return 1
    fi

    # staged changes not commited not allowed.
    if git status | grep -q "Changes not staged"; then
        echo "ERROR: not staged changes." >&2
        return 1
    fi

    args+=("origin")
    args+=("$branch")

    # git push manage by itself if there is nothing to push
    # it also does not care about not stagged changes
    git push "${args[@]}"
}
alias g_push='git_push'
alias g_p='git_push'

alias git_pushf='git_push -f'
alias g_pushf='git_push -f'
alias g_pf='git_push -f'


function git_rebase_all {
    if [[ -z "$(command -v git)" ]]; then
        echo "WARNING: git does not seem to be installed"
        return 1
    fi

    if git status 2>&1 | grep -q "not a git repository"; then
        echo "ERROR: not a git repository" >&2
        return 1
    fi

    local branch_to_use current_branch branch

    if [[ -n "$1" ]]; then
        branch_to_use="$1"
        echo "INFO: will use branch $branch_to_use in place of branch master."
    else
        if ! branch_to_use="$(get_main_branch_name)"; then
            echo "ERROR: no main branch" >&2
            return 1
        fi
    fi

    current_branch=$(git rev-parse --abbrev-ref HEAD)

    for branch in $(git branch -l | tr '*' ' '); do
        # skip master
        if [[ "$branch" == "$branch_to_use" ]]; then
            continue
        fi

        git checkout "$branch"

        #stop if any error
        git rebase "$branch_to_use" || return 1
    done

    git checkout "$current_branch"
}
