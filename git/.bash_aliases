#!/usr/bin/env bash

# git alias
if [[ -n "$GITPATH" ]]; then
    alias cdgit='cd $GITPATH'
fi

# git perso alias (if exists)
if [[ -n "$GITPPATH" ]]; then
    alias cdpgit='cd "$GITPPATH"'
fi
