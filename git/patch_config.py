#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from configparser import ConfigParser


def main(original_file, new_file):
    config = ConfigParser()
    config.read(original_file)
    config.read(new_file)

    with open(original_file, 'w') as configfile:
        config.write(configfile)


if __name__ == "__main__":
    import argparse
    import sys
    assert sys.version_info >= (3, 2), "Script requires Python 3.2+."

    parser = argparse.ArgumentParser(description="Patch an existing config file")
    parser.add_argument('original_file', type=str, help='the existing file to patch')
    parser.add_argument('new_file', type=str, help='the new file to merge into the existing one')
    args = parser.parse_args()

    exit(main(args.original_file, args.new_file))
