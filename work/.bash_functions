#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function _getwpath {
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        local setting_path
        setting_path="$CUSTOM_CONFIG_DIRECTORY/settings"

        if [[ ! ( -d "$setting_path" ) ]]; then
            mkdir -p "$setting_path"
        fi

        echo "$setting_path/work_dir"  # not an hidden file
    else
        echo "$HOME/.work_dir"  # an hidden file because in HOME
    fi
}


function setw {
    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    local new_working_dir
    new_working_dir=$1

    if [[ -z "$new_working_dir" ]]; then
        new_working_dir=.
    fi

    if [[ ! ( -d "$new_working_dir" ) ]]; then
        echo "ERROR: not a directory" >&2
        return 2
    fi

    new_working_dir=$(realpath "$new_working_dir")

    # is this in work directory ?
    local work_directory
    work_directory=$(realpath "$HOME/work")
    if [[ ! ( "$new_working_dir" = "$work_directory/"* ) ]]; then
        echo "ERROR: not in $work_directory"  >&2
        return 1
    fi

    # is this in todo|doing|done directory ?
    if [[ ! ( "$new_working_dir" =~ ^$HOME/work/(todo|doing|done)/.*$ ) ]]; then
        echo "ERROR: not in one of the work subdirectory todo, doing, or done."  >&2
        return 1
    fi

    # compute top level directory 
    local parent_new_working_dir
    while [[ ! ( "$new_working_dir" =~ ^$HOME/work/(todo|doing|done)$ ) ]]; do
        parent_new_working_dir=$(dirname "$new_working_dir")

        if [[ "$parent_new_working_dir" = "/" ]]; then
            echo "ERROR: reached root directory." >&2
            return 1
        fi

        if [[ "$parent_new_working_dir" =~ ^$HOME/work/(todo|doing|done)$ ]]; then
            break
        fi

        new_working_dir="$parent_new_working_dir"
    done

    # extract existing saved working directory and compare.
    local work_dir_path working_directory
    work_dir_path=$(_getwpath)
    if [[ -f "$work_dir_path" ]]; then
        working_directory=$(cat "$work_dir_path")

        if [[ -d "$working_directory" ]]; then
            if [[ "$working_directory" = "$new_working_dir" ]]; then
                echo "no change: $new_working_dir"
                return 0
            fi

            echo "previous was: $working_directory"
        fi
    fi

    # save changes if needed
    echo "current: $new_working_dir"
    echo "$new_working_dir" > "$work_dir_path"
    return 0
}


function getw {
    local work_dir_path
    work_dir_path=$(_getwpath)
    if [[ ! ( -f "$work_dir_path" ) ]]; then
        echo "WARNING: no working directory defined"
        return 1
    fi

    local working_directory
    working_directory=$(cat "$work_dir_path")

    if [[ ! ( -d "$working_directory" ) ]]; then
        echo "ERROR: not a working directory" >&2
        rm "$work_dir_path"
        return 2
    fi

    echo "$working_directory"
    return 0
}


function cdw {
    local work_dir_path
    work_dir_path=$(_getwpath)
    if [[ ! ( -f "$work_dir_path" ) ]]; then
        echo "WARNING: no working directory defined"
        return 1
    fi

    local working_directory
    working_directory=$(cat "$work_dir_path")

    if [[ ! ( -d "$working_directory" ) ]]; then
        echo "ERROR: not a working directory" >&2
        rm "$work_dir_path"
        return 2
    fi

    cd "$working_directory" || return
}


function _check_work_directories {
    if [[ ! ( -d $HOME/work ) ]]; then
        echo "creating $HOME/work directory"
        mkdir "$HOME/work" || { echo "ERROR: fail to create $HOME/work directory" >&2; return 1;}
    fi

    if [[ ! ( -d $HOME/work/todo ) ]]; then
        echo "creating $HOME/work/todo directory"
        mkdir "$HOME/work/todo" || { echo "ERROR: fail to create $HOME/work/todo directory" >&2; return 2;}
    fi

    if [[ ! ( -d $HOME/work/doing ) ]]; then
        echo "creating $HOME/work/doing directory"
        mkdir "$HOME/work/doing" || { echo "ERROR: fail to create $HOME/work/doing directory" >&2; return 3;}
    fi

    if [[ ! ( -d $HOME/work/done ) ]]; then
        echo "creating $HOME/work/done directory"
        mkdir "$HOME/work/done" || { echo "ERROR: fail to create $HOME/work/done directory" >&2; return 4;}
    fi

    return 0
}


function _wmove {
    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    # check $home/work/* directories
    if ! _check_work_directories;then
        return $?
    fi

    # get current directory
    local current_directory original_directory
    current_directory=$(pwd)
    current_directory=$(realpath "$current_directory")
    original_directory=$current_directory

    # is this in work directory ?
    local work_directory
    work_directory=$(realpath "$HOME/work")
    if [[ ! ( "$current_directory" = "$work_directory/"* ) ]]; then
        echo "ERROR: not in $work_directory"  >&2
        return 1
    fi

    # is this already in place ?
    local destination_directory
    destination_directory=$(realpath "$2")
    if [[ "$current_directory" = "$destination_directory/"* ]]; then
        echo "WARNING: already in $destination_directory"  >&2
        return 0
    fi

    # identify source_directory
    if [[ -n "$1" ]]; then
        local source_directory
        source_directory=$(realpath "$1")
    else
        local doing_directory done_directory
        doing_directory=$(realpath "$HOME/work/doing")
        done_directory=$(realpath "$HOME/work/done")

        local source_directory
        if [[ "$current_directory" = "$doing_directory/"* ]]; then
            source_directory=$doing_directory
        elif [[ "$current_directory" = "$done_directory/"* ]]; then
            source_directory=$done_directory
        else
            echo "ERROR: failed to find current directory parent." >&2
            return 1
        fi
    fi

    # can't use master directory todo/doing/done as work directory
    if [[ "$current_directory" = "$source_directory" ]]; then
        echo "ERROR: not in a working directory."  >&2
        return 1
    fi

    # current directory is not a subdirectory of todo/doing/done
    if [[ ! ( "$current_directory" = "$source_directory/"* ) ]]; then
        echo "ERROR: not in $source_directory"  >&2
        return 1
    fi

    # need to get the upper directory bellow todo/doing/done
    local parent_current_directory base_current_directory
    while [[ "$current_directory" != "$source_directory" ]]; do
        parent_current_directory=$(dirname "$current_directory")
        base_current_directory=$(basename "$current_directory")

        if [[ "$parent_current_directory" = "/" ]]; then
            echo "ERROR: reached root directory." >&2
            return 1
        fi

        if [[ "$parent_current_directory" = "$source_directory" ]]; then
            break
        fi

        current_directory="$parent_current_directory"
    done

    # ugly but only way to update PWD correctly.
    cd "$source_directory" || { echo "fail to move to $source_directory"; return 1;}
    mv "$base_current_directory" "$destination_directory"
    original_directory="${original_directory/$source_directory/}"
    cd "$destination_directory/${original_directory:1}" || { echo "fail to move to $destination_directory/${original_directory:1}"; return 1;}

    # update work_dir file
    local work_dir_path
    work_dir_path=$(_getwpath)
    if [[ -f "$work_dir_path" ]]; then
        local working_directory
        working_directory=$(cat "$work_dir_path")
    else
        return
    fi

    if [[ "$current_directory" = "$working_directory" ]]; then
        realpath . > "$work_dir_path"
    fi
}


function wstart {
    _wmove "$HOME/work/todo" "$HOME/work/doing"
}


function wend {
    _wmove "$HOME/work/doing" "$HOME/work/done"
}
alias wdone='wend'


function wtodo {
    _wmove "" "$HOME/work/todo"
}


function wcreate {
    if ! _check_work_directories; then
        return $?
    fi

    if [[ -z "$1" ]]; then
        echo "ERROR: no name provided." >&2
        return 1
    fi

    local new_name
    new_name="$1"

    # check if new_name is a name and not a path
    if [[ $(basename "$new_name") != "$new_name" ]]; then
        echo "ERROR: provided name looks like a path.  need a single name." >&2
        return 1
    fi

    if [[ -d "$HOME/work/todo/$new_name" ]]; then
        echo "WARNING: already exists."
        cd "$HOME/work/todo/$new_name" || return 1
        return 0
    fi

    if mkdir "$HOME/work/todo/$new_name"; then
        cd "$HOME/work/todo/$new_name" || return 1
    fi
}
