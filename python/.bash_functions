#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function pclear {
    find . -name "*~" -exec rm -f {} \; 2> /dev/null
    find . -name "*.pyc" -exec rm -f {} \; 2> /dev/null
    find . -name "__pycache__" -exec rm -rf {} \; 2> /dev/null
    find . -name ".cache" -exec rm -rf {} \; 2> /dev/null
    find . -name ".Dstore" -exec rm -f {} \; 2> /dev/null
    find . -name "*.mo" -exec rm -f {} \; 2> /dev/null
    find . -name ".coverage" -exec rm -f {} \; 2> /dev/null
    find . -name ".pytest_cache" -exec rm -rf {} \; 2> /dev/null
}


function _pip_upgrade {
    if [[ -z "$(command -v jq)" ]]; then
        echo "ERROR: jq is not installed and is needed." >&2
        return 1
    fi

    local PYTHON_EXEC SKIP_LIST_FILENAME
    PYTHON_EXEC=$1

    if [[ -z "$PYTHON_EXEC" ]]; then
        echo "ERROR: got an empty string as pythn executable." >&2
        return 1
    fi

    SKIP_LIST_FILENAME="$HOME/$2"

    echo "Using: $PYTHON_EXEC"
    echo "Ignore file: $SKIP_LIST_FILENAME"

    if [[ -f "$SKIP_LIST_FILENAME" ]]; then
        echo "Ignore file exist: YES"
    else
        echo "Ignore file exist: NO"
    fi

    declare -a args
    args+=("-m")
    args+=("pip")
    args+=("install")

    if [[ -z "$VIRTUAL_ENV" ]]; then
        echo "Virtual env: None"
        args+=("--user")
    else
        echo "Virtual env: $VIRTUAL_ENV"
    fi

    args+=("--upgrade")

    echo

    echo "Upgrade pip..."
    if ! $PYTHON_EXEC "${args[@]}" pip; then
        echo "ERROR: fail to upgrade pip." >&2
        return 1
    fi

    echo "Done."
    echo

    echo "Retrieving package to update..."
    local package_to_upgrade_json
    package_to_upgrade_json=$($PYTHON_EXEC -m pip list --outdated --format=json)
    echo "Done."
    echo

    local json_filter
    if [[ -f "$SKIP_LIST_FILENAME" ]]; then
        json_filter=$(jq -R . "$SKIP_LIST_FILENAME" | jq -s .)
    else
        json_filter="[]"
    fi

    local package_to_update_count_before_filter package_to_update package_to_update_count_after_filter
    package_to_update_count_before_filter=$(echo "$package_to_upgrade_json" | jq '. | length')
    package_to_update=$(echo "$package_to_upgrade_json" | jq --argjson ids "$json_filter" 'map( select( .name as $id | $ids | index($id) | not ))')
    package_to_update_count_after_filter=$(echo "$package_to_update" | jq '. | length')

    # nothing-to-do check
    if [[ $package_to_update_count_after_filter -eq 0 ]]; then
        echo "nothing to do."
        return 0
    fi

    local ignored_count
    ignored_count="$((package_to_update_count_before_filter - package_to_update_count_after_filter))"
    echo "$ignored_count package(s) ignored"

    echo
    echo "The following package will be updated:"
    echo "$package_to_update" | jq -r 'sort_by(.name) | .[] | "\(.name) \(.version) => \(.latest_version)"'

    echo
    echo "<!> THIS ACTION MAY BREAK MANY SOFTWARES <!>"
    read -p "Do you want to continue? [y/n]" -n 1 -r
    echo    # (optional) move to a new line
    if [[ ! ( $REPLY =~ ^[Yy]$ ) ]]; then
        return 0
    fi

    local package_name
    while read -r package_name; do
        $PYTHON_EXEC "${args[@]}" "$package_name"
    done < <(echo "$package_to_update" | jq -r 'sort_by(.name) | .[] | .name')

    return 0
}


function pip3_upgrade {
    # shellcheck disable=SC2153    
    if [[ -z "$_PYTHON3_EXEC" ]]; then
        echo "WARNING: no python3 executable defined"
        return 1
    fi

    _pip_upgrade "$_PYTHON3_EXEC" ".pip3_update_ignore"
}


function pip3_install {
    if [[ -z "$_PYTHON3_EXEC" ]]; then
        echo "WARNING: no python3 executable defined"
        return 1
    fi

    declare -a args
    args+=("-m")
    args+=("pip")
    args+=("install")

    echo "Using: $_PYTHON3_EXEC"

    if [[ -z "$VIRTUAL_ENV" ]]; then
        echo "Virtual env: None"
        args+=("--user")
    else
        echo "Virtual env: $VIRTUAL_ENV"
    fi

    args+=("--upgrade")
    $_PYTHON3_EXEC "${args[@]}" "$@"
}


function pip2_upgrade {
# shellcheck disable=SC2153
    if [[ -z "$_PYTHON2_EXEC" ]]; then
        echo "WARNING: no python2 executable defined"
        return 1
    fi

    _pip_upgrade "$_PYTHON2_EXEC" ".pip_update_ignore"
}


function pip2_install {
    if [[ -z "$_PYTHON2_EXEC" ]]; then
        echo "WARNING: no python2 executable defined"
        return 1
    fi

    declare -a args
    args+=("-m")
    args+=("pip")
    args+=("install")

    echo "Using: $_PYTHON2_EXEC"

    if [[ -z "$VIRTUAL_ENV" ]]; then
        echo "Virtual env: None"
        args+=("--user")
    else
        echo "Virtual env: $VIRTUAL_ENV"
    fi

    args+=("--upgrade")

    $_PYTHON2_EXEC "${args[@]}" "$@"
}


function pip_upgrade {
    if [[ -z "$_MAIN_PYTHON_EXEC" ]]; then
        echo "WARNING: no python executable defined"
        return 1
    fi

    _pip_upgrade "$_MAIN_PYTHON_EXEC" ".pip3_update_ignore"
}


function pip_install {
    if [[ -z "$_MAIN_PYTHON_EXEC" ]]; then
        echo "WARNING: no python executable defined"
        return 1
    fi

    declare -a args
    args+=("-m")
    args+=("pip")
    args+=("install")

    echo "Using: $_MAIN_PYTHON_EXEC"

    if [[ -z "$VIRTUAL_ENV" ]]; then
        echo "Virtual env: None"
        args+=("--user")
    else
        echo "Virtual env: $VIRTUAL_ENV"
    fi

    args+=("--upgrade")

    $_MAIN_PYTHON_EXEC "${args[@]}" "$@"
}
