#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# python tools
if [[ -n "$_MAIN_PYTHON_EXEC" ]]; then
    alias venv='$_MAIN_PYTHON_EXEC -m virtualenv'
    alias p='$_MAIN_PYTHON_EXEC'
    alias pt='$_MAIN_PYTHON_EXEC -m pytest'
    alias ap8='$_MAIN_PYTHON_EXEC -m autopep8'
    alias python='$_MAIN_PYTHON_EXEC'
fi

# python2 tools
if [[ -n "$_PYTHON2_EXEC" ]]; then
    alias p2='$_PYTHON2_EXEC'
    alias python2='$_PYTHON2_EXEC'
    alias pt2='$_PYTHON2_EXEC -m pytest'
fi

# python3 tools
if [[ -n "$_PYTHON3_EXEC" ]]; then
    alias p3='$_PYTHON3_EXEC'
    alias python3='$_PYTHON3_EXEC'
    alias pt3='$_PYTHON3_EXEC -m pytest'
fi

alias py2_check_test="PYTHON_EXECUTABLE=python2 py_check_test"
alias py3_check_test="PYTHON_EXECUTABLE=python3 py_check_test"

alias pytc="py_test_coverage"
alias py2_tc="PYTHON_EXECUTABLE=\"python2\" py_test_coverage"
alias py3_tc="PYTHON_EXECUTABLE=\"python3\" py_test_coverage"

alias pytdev="py_test_dev"
alias py2_tdev="PYTHON_EXECUTABLE=\"python2\" py_test_dev"
alias py3_tdev="PYTHON_EXECUTABLE=\"python3\" py_test_dev"
