#!/usr/bin/env bash
#
# Copyright (C) 2021  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

EASYRSA_DIR="$VPN_DIRECTORY/EasyRSA-$VPN_EASY_RSA_VERSION"


function main_help {
    cat << EOF
usage: ${0##*/} [options] NAME

This script create a new user OpenVPN certificate.

Options:
  -h            Show this help message
  -d            EasyRSA directory (default: $EASYRSA_DIR)
  -s            Create a server certificate (default: user certificate)
EOF
}


function pre_check {
    local easyrsa_dir name
    easyrsa_dir="$1"
    name="$2"

    if ! [[ -d "$easyrsa_dir" ]]; then
        echo "ERROR: directory $easyrsa_dir does not exist" >&2
        exit 1
    fi

    if [[ -f "$easyrsa_dir/pki/private/ca.key" ]]; then
        echo "WARNING: not allowed on a CA installation. aborting."
        exit 0
    fi

    if [[ -z "$name" ]]; then
        echo "ERROR: no user provided" >&2
        exit 1
    fi

    if [[ -z "$VPN_CA_HOSTNAME" ]]; then
        echo "WARNING: CA hostname not defined.  Sending signature request aborted."
        exit 0
    fi

    if [[ -z "$VPN_CA_USER" ]]; then                                               
        echo "WARNING: CA hostname not defined.  Sending signature request aborted."
        exit 0                                                                         
    fi

    if [[ -z "$VPN_CA_DIRECTORY" ]]; then
        echo "WARNING: CA hostname not defined.  Sending signature request aborted."
        exit 0
    fi

}


function client_create {
    local name
    name="$1"
    ./easyrsa gen-req "$name"

    # sending request to CA
    scp "./pki/reqs/${name}.req" "$VPN_CA_USER@$VPN_CA_HOSTNAME:/tmp"

    echo
    echo "Go to CA server and sign request for user ${name}..."
    read -p "Press ANY key when it is done." -n 1 -r

    scp "$VPN_CA_USER@$VPN_CA_HOSTNAME:$VPN_CA_DIRECTORY/pki/issued/${name}.crt" ./pki/issued
}


function server_create {
    local name
    name="$1"

    if ! [[ -f "./pki/dh.pem" ]]; then
        ./easyrsa gen-dh
    fi

    if ! [[ -f "./pki/private/ta.key" ]]; then
        openvpn --genkey --secret "./pki/private/ta.key"
    fi

    ./easyrsa gen-req "$name" nopass

    # sending request to CA
    scp "./pki/reqs/${name}.req" "$VPN_CA_USER@$VPN_CA_HOSTNAME:/tmp"

    echo
    echo "Go to CA server and sign request for server ${name}..."
    read -p "Press ANY key when it is done." -n 1 -r

    # retrieve certificates from CA
    mkdir -p "./pki/issued"
    scp "$VPN_CA_USER@$VPN_CA_HOSTNAME:$VPN_CA_DIRECTORY/pki/issued/${name}.crt" ./pki/issued
    scp "$VPN_CA_USER@$VPN_CA_HOSTNAME:$VPN_CA_DIRECTORY/pki/ca.crt" ./pki
    scp "$VPN_CA_USER@$VPN_CA_HOSTNAME:$VPN_CA_DIRECTORY/pki/crl.pem" ./pki
}


function main {
    local server easyrsa_dir
    server=0
    easyrsa_dir="$EASYRSA_DIR"

    while getopts ":hds" opt; do
        case $opt in
            h)
                main_help
                exit 0
                ;;
            s)
                server=1
                ;;
            d)
                easyrsa_dir="$OPTARG"
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                main_help >&2
                exit 1
                ;;
            *)
                echo "unknown option -- $1"
                main_help >&2
                exit 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    name="$1"
    pre_check "$easyrsa_dir" "$name"

    cd "$easyrsa_dir" || exit 1

    if [[ $server -eq 1 ]]; then
        server_create "$name"
    else
        client_create "$name"
    fi
}

main "$@"
