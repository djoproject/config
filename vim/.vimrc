"### Built-in ################################################################"

"Enable syntax highlighning"
syntax on

"Disable visual mode on mouse selection"
set mouse-=a

"Highligh search"
set hlsearch

"The width of a TAB is set to 4."
set tabstop=4

"Indents will have a width of 4"
set shiftwidth=4

"Sets the number of columns for a TAB"
set softtabstop=4

"Expand TABs to spaces"
set expandtab

"Allow hidden buffer with not yet saved modification"
set hidden

"Set lin length marker"
set colorcolumn=80

"Print line number"
set number

"### Light Line ##############################################################"

"Enable light line"
set laststatus=2

"Remove show mode to avoid duplicated information with lightline"
set noshowmode

"Integrate fugitive into lightline"
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }

"### NERDTree ################################################################"

"NERDTree shortcut"
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

"Show hidden files with NERDTree"
let NERDTreeShowHidden=1

"### Golang-vim ##############################################################"

"finish to install/enable go-vim"
"see: https://github.com/fatih/vim-go/blob/master/doc/vim-go.txt"
filetype plugin indent on

"Run go imports on save"
let g:go_fmt_command = "goimports"

"### Rust-vim ################################################################"
let g:rustfmt_autosave = 1

"### Logging functions #######################################################"

func! Logging(level, message)
    let l:timestamp = strftime('%a, %d %b %Y %H:%M:%S %z')
    let l:procces_id = getpid()
    let l:formated_message = l:timestamp . " " . l:procces_id . " " . a:level . " " . a:message
    call writefile([l:formated_message], expand("~/.vim/events.log"), "a")
endfunction


func! Debug(message)
    call Logging("DEBUG", a:message)
endfunction


func! Info(message)
    call Logging("INFO", a:message)
endfunction


func! Warning(message)
    call Logging("WARN", a:message)
endfunction


func! Error(message)
    call Logging("ERROR", a:message)
endfunction

"### Event subfunction functions #############################################"

let g:session_enabled = 1


func! IsSessionNeededInFrontOfArguments()
  let l:arg_count = argc()
  for arg_id in range(0, l:arg_count - 1)
    let l:arg_value = argv(arg_id)

    if l:arg_value =~# '.git/MERGE_MSG$'
        call Info("This is a git merge message edition, disabling session.")
        let g:session_enabled = 0
    elseif l:arg_value =~# '.git/COMMIT_EDITMSG$'
        call Info("This is a git commit message edition, disabling session.")
        let g:session_enabled = 0
    elseif l:arg_value =~# '^\/dev\/shm\/pass\..*\.txt$' || l:arg_value =~# '^\/private\/var\/folders\/.*\/pass\..*\.txt$'
        call Info("This is a password-store file edition, disabling session.")
        let g:session_enabled = 0
    elseif l:arg_value =~# '\.ansible\/tmp\/ansible-local.*$'
        call Info("This is an ansible vault edition, disabling session.")
        let g:session_enabled = 0
    endif

    call Debug("Arg " . arg_id . ": " . l:arg_value)
  endfor

  if l:arg_count == 0
    call Debug("No arg provided.")
  endif
endfunction


func! GoToLastArgumentFile()
    " NOTE 1: does not work when used through event SessionLoadPost.

    let l:current_buf_id = bufnr('%')
    let l:highest_buf_id = l:current_buf_id

    call Debug("GoToLastArgumentFile (current=" . l:current_buf_id . ", highest=" . l:highest_buf_id . ")")

    for buf_id in range(1, bufnr('$'))
        call Debug("   id=<" . buf_id . ">, exists=<" . bufexists(buf_id) . ">, loaded=<" . bufloaded(buf_id) . ">")
        if bufexists(buf_id) && l:current_buf_id != buf_id && bufloaded(buf_id)
            let l:highest_buf_id = buf_id
        endif
    endfor

    call Debug("    highest: " . l:highest_buf_id)
    if l:highest_buf_id < l:current_buf_id
        call Info("    set buffer: " . l:highest_buf_id)
        execute ":buffer ".l:highest_buf_id
    endif
    call Debug("GoToLastArgumentFileEND")
endfunction

"### Event main functions ###################################################"

func! AtStartUp()
    call Debug("AtStartUp")
    call IsSessionNeededInFrontOfArguments()

    "If no session is provided, try to open the default. "
    if g:session_enabled == 0
        call Info("Session disabled, no loading")
    elseif v:this_session != ''
        call Info("Use provided file " . v:this_session)
        call GoToLastArgumentFile()
    elseif filereadable(expand("~/.vim/sessions/default.vim"))
        call Info("Use default file " . expand("~/.vim/sessions/default.vim"))
        exec "source ~/.vim/sessions/default.vim"
        call GoToLastArgumentFile()
    else
        call Info("No session file used.")
    endif
    call Debug("AtStartUpEND")
endfunc


func! AtExit()
    call Debug("AtExit")

    "Remove any args provided to vim or pushed through remote action."
    %argdel

    "If there was a opened session, save it at exit."
    if g:session_enabled == 0
        call Info("Session disabled, no saving")
    elseif v:this_session != ''
        call Info("Save session to " . v:this_session)
        exec "mks! " . v:this_session
    else
        call Info("Create session to ~/.vim/sessions/default.vim")
        set sessionoptions-=curdir
        exec "mks! ~/.vim/sessions/default.vim"
    endif
    call Debug("AtExitEND")
    call Info("------------------------------------------------------")
endfunc


func! OnBufEnter()
    call Debug("OnBufEnter")
    "Restore the cursor at the saved position while entering an existing buffer"
    "src: https://stackoverflow.com/questions/40984988"
    silent! normal! g`"

    "Exit Vim if NERDTree is the only window left.
    " if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree()
    "    quit
    " endif
    call Debug("OnBufEnterEND")
endfunc


autocmd VimEnter * :call AtStartUp()
autocmd VimLeavePre * :call AtExit()
autocmd BufEnter * :call OnBufEnter()

