function! OpenVimPyServer()
    if !has('python3')
        echo "No python detected, server won't  start."
        return
    endif

    if !exists("g:session_socket_path") || g:session_socket_path == ''
        if v:this_session == ''
            echo "No session defined, can not build socket path from it"
            return
        endif

        let g:session_socket_path = fnamemodify(v:this_session, ":h") . "/S." . fnamemodify(v:this_session, ":t")
    endif

python3 << endpython
# inspired from: 
# https://github.com/degyves/VimPyServer/blob/master/plugin/VimPyServer.vim
from logging import INFO
from logging import basicConfig
from logging import error
from logging import getLogger
from logging import info
from logging import warning
from os.path import expanduser
from os.path import exists
from os import remove
from socket import AF_UNIX
from socket import SOCK_STREAM
from socket import socket
from vim import command
from _thread import start_new_thread

_LOGGER = getLogger(__name__)

def accept_command(command):
    if command.startswith("edit "):
        return True

    return False


def run_server():
    while True:
        # Receive incoming connection
        try:
            clientConnection, _ = server.accept()
        except Exception as e:
            error("Failed to accept client connection: {0}".format(str(e)))
            continue

        # Process command
        try:
            text_command = clientConnection.recv(1024)
            text_command = text_command.decode("UTF-8")
            text_command = text_command.strip()
            if accept_command(text_command):
                info("Execute command: {0}".format(text_command))
                command(text_command)
            else:
                warning("Command discarded: {0}".format(text_command))
        except Exception as e:
            error("Failed to process client command: {0}".format(str(e)))
            continue

        # Close connection
        try:
            clientConnection.close()
        except:
            pass  # Do not care about error on close.


def start_server():
    global server

    # does server already exists ?
    try:
        server
        warning("Server already exists")
        return
    except NameError:
        pass

    # process socket path
    try:
        socket_path = vim.eval("g:session_socket_path")
    except vim.error:
        error("Global variable `g:session_socket_path` is not defined")
        return

    if socket_path is None or len(socket_path) == 0:
        return

    if exists(socket_path):
        remove(socket_path)

    # start server
    try:
        server = socket(AF_UNIX, SOCK_STREAM)
        server.bind(socket_path)
        server.listen(1) # Only 1 connection allowed.
        start_new_thread( run_server  ,() )
        info("Starting server...")
    except Exception as e:
        error("Failed to start server: {0}".format(str(e)))

basicConfig(level=INFO, filename=expanduser("~/.vim/python.log"))
start_server()
endpython
endfunction

function! CleanSocketServer()
    if exists("g:session_socket_path") && filewritable(g:session_socket_path)
        call delete(g:session_socket_path)
    endif
endfunction


autocmd VimEnter * call OpenVimPyServer()
autocmd VimLeavePre * :call CleanSocketServer()
