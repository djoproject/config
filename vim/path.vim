func! GetGoPath(file_path)
    " This function compute if a file path is located bellow one of the path
    " contained into the GOPATH env variable.
    " If this is the case, it returns the first identified path.
    " It returns an empty string otherwise.

    if empty($GOPATH)
        return ""
    endif

    let l:gopath = $GOPATH

    " split function does not preserve empty items"
    let l:gopath_splitted = split(l:gopath, ":")

    for gopath_item in l:gopath_splitted
        let l:gopath_item_expanded = expand(gopath_item)

        if ! isdirectory(l:gopath_item_expanded)
            continue
        endif

        if a:file_path =~# "^" . l:gopath_item_expanded
            return l:gopath_item_expanded
        endif
    endfor

    return ""
endfunc


func! SmartPath()
    " This function tries to remove obvious part of the current buffer path
    " to contract it at its shortest form.
    "
    " It returns a shortened form of the path, or the full path if no
    " contraction was found.

    let l:current_buffer_path = expand("%:p")
    let l:home_path = expand("~")
    let l:current_working_path = getcwd()

    " Is it in the current directory ? then just print the suffix path
    if l:home_path != l:current_working_path && l:current_buffer_path =~# "^" . l:current_working_path . "/"
        return "[" . fnamemodify(l:current_working_path, ':t') . "] " . strpart(l:current_buffer_path, strlen(l:current_working_path)+1, strlen(l:current_buffer_path)-1)
    endif

    " Is it in the GOPATH directories ? then replace gopath part with GOPATH
    let l:gopath_dir = GetGoPath(l:current_buffer_path)
    if strlen(l:gopath_dir) > 0
        return "GOPATH" . strpart(l:current_buffer_path, strlen(l:gopath_dir), strlen(l:current_buffer_path)-1)
    endif

    " Is it in the HOME directory ? then replace home directory prefix with ~
    if l:current_buffer_path =~# "^" . l:home_path
        return "~" . strpart(l:current_buffer_path, strlen(l:home_path), strlen(l:current_buffer_path)-1)
    endif

    " return full path
    return l:current_buffer_path
endfunction


func! GetOccupiedSpace()
    let l:mode_length = strlen(lightline#mode()) + 2
    let l:file_info_length = strlen(&filetype) + strlen(&fileformat) + strlen(&encoding) + 8
    let l:percentage_length = 6
    let l:line_column_length_length = 8  " TODO
    let l:file_space = 2

    let l:modifier_length = 0
    if &modified
        let l:modifier_length = 4
    endif

    let l:readonly_length = 0
    if &readonly
        let l:readonly_length = 5
    endif

    let l:git_branch_length = strlen(FugitiveHead())
    if l:git_branch_length > 0
        let l:git_branch_length += 3
    endif

    let l:total_length = l:mode_length + l:git_branch_length + l:file_space + l:modifier_length + l:readonly_length + l:file_info_length + l:percentage_length + l:line_column_length_length

    return l:total_length

endfunction


func! ShortenPath()
    " This function adapt the printed path to fit the current window size.
    " It removes as many as needed character to fit the available space
    " in the light line path area
    "
    " It returns an empty string if there is not enough space to print
    " anything.

    let l:current_buf_id = bufnr('%')
    let l:current_width = winwidth(0)

    " Event 1: buf id change
    " Event 2: width change
    " Event 3: FIXME it must also be changed if &modified flag flap.
    " Event 4: FIXME it must also be changed if current working directory change
    if exists("g:current_buf_id") && exists("g:current_width") && exists("g:current_buffer_path")
        if l:current_buf_id == g:current_buf_id && l:current_width == g:current_width
            return g:current_buffer_path
        endif
    endif

    let l:path = SmartPath()

    " Do not save empty path
    if strlen(l:path) == 0
        unlet! g:current_buf_id
        unlet! g:current_width
        unlet! g:current_buffer_path

        return ""
    endif

    let l:width = winwidth(0) - GetOccupiedSpace()  " 45
    let l:path_width = strlen(l:path)

    if l:path_width <= l:width
        let g:current_buffer_path = l:path
    elseif l:width < 4
        let g:current_buffer_path = ""
    else
        let g:current_buffer_path = "... " . l:path[ l:path_width - l:width + 4 : l:path_width ]
    endif

    let g:current_buf_id = l:current_buf_id
    let g:current_width = l:current_width

    return g:current_buffer_path
endfunction

if exists("g:lightline['active']['left'][1][2]") || g:lightline['active']['left'][1][2] == "filename"
    let g:lightline['active']['left'][1][2] = "smartpath"
    let g:lightline['component_function'].smartpath = "ShortenPath"
endif
