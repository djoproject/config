#!/usr/bin/env bash

# Copyright (C) 2021  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function ve {
    if [[ -z "$(command -v nc)" ]]; then
        echo "ERROR: nc does not seem installed" >&2
        return 1
    fi

    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    if ! [[ -S "$HOME/.vim/sessions/S.default.vim" ]]; then
        echo "ERROR: socket not defined." >&2
        return 1
    fi

    if [[ $# -eq 0 ]]; then
        echo "ERROR: no argument provided." >&2
        return 1
    fi

    local arg
    for arg in "$@"; do
        if ! [[ -f "$arg" ]]; then
            echo "ERROR: path provided is not a file" >&2
            continue
        fi

        local path
        path="$(realpath "$arg")"

        echo "edit $path" | nc -U "$HOME/.vim/sessions/S.default.vim"
    done
}

