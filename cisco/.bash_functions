#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function cisco_generate_password {
    if [[ -z "$(command -v openssl)" ]]; then
        echo "WARNING: openssl does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "ERROR: specify a user as first argument" >&2
        return 1
    fi

    local password
    if [[ -z "$2" ]]; then
        password=$(openssl rand -base64 18)
    else
        password="$2"
    fi

    local salt hashed
    salt=$(openssl rand -base64 3)
    hashed=$(openssl passwd -salt "$salt" -1 "$password")
    echo "username $1 privilege 15 secret 5 $hashed"
    echo "password: $password"
}


function cisco_set_clock {
    date "+clock set %T %b %d %Y"
}
