#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

##############################################################################
### MISC ALIASES #############################################################
##############################################################################

if [[ -n "$DISPLAY" ]]; then
    if [[ -n "$(command -v pluma)" ]]; then
        function _start_editor {
            (pluma "$@" 2> /dev/null &)
            # disown
        }

        alias gedit='_start_editor'
        alias e="_start_editor"
    elif [[ -n "$(command -v vim)" ]]; then
        alias e='vim'
    fi
# MacOS does not set DISPLAY variable
elif [[ -x "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" ]]; then
    alias subl='/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl'
    alias gedit='subl'
    alias e='subl'
elif [[ -n "$(command -v vim)" ]]; then
    alias e='vim'
elif [[ -n "$(command -v vi)" ]]; then
    alias e='vi'
fi

alias todo='grep -Isnr "TODO" * .[^.]*'
alias todof='grep -Islr "TODO" * .[^.]*'
alias todou='grep -Isnr "TODO($USER)" * .[^.]*'
alias todouf='grep -Islr "TODO($USER)" * .[^.]*'
alias fixme='grep -Isr "FIXME" * .[^.]*'
alias fixmef='grep -Islr "FIXME" * .[^.]*'

if [[ "$OSTYPE" = "darwin"* ]]; then
	if [[ -n "$(command -v port)" ]]; then
	alias package-update='sudo port selfupdate && sudo port upgrade outdated'
	fi
elif [[ -n "$(command -v apt)" ]]; then
    if [[ -n "$(command -v sudo)" ]]; then
        alias package-update='sudo apt update && sudo apt dist-upgrade && sudo apt autoremove'
    else
        # termux does not have sudo command but use apt
        alias package-update='apt update && apt dist-upgrade && apt autoremove'
    fi
fi

# allow sudo to use aliases as command.
alias sudo='sudo '

alias k='clear'

##############################################################################
### NETWORK ALIASES ##########################################################
##############################################################################

alias pingg='ping -c 5 dns.google'
alias pingg6='ping6 -c 5 dns.google'
alias ping8='ping -c 5 8.8.8.8'

if [[ -n "$(command -v ip)" ]]; then
    alias ip='ip --color'
    alias ip4='ip -4'
    alias ip6='ip -6'
fi

alias pubip='curl -s https://api.ipify.org;echo'
alias pubip6='curl -6 -s https://api6.ipify.org;echo'
alias mtr='mtr -t -b'

##############################################################################
### LS ALIASES ###############################################################
##############################################################################

# define some environment variable for BSD ls
if [[ "$OSTYPE" = "darwin"* ]] && false; then
    if [[ -z "$LSCOLORS" ]]; then
        export LSCOLORS=ExFxBxDxCxegedabagacad
    fi

    if [[ -z "$CLICOLOR" ]] || [[ "$CLICOLOR" = "0" ]]; then
        export CLICOLOR=1
    fi
fi

alias grep='grep --color=auto'
alias ll='ls -AlFh'
alias la='ls -A'
alias l='ls -CF'

##############################################################################
### CD ALIASES ###############################################################
##############################################################################

if [[ -n "$DOWNLOAD_DIR" ]]; then
    alias cdd='cd $DOWNLOAD_DIR'
fi
