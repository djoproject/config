#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

##############################################################################
### MISC #####################################################################
##############################################################################

function ls {
    # ls -G has not the same meaning with BSD ls and GNU ls
    if [[ "$(command ls --version 2>&1)" = *GNU* ]]; then
        command ls --color=auto "$@"
    else
        command ls -G "$@"
    fi
}


function c {
    if [[ -z "$(command -v code)" ]]; then
        echo "WARNING: vscode does not seem to be installed"
        return 1
    fi

    code "$@"
}


function compress_dir {
    if [[ -z "$(command -v tar)" ]]; then
        echo "WARNING: tar does not seem to be installed"
        return 1
    fi

    if [[ "$(tar --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: tar is not the GNU tar."
        return 1
    fi

    tar zcf "$1.tar.gz" "$1"
}


function uncompress_dir {
    if [[ -z "$(command -v tar)" ]]; then
        echo "WARNING: tar does not seem to be installed"
        return 1
    fi

    if [[ "$(tar --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: tar is not the GNU tar."
        return 1
    fi

    tar -zxf "$@"
}


function check_locales {
    echo "Selected locales into /etc/locale.gen"
    grep -v "^#" /etc/locale.gen | grep -v "^$"
    echo
    echo "Variables status:"
    echo "LANGUAGE: $LANGUAGE"
    echo "LANG: $LANG"
    echo "LC_ALL: $LC_ALL"
    echo
    echo "####################################################################"
    echo
    echo "Locales used by these variables should be selected inside the file"
    echo "/etc/locale.gen"
    echo
    echo "Two scenario:"
    echo "1) The wanted locale is not enabled into the file."
    echo "   Edit the file, uncomment the needed locales, comment the unwanted"
    echo "   locales.  then run one of the suggested commands to build the"
    echo "   locales.  Don't forget to restart the terminal at the end."
    echo
    echo "2) The wanted locale is already enabled.  Maybe they need to be"
    echo "   generated.  Run one of the suggested commands then restart the"
    echo "   terminal.  If the wrong locale are still selected in the"
    echo "   variables, try to force them to the right value inside .bashrc"
    echo
    echo "Suggested command:"
    echo "  sudo locale-gen"
    echo "  dpkg-reconfigure locales"
    echo "  # HINT: dpkg-reconfigure is interactive and calls locale-gen"
}


function scr {
    if [[ -z "$(command -v shellcheck)" ]]; then
        echo "WARNING: shellcheck does not seem to be installed"
        return 1
    fi

    if [[ $# -ne 0 ]]; then
        echo "WARNING: argument are ignored by scr,"
    fi

    local path
    while read -r path; do
        # skip gitlab ci job
        if [[ "$path" = *".gitlab-ci.yml" ]]; then
            continue
        fi

        # skip vim swap file
        if [[ "$path" = *".swp" ]]; then
            continue
        fi

        shellcheck --exclude=SC1117 "$path"
    # done < <(grep -Flr "#!/usr/bin/env bash" ./*)
    done < <(find . -type f -print0 | xargs -r0 grep -Esrl --exclude=.gitlab-ci.yml "env (ba|a)?sh")
}


function sc {
    if [[ -z $(command -v shellcheck) ]]; then
        echo "WARNING: shellcheck does not seem to be installed"
        return 1
    fi

    shellcheck "$@"
}


function srm {
    if [[ -z "$(command -v shred)" ]]; then
        echo "WARNING: shellcheck does not seem to be installed"
        return 1
    fi

    shred -zvu -n 5 "$@"
}


function tl {
    if [[ -z "$(command -v telnet)" ]]; then
        echo "WARNING: telnet does not seem to be installed"
        return 1
    fi

    telnet 127.0.0.1 "$@"
}


function catpy {
    # print color schems: pygmentize -L styles

    if [[ -z "$(command -v pygmentize)" ]]; then
        echo "WARNING: pigments does not seem to be installed"
        return 1
    fi

    # TODO find option list and description
    # Available options:
    #   * style
    #   * linenos
    #   * full

    pygmentize -O 'style=monokai' -g "$@"
}


function shut {
    if [[ -z "$(command -v shutdown)" ]]; then
        echo "WARNING: shutdown does not seem to be installed"
        return 1
    fi

    sudo shutdown -h now
}


function t {
    if [[ -z "$(command -v telnet)" ]]; then
        echo "WARNING: telnet does not seem to be installed"
        return 1
    fi

    telnet "$@"
}


function ping64 {
    ping6 "64:ff9b::$1"
}

function gr {
    grep -Inr "$1" * .[^.]*
}

# gr specialised for rust project.
function rgr {
    grep -Inr --exclude-dir=target* --exclude-dir=pgdata --exclude-dir=.git --exclude-dir=.git "$1" * .[^.]*
}

##############################################################################
### INIT #####################################################################
##############################################################################

function init_directories {
    if [[ ! ( -d "$HOME/development" ) ]]; then
        mkdir "$HOME/development"
        local ret_value=$?

        if [[ ! ( $ret_value -eq 0 ) ]]; then
            echo "ERROR: failed to create directory $HOME/development" >&2
            return $ret_value
        fi
    fi

    if [[ ! ( -d "$HOME/development/git" ) ]]; then
        mkdir "$HOME/development"
        local ret_value=$?

        if [[ ! ( $ret_value -eq 0 ) ]]; then
            echo "ERROR: failed to create directory $HOME/development/git" >&2
            return $ret_value
        fi
    fi
}

##############################################################################
### OBJECT LISTING/SHOWING ###################################################
##############################################################################


function path {
    if [[ -z "$(command -v realpath)" ]]; then
        echo "WARNING: realpath does not seem to be installed"
        return 1
    fi

    if [[ -z "$1" ]]; then
        echo "ERROR: need an argument " >&2
        return 1
    fi

    local obj_name result
    obj_name="$1"
    result="$(command -v "$obj_name")"

    # not found
    if [[ -z "$result" ]]; then
        echo "WARNING: unknown object $obj_name"
        return 0
    fi

    # alias
    if [[ "$result" = "alias "* ]]; then
        echo "$result"
        return 0
    fi

    # file
    if [[ -f "$result" ]]; then
        realpath "$result"
        return 0
    fi

    # function
    if [[ "$obj_name" = "$result" ]]; then
        echo "function $obj_name"
        return 0
    fi

    echo "ERROR: unknown object type $obj_name" >&2
    echo "$result"
    return 1
}
