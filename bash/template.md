# Bash template

## Indexed array

### Creation
```bash
declare -a array
```

### Length
```bash
${#aray[@]}
```

### Pop
```bash
array=("${array[@]:1}")
```

### Append
```bash
array+=("value")
```

### Copy an array
```bash
array=("${array[@]}")
```


## Associative array
[Nice article about associative array](https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/)

### Creation
```bash
declare -A assoc_array
```

### Get all keys
```bash
${!assoc_array[@]}
```

### Key exists method 1
```bash
[[ ${assoc_array[foo]+_} ]]
```

### Key exists method 2
```bash
[[ -z "${assoc_array[foo]}" ]]
```

### Count entries
```bash
${#assoc_array[@]}
```

### Set item
```bash
assoc_array[foo]=bar
```

### Get item
```bash
${assoc_array[foo]}
```

### Get last element
```bash
${assoc_array[-1]}
```

### Looping on keys
```bash
for K in "${!assoc_array[@]}"; do echo $K; done
```

### Looping on values
```bash
for V in "${assoc_array[@]}"; do echo $V; done
```

## String

### Upper case (all)
```bash
${var^^}
```

### Upper case (title)
```bash
${var^}
```

### Lower case
```bash
${var,,}
```

### Split string
```bash
IFS=':'; read -ra parsed_var <<< "$var"; unset IFS;
```

## File

### Read line by line
```bash
while IFS= read -r line; do
    echo "$line"
done < "$path"
```

## Process 

### Iterate over process output

```bash
while read -r line; do
    echo "$line"
done < <(process_command)
```

## Args management

### Helper
```bash
function _help {
    cat << EOF
usage:./SCRIPT_NAME.sh [options]
Do a thing

Options:
  -h            Getting help
  -e            enable
  -n NAME       Pick a name (default: $DEFAULT)
EOF
}
```

### Main
```bash
    local name enable
    enable=0

    unset OPTIND
    while getopts ":he:n" opt; do
        case $opt in
            h)
                _help
                return 0
                ;;
            n) name="$OPTARG"
                ;;
            e) enable=1
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

```
