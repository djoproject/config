# Normal version
alias cb="cargo build"
alias cbi="cargo build --message-format short"
alias cr="cargo run"
alias cc="cargo clean"
alias ct="RUST_MIN_STACK=8000000 RUST_BACKTRACE=1 cargo test --frozen --all-targets --all-features"
alias cl="cargo clippy --frozen --all-targets --all-features"
alias cf="cargo fmt --all"

# Nightly version
alias cnb="cargo +nightly build --target-dir ./target-nightly"
alias cnbi="cargo +nightly build --target-dir ./target-nightly --message-format short"
alias cnr="cargo +nightly run --target-dir ./target-nightly"
alias cnc="cargo +nightly clean --target-dir ./target-nightly"
alias cnt="RUST_MIN_STACK=8000000 RUST_BACKTRACE=1 cargo +nightly test --target-dir ./target-nightly --frozen --all-targets --all-features"
alias cnl="cargo +nightly clippy --target-dir ./target-nightly --all-targets --all-features"
alias cnf="cargo +nightly fmt --all"

# rust-analyser
alias cac="cargo clean --target-dir ./target-rust-analyser"
