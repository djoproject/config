#!/usr/bin/env bash

# Copyright (C) 2023  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function _venv_ls_help {
    cat << EOF
usage:venv_ls [options]
List virtual environment in their order of appearance in VENV_PATH variable.

Options:
  -h            Getting help
  -m            Only list env that matches current directory
EOF
}

function venv_ls {
    local match
    match=0

    unset OPTIND
    while getopts ":hm" opt; do
        case $opt in
            h)
                _venv_ls_help
                return 0
                ;;
            m)  match=1
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _venv_create_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _venv_create_help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    local current current_name
    current="$(pwd)"
    current_name="$(basename "$current")"

    IFS=':'
    # shellcheck disable=SC2153
    read -ra path_unorder <<< "$VENV_PATH"
    unset IFS

    # while read -r directory; do
    for directory in "${path_unorder[@]}"; do
        if ! [[ -d "$directory" ]]; then
            continue
        fi

        for path in "$directory"/*; do
            if [[ -f "$path/.project" ]]; then
                target="$(cat "$path/.project")"

                if [[ $match -eq 1 ]]; then
                    if [[ "$current" = "$target" ]]; then
                        echo "$path"
                    fi
                    continue
                fi

                echo -e "$path\n    => $target"
            else
                local name
                name="$(basename "$path")"

                if [[ $match -eq 1 ]] && [[ "$name" != "$current_name"* ]]; then
                    continue
                fi

                echo "$path"
            fi
        done
    # done < <(IFS=$'\n'; sort -u <<<"${path_unorder[*]}"; unset IFS;)
    done
}
alias lsvenv="venv_ls"

function _venv_create_help {
    cat << EOF
usage:venv_create [options]
Create a virtual environment.

Options:
  -h            Getting help
  -n            Environment name
  -a            Attach to current directory
  -d            Don't activate on creation
EOF
}

function venv_create {
    echo "Using: $_MAIN_PYTHON_EXEC"

    local name attach activate
    name=""
    attach=0
    activate=1

    unset OPTIND
    while getopts ":hn:ad" opt; do
        case $opt in
            h)
                _venv_create_help
                return 0
                ;;
            n)  name=$OPTARG
                ;;
            a)  attach=1
                ;;
            d)  activate=0
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _venv_create_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _venv_create_help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    if [[ -z "$name" ]]; then
        if [[ $attach -eq 0 ]]; then
            echo "ERROR: need a name for this venv or to be attached to the current directory" >&2
            return 1
        fi

        local current path_hash
        current="$(pwd)"
        path_hash="$(echo "$current" | md5sum | cut -f1 -d" ")"
        name="$(basename "$current")-${path_hash:0:8}"

        # no need for a .project file containing the path.  basename-hash contains the same info.
        attach=0
    fi

    if [[ -z "$name" ]]; then
        echo "ERROR: no name provided." >&2
        return 1
    fi

    local path
    path="$HOME/.venv/$name"

    if [[ -e "$path" ]]; then
        echo "ERROR: $name already exists." >&2
        return 1
    fi

    if ! $_MAIN_PYTHON_EXEC -m venv "$path"; then
        echo "ERROR: failed to create venv." >&2
        return 1
    fi

    if [[ $attach -eq 1 ]]; then
        echo "$(pwd)" > "$path/.project"
    fi

    if [[ $activate -eq 1 ]]; then
        _venv_inner_activate "$path"
    fi
}
alias v_c="venv_create"

function _venv_inner_activate {
    venv_path="$1"
    
    if ! [[ -f "$venv_path/bin/activate" ]]; then
        echo "ERROR: no activate file found in $venv_path" >&2
        return 1
    fi

    echo "INFO: using $venv_path, activating..."

    # shellcheck disable=SC1091
    source "$venv_path/bin/activate"

    if [[ -n "$(command -v python_compute_variables)" ]]; then
        python_compute_variables
    fi
}

function venv_activate {
    local current_directory current_directory_name
    current_directory="$(pwd)"
    current_directory_name="$(basename "$current_directory")"

    # generate basename-hash variable
    local current path_hash current_directory_name_hash
    current="$(pwd)"
    path_hash="$(echo "$current" | md5sum | cut -f1 -d" ")"
    current_directory_name_hash="$(basename "$current")-${path_hash:0:8}"

    IFS=':'; read -ra splitted_path <<< "$VENV_PATH"; unset IFS;
    for directory in "${splitted_path[@]}"; do
        if ! [[ -d "$directory" ]]; then
            continue
        fi

        # look for .project file
        for path in "$directory"/*; do
            if [[ -f "$path/.project" ]]; then
                local target
                target="$(cat "$path/.project")"

                if [[ "$target" = "$current_directory" ]]; then
                    _venv_inner_activate "$path"
                    return $?
                fi
            fi
        done

        # look for basename-hash match
        for path in "$directory"/*; do
            local target
            target="$(basename "$path")"

            if [[ "$target" = "$current_directory_name_hash" ]]; then
                _venv_inner_activate "$path"
                return $?
            fi
        done

        # look for basename match
        for path in "$directory"/*; do
            local target
            target="$(basename "$path")"

            if [[ "$target" = "$current_directory_name" ]]; then
                _venv_inner_activate "$path"
                return $?
            fi
        done

        # look for poetry shit
        # inspired from https://stackoverflow.com/a/76913112/1720475
        poetry_hash=$(python -c "import hashlib; import os; import base64; print(base64.urlsafe_b64encode(hashlib.sha256(os.getcwd().encode('utf-8')).digest()).decode()[:8])")
        for path in "$directory"/*; do
            local target
            target="$(basename "$path")"
            # TODO replace the wildcard * with the version python (e.g. -py3.10)
            #   XXX where does that version come from ?
            if [[ "$target" = "$current_directory_name-$poetry_hash"* ]]; then
                _venv_inner_activate "$path"
                return $?
            fi
        done

    done
    
    echo "WARNING: no venv match."
    return 1
}
alias v="venv_activate"

function venv_deactivate {
    if [[ -z "$(command -v deactivate)" ]]; then
        echo "ERROR: no deactivate command available, are you sure to be in a venv ?" >&2
        return 1
    fi
    deactivate
}
alias d="venv_deactivate"
