#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


function _lspath_help {
    cat << EOF
usage: lspath [options]
List the content of a PATH variable.

Options:
  -h            Getting help
  -o            Order output by priority
  -f            File where are stored dynamic paths (default: custom_path)
  -n            Variable name to manage (default: PATH)
EOF
}

function _lspath_print_line {
    local line custom_path_path
    line="$1"
    custom_path_path="$2"

    custom_path=$(grep -s "^$line$" "$custom_path_path" 2> /dev/null)

    if [[ -n "$custom_path" ]]; then
        echo "(DYNAMIC) $line"
    else
        echo "(STATIC)  $line"
    fi
}

function lspath {
    local order varname custom_path_file
    order=0
    varname="PATH"
    custom_path_file="custom_path"

    unset OPTIND
    while getopts ":hof:n:" opt; do
        case $opt in
            h)
                _lspath_help
                return 0
                ;;
            o)  order=1
                ;;
            n)  varname=$OPTARG
                ;;
            f)  custom_path_file=$OPTARG
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _lspath_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _lspath_help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    # parsing variable
    local path_unorder custom_path_path
    IFS=':'; read -ra path_unorder <<< "${!varname}"; unset IFS;

    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        custom_path_path="$CUSTOM_CONFIG_DIRECTORY/settings/custom_path"
    else
        custom_path_path="$HOME/.custom_path"
    fi

    # printing
    local line
    if [[ $order -eq 1 ]]; then  # Ordered by priority
        for line in "${path_unorder[@]}"; do
            _lspath_print_line "$line" "$custom_path_path"
        done
    else # Ordered by alphanumeric
        while read -r line; do
            _lspath_print_line "$line" "$custom_path_path"
        done < <(IFS=$'\n'; sort -u <<<"${path_unorder[*]}"; unset IFS;)
    fi
}

function _path_add_help {
    cat << EOF
usage: path_add [options] PATH
Add a path entry to a PATH variable.

Options:
  -h            Getting help
  -f            File where are stored dynamic paths (default: custom_path)
  -n            Variable name to manage (default: PATH)
EOF
}

function path_add {
    # Requirements
    if [[ -z "$(command -v realpath)" ]]; then
        echo "ERROR: realpath does not seem to be installed"
        return 1
    fi

    # Arguments
    local varname custom_path_file
    varname="PATH"
    custom_path_file="custom_path"

    unset OPTIND
    while getopts ":hf:n:" opt; do
        case $opt in
            h)
                _path_add_help
                return 0
                ;;
            n)  varname=$OPTARG
                ;;
            f)  custom_path_file=$OPTARG
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _path_add_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _path_add_help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    # no arg
    if [[ -z "$1" ]]; then
        echo "ERROR: no directory provided" >&2
        return 1
    fi

    local directory
    directory=$(realpath "$1")

    # arg is not a directory
    if [[ ! ( -d "$directory" ) ]]; then
        echo "ERROR: not a directory: $directory" >&2
        return 1
    fi

    # trim the / at the end if there is one
    if [[ "$directory" = *"/" ]] && [[ "$directory" != "/" ]]; then
        directory=${directory::-1}
    fi

    local custom_path_path
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        custom_path_path="$CUSTOM_CONFIG_DIRECTORY/settings"
        mkdir -p "$custom_path_path"
        custom_path_path="$custom_path_path/$custom_path_file"
    else
        custom_path_path="$HOME/.$custom_path_file"
    fi

    # directory already exist in file
    if [[ -f "$custom_path_path" ]]; then
        local part_line_number
        part_line_number=$(grep -F -n "$directory" "$custom_path_path" | head -n 1 | cut -d: -f1)
        if [[ -n "$part_line_number" ]]; then
            echo "INFO: already exists"
            return 0
        fi
    fi

    # if directory already exist in varname, no need to add.
    if [[ ${!varname} = *"$directory"* ]]; then
        echo "INFO: already existing in ${!varname} variable."
        return 0
    fi

    # update file
    echo "$directory" >> "$custom_path_path"

    # udpate varname
    declare -gx "$varname"="$directory:${!varname}"

    echo "INFO: added."
}

function _path_remove_help {
    cat << EOF
usage: path_remove [options] PATH
Remove a path entry from a PATH variable.

Options:
  -h            Getting help
  -f            File where are stored dynamic paths (default: custom_path)
  -n            Variable name to manage (default: PATH)
EOF
}

function path_remove {
    # Requirements
    if [[ -z "$(command -v realpath)" ]]; then
        echo "ERROR: realpath does not seem to be installed"
        return 1
    fi

    if [[ "$(sed --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: sed is not the GNU sed."
        return 1
    fi

    # Arguments
    local varname custom_path_file
    varname="PATH"
    custom_path_file="custom_path"

    unset OPTIND
    while getopts ":hf:n:" opt; do
        case $opt in
            h)
                _path_remove_help
                return 0
                ;;
            n)  varname=$OPTARG
                ;;
            f)  custom_path_file=$OPTARG
                ;;
            ?)
                echo "unknown option -- ${1:1:1}"
                _path_remove_help >&2
                return 1
                ;;
            *)
                echo "unknown option -- $1"
                _path_remove_help >&2
                return 1
                ;;
        esac
    done

    # Discard the options and sentinel --
    shift "$((OPTIND-1))"

    # no arg
    if [[ -z "$1" ]]; then
        echo "ERROR: no directory provided" >&2
        return 1
    fi

    local custom_path_path
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        custom_path_path="$CUSTOM_CONFIG_DIRECTORY/settings/$custom_path_file"
    else
        custom_path_path="$HOME/.$custom_path_file"
    fi

    # does the path file exist ?
    if [[ ! ( -f "$custom_path_path" ) ]]; then
        echo "INFO: no custom_path file, nothing to remove."
        return 0
    fi

    local directory
    directory=$(realpath "$1")

    # trim the / at the end if there is one
    if [[ "$directory" = *"/" ]] && [[ "$directory" != "/" ]]; then
        directory=${directory::-1}
    fi

    # directory does not exist in file
    local part_line_number
    part_line_number=$(grep -F -n "$directory" "$custom_path_path" | head -n 1 | cut -d: -f1)
    if [[ -z "$part_line_number" ]]; then
        echo "INFO: directory not in custom_path file."
        return 0
    fi

    # remove it from path file
    sed -i -e "${part_line_number}d" "$custom_path_path"

    # if file is empty, remove it.
    local line_count
    line_count=$(wc -l "$custom_path_path" | awk '{ print $1 }')

    if [[ $line_count -eq 0 ]]; then
        rm "$custom_path_path"
    fi

    # remove directory from varname
    if [[ ${!varname} = *"$directory"* ]]; then
        local WORK
        #src: https://stackoverflow.com/a/2108540/1720475
        WORK=:${!varname}: # WORK => :/bin:/opt/a dir/bin:/sbin:
        WORK=${WORK/:$directory:/:} # WORK => :/bin:/sbin:
        WORK=${WORK%:}
        WORK=${WORK#:}
        declare -gx "$varname"="$WORK"
    fi

    echo "INFO: removed."
}
