#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# TODO (long term) manage multi line descriptions.

# TODO FIXME if an object is removed, its description never disappear
#   To reproduce:
#       * add a description to a variable
#       * install config
#       * remove description
#       * install config again (no really needed)
#       * do lsvar
#       
#       The description is still there.

# TODO check if objects exist before to print them
#   Add an argument to warn if an object does not exist and should.

function _documentation_help {
    cat << EOF
usage: $1 [options] [PREFIX]

$1 list every $2 defined in this bash instance.  If PREFIX is provided, only
print $2 starting with PREFIX.

Options:
  -h    Show this help message
  -d    Show $2 with description if any.
  -n    Show $2 without description
  -c    Show $2 content
  -e    Exact PREFIX match
  -a    Show all (including those starting with '_')
  -b    Show Shell variables
EOF
}

__DOCUMENTATION_FLAGS_ERROR=1
__DOCUMENTATION_FLAGS_SHOW_DESCRIPTION=2
__DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION=4
__DOCUMENTATION_FLAGS_SHOW_CONTENT=8
__DOCUMENTATION_FLAGS_SHOW_BASH=32
__DOCUMENTATION_FLAGS_SHOW_HIDDEN=64
__DOCUMENTATION_FLAGS_EXACT_PREFIX=128


function _documentation_args {
    local OPTIND func_name obj_name flags show_count
    OPTIND=1
    func_name=$1
    obj_name=$2
    flags=0
    show_count=0
    shift 2
    while getopts ":hdnceiab" opt; do
        case $opt in
            d) flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION))
                show_count=$((show_count + 1))
                ;;
            n) flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION))
                show_count=$((show_count + 1))
                ;;
            c) flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_CONTENT))
                # with script/function, possible to print description at top
                if [[ "$obj_name" != "functions" ]] && [[ "$obj_name" != "scripts" ]]; then
                    show_count=$((show_count + 1))
                fi
                ;;
            e) flags=$((flags | __DOCUMENTATION_FLAGS_EXACT_PREFIX))
                ;;
            a) flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_HIDDEN))
                flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_BASH))
                ;;
            b) flags=$((flags | __DOCUMENTATION_FLAGS_SHOW_BASH))
                ;;
            h|?|*)
                ! [[ ${1:1:1} =~ h|\?  ]] && echo -e "unknown option -- ${1:1:1}\\n"
                _documentation_help "$func_name" "$obj_name" >&2
                return 1
                ;;
        esac
    done
    shift "$((OPTIND-1))"   # Discard the options and sentinel --

    if [[ $show_count -gt 1 ]]; then
        echo "ERROR: only one show option at a time." >&2
        return 1
    fi

    # Print prefix filter
    echo "$@"

    return $flags
}


function _documentation_load_description {
    # Load descriptions
    local description_file
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        description_file="$CUSTOM_CONFIG_DIRECTORY/documentation/$1_description"
    else
        description_file="$HOME/.$1_description"
    fi
    
    local key value line
    while IFS= read -r line; do
        key=$(echo "$line" | cut -d ' ' -f1)
        value=$(echo "$line" | cut -d ' ' -f2-)
        __element_descriptions[key]=$value
    done < "$description_file"
}

function _documentation_generate_aliases {
    # Load descriptions
    local description_file
    if [[ -n "$CUSTOM_CONFIG_DIRECTORY" ]]; then
        description_file="$CUSTOM_CONFIG_DIRECTORY/documentation/alias_to_description"
    else
        description_file="$HOME/.alias_to_description"
    fi
    
    if ! [[ -f "$description_file" ]]; then
        return 0
    fi

    local final_string al line first
    final_string=""
    first=0

    while read -r line; do
        al=$(echo "$line" | cut -d ' ' -f1)
        if [[ $first -eq 0 ]]; then
            final_string="$al"
            first=1
        else
            final_string="$final_string $al"
        fi
    done < <(grep "$1$" "$description_file")

    echo "$final_string"
}

function lsvariable {
    # manage arguments
    local flags filter
    filter=$(_documentation_args "lsvariable" "variables" "$@")
    flags=$?
    [[ $(( flags & __DOCUMENTATION_FLAGS_ERROR )) -ne 0 ]] && return 1

    # load descriptions
    local -A _variable_descriptions
    if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]] || [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
        # shellcheck disable=SC2178
        declare -n __element_descriptions=_variable_descriptions
        _documentation_load_description "variable"
        unset -n __element_descriptions
    fi

    # Load bash variables
    local line current_path shell_variables
    if ! [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_BASH )) -ne 0 ]]; then
        current_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
        declare -A shell_variables
        while IFS= read -r line; do
            shell_variables["$line"]=1
        done < "$current_path/shell_variables"
    fi

    # print variables
    local variable_name
    while read -r variable_name; do
        if [[ -z "$variable_name" ]]; then
            continue
        fi

        if [[ "$variable_name" = "flags" ]] || [[ "$variable_name" = "filter" ]] || [[ "$var_name" = "_variable_descriptions" ]] || [[ "$variable_name" = "line" ]] || [[ "$variable_name" = "current_path" ]] || [[ "$variable_name" = "variable_name" ]] || [[ "$variable_name" = "shell_variables" ]]; then
            continue
        fi

        if [[ ${shell_variables[$variable_name]+_} ]];then
            continue
        fi

        if [[ -n "$filter" ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_EXACT_PREFIX )) -ne 0 ]]; then
                [[ "$variable_name" != "$filter" ]] && continue
            else
                [[ "$variable_name" != "$filter"* ]] && continue
            fi
        else
            if [[ "$variable_name" = "_"* ]] && [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_HIDDEN )) -eq 0 ]]; then
                continue
            fi
        fi

        if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_variable_descriptions[$variable_name]+_} ]]; then
                echo "$variable_name: ${_variable_descriptions[$variable_name]}"
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_variable_descriptions[$variable_name]+_} ]]; then
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_CONTENT )) -ne 0 ]]; then
            declare -p "$variable_name"
            continue
        fi

        echo "$variable_name"

    # do not use printenv to list variable, it only shows exported variables.
    done < <(compgen -v | sort)
}
alias lsvar='lsvariable'


function showvariable {
    if [[ -z "$1" ]]; then
        echo "ERROR: need a varible name" >&2
        return 1
    fi

    lsvariable -ce "$1"
}
alias showvar='showvariable'


function lsfunction {
    if [[ "$(sed --version 2>&1)" != *GNU* ]]; then
        echo "ERROR: sed is not the GNU sed."
        return 1
    fi

    # manage arguments
    local flags filter
    filter=$(_documentation_args "lsfunction" "functions" "$@")
    flags=$?
    [[ $(( flags & __DOCUMENTATION_FLAGS_ERROR )) -ne 0 ]] && return 1

    # load descriptions
    local -A _function_descriptions
    if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]] || [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
        # shellcheck disable=SC2178
        declare -n __element_descriptions=_function_descriptions
        _documentation_load_description "function"
        unset -n __element_descriptions
    fi

    local raw_function function_name known_aliases was_descr
    while read -r raw_function; do
        if [[ -z "$raw_function" ]]; then
            continue
        fi
        function_name=${raw_function:11:${#raw_function}}

        if [[ -n "$filter" ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_EXACT_PREFIX )) -ne 0 ]]; then
                [[ "$function_name" != "$filter" ]] && continue
            else
                [[ "$function_name" != "$filter"* ]] && continue
            fi
        else
            if [[ "$function_name" = "_"* ]] && [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_HIDDEN )) -eq 0 ]]; then
                continue
            fi
        fi

        if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_CONTENT )) -ne 0 ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
                was_descr=0
                if [[ ${_function_descriptions[$function_name]+_} ]]; then
                    echo -e "#\\n# ${_function_descriptions[$function_name]}\\n#"
                    was_descr=1
                fi
                known_aliases=$(_documentation_generate_aliases "$function_name")
                if [[ -n "$known_aliases" ]]; then
                    if [[ $was_descr -eq 0 ]]; then
                        echo "#"
                    fi
                    echo -e "# Known alias(es): $known_aliases\\n#"
                fi
            elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
                if [[ ${_function_descriptions[$function_name]+_} ]]; then
                    continue
                fi
            fi

            if [[ -n "$(command -v pygmentize)" ]]; then
                type "$function_name" | sed '1d' | pygmentize -O 'style=monokai' -l bash
            else
                type "$function_name" | sed '1d'
            fi

            echo
            continue
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_function_descriptions[$function_name]+_} ]]; then
                echo "$function_name: ${_function_descriptions[$function_name]}"
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_function_descriptions[$function_name]+_} ]]; then
                continue
            fi
        fi

        echo "$function_name"
    done < <(declare -F)
}
alias lsfun='lsfunction'


function showfunction {

    if [[ -z "$1" ]]; then
        echo "ERROR: need a varible name">&2
        return 1
    fi

    lsfunction -ce "$1"
}
alias showfun='showfunction'


function lsalias {
    # manage arguments
    local flags filter
    filter=$(_documentation_args "lsvariable" "variables" "$@")
    flags=$?
    [[ $(( flags & __DOCUMENTATION_FLAGS_ERROR )) -ne 0 ]] && return 1

    # load descriptions
    local -A _alias_descriptions
    if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]] || [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
        # shellcheck disable=SC2178
        declare -n __element_descriptions=_alias_descriptions
        _documentation_load_description "alias"
        unset -n __element_descriptions
    fi

    # load descriptions
    local -A _alias_to_descriptions
    if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]] || [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
        # shellcheck disable=SC2178
        declare -n __element_descriptions=_alias_to_descriptions
        _documentation_load_description "alias_to"
        unset -n __element_descriptions
    fi

    local raw_alias alias_name
    while read -r raw_alias; do
        if [[ -z "$raw_alias" ]]; then
            continue
        fi
        alias_name=${raw_alias:6:${#raw_alias}}
        alias_name=${alias_name%%"="*}

        if [[ -n "$filter" ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_EXACT_PREFIX )) -ne 0 ]]; then
                [[ "$alias_name" != "$filter" ]] && continue
            else
                [[ "$alias_name" != "$filter"* ]] && continue
            fi
        else
            if [[ "$alias_name" = "_"* ]] && [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_HIDDEN )) -eq 0 ]]; then
                continue
            fi
        fi

        if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_alias_descriptions[$alias_name]+_} ]]; then
                echo "$alias_name: ${_alias_descriptions[$alias_name]}"
                continue
            fi

            if [[ ${_alias_to_descriptions[$alias_name]+_} ]]; then
                echo "$alias_name: alias to function/script ${_alias_to_descriptions[$alias_name]}"
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
            if [[ ${_alias_descriptions[$alias_name]+_} ]] || [[ ${_alias_to_descriptions[$alias_name]+_} ]]; then
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_CONTENT )) -ne 0 ]]; then
            echo "${raw_alias:6:${#raw_alias}}"
            continue
        fi

        echo "$alias_name"
    done < <(alias)
}


function showalias {
    if [[ -z "$1" ]]; then
        echo "ERROR: need a varible name" >&2
        return 1
    fi

    lsalias -ce "$1"
}


function lsscript {
    # manage arguments
    local flags filter
    filter=$(_documentation_args "lsscript" "scripts" "$@")
    flags=$?
    [[ $(( flags & __DOCUMENTATION_FLAGS_ERROR )) -ne 0 ]] && return 1

    # load descriptions
    local -A _script_descriptions
    # shellcheck disable=SC2178,SC2034
    declare -n __element_descriptions=_script_descriptions
    _documentation_load_description "script"
    unset -n __element_descriptions

    # print scripts
    local script_name description was_descr known_aliases
    for script_name in $(for x in "${!_script_descriptions[@]}"; do echo "$x"; done | sort); do
        if [[ -n "$filter" ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_EXACT_PREFIX )) -ne 0 ]]; then
                [[ "$script_name" != "$filter" ]] && continue
            else
                [[ "$script_name" != "$filter"* ]] && continue
            fi
        else
            if [[ "$script_name" = "_"* ]] && [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_HIDDEN )) -eq 0 ]]; then
                continue
            fi
        fi

        description="${_script_descriptions[$script_name]}"

        if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_CONTENT )) -ne 0 ]]; then
            if [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
                was_descr=0
                if [[ -n "$description" ]]; then
                    echo -e "#\\n# $description\\n#"
                    was_descr=1
                fi

                known_aliases=$(_documentation_generate_aliases "$script_name")
                if [[ -n "$known_aliases" ]]; then
                    if [[ $was_descr -eq 0 ]]; then
                        echo "#"
                    fi
                    echo -e "# Known alias(es): $known_aliases\\n#"
                fi

            elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
                if [[ -n "$description" ]]; then
                    continue
                fi
            fi

            local path
            path="$(command -v "$script_name")"

            if ! [[ -f "$path" ]]; then
                echo "ERROR: script path does not exist: $path" >&2
                continue
            fi

            if [[ -n "$(command -v pygmentize)" ]]; then
                pygmentize -O 'style=monokai' -g "$path"
            else
                cat "$path"
            fi
            continue
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_DESCRIPTION )) -ne 0 ]]; then
            if [[ -n "$description" ]]; then
                echo "$script_name: $description"
                continue
            fi
        elif [[ $(( flags & __DOCUMENTATION_FLAGS_SHOW_WITHOUT_DESCRIPTION )) -ne 0 ]]; then
            if [[ -n "$description" ]]; then
                continue
            fi
        fi

        echo "$script_name"
    done
}


function showscript {
    if [[ -z "$1" ]]; then
        echo "ERROR: need a script name" >&2
        return 1
    fi

    if [[ "$(type "$1")" != "$1 is /"* ]]; then
        echo "ERROR: not a script: $1"
        return 1
    fi

    lsscript -ce "$1"
}

function lostvariable {
    # The goal of the lostvariable function is to build a list of variables
    # that exist in local environment, have not the export flag, and are not
    # Bash variables.
    #
    # With those filters applied, the remaining variables are probably
    # variable that either lack the `local` attribute in functions.  Either
    # lack the `export` attribute.
    #
    # A bash variable is a variable set by the bash interpreter to help.
    # (e.g. $RANDOM)
    #
    # 'printenv' only returns variables that have the export flag set.
    #
    # 'compgen -v' returns every variables existing in current environment
    # with or without export flag set.
    #
    # difference between two minus bash variables should print global
    # variables.

    # collect ALL variable
    local var_name exported_var variable_to_ignore line current_path

    # variables set by bash.  (src: bash manpage)
    current_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    declare -A variable_to_ignore
    while IFS= read -r line; do
        variable_to_ignore["$line"]=1
    done < "$current_path/shell_variables"

    # other variables
    variable_to_ignore["_backup_glob"]=1
    variable_to_ignore["BASH_COMPLETION_VERSINFO"]=1
    variable_to_ignore["__git_printf_supports_v"]=1
    variable_to_ignore["_xspecs"]=1

    # collect exported variables.
    declare -A exported_var
    while read -r var_name; do
        if [[ -z "$var_name" ]]; then
            continue
        fi

        var_name=${var_name%%"="*}
        exported_var["$var_name"]=1
    done < <(printenv | sort)

    # collect all variables
    while read -r var_name; do
        if [[ "$var_name" = "var_name" ]] || [[ "$var_name" = "exported_var" ]] || [[ "$var_name" = "variable_to_ignore" ]] || [[ "$var_name" = "line" ]] || [[ "$var_name" = "current_path" ]]; then
            continue
        fi

        if [[ -z "$var_name" ]]; then
            continue
        fi

        if [[ "$var_name" = "__git_"* ]]; then
            continue
        fi

        if [[ -n "${variable_to_ignore[$var_name]}" ]]; then
            continue
        fi


        if [[ -n "${exported_var[$var_name]}" ]]; then
            continue
        fi

        # Variable named in capital letters only meant to be global.
        if [[ "$var_name" =~ ^[A-Z0-9_]+$ ]]; then
            continue
        fi

        # if [[ "$(declare -p $var_name)" = "declare -a"* ]] || [[ "$(declare -p $var_name)" = "declare -A"* ]]; then
        #     echo "$var_name (array type, can not be exported, so may be normal to appear here)"
        #     continue
        # fi

        echo "$var_name"
    done < <(compgen -v | sort)
}
