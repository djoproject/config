# Introduction
Here is my public personal *nix config.  It set configuration for most of the tools I use with bash.  It also creates bash aliases, functions and variables.

# Installation

WARNING: really few component manage removal so far.  Once installed, removal is manual.

To only patch files:

```bash
user@computer$ ./install
```

To add bash support with a normal installation:

```bash
user@computer$ ./install -b
```

# Removal

```bash
user@computer$ ./removal
```

To remove bash support with a normal installation:

```bash
user@computer$ ./removal -b
```

To completly remove the bash support, need to restart bash.

# Supported plateforms
 * GNU Linux
 * Termux (Android)
 * OS-X

# Components
This section does not fully describes each component but details oddities that each on them has.
Most component creates aliases or functions and set environment variables.

## bash

A lot of bash alias/functions/variables/etc. to help my day to day basis with bash.

## cisco

A few command helper to generate password and clock format for cisco IOS.

## debian-tools

Tools to manage debian package creation and PPA management.

## docker

A few helper to manage local docker operations.

## garmin

Patch udev rules to support correctly a garmin foretrex 601 on Linux.

## git

If a .gitconfig file already exists on the computer, it merges the existing content with the .gitconfig of this repository.  If some keys already exists in the original .gitconfig, they will ber overwritten.

## gns3

A few scripts to easily manage existing projects and device through CLI without having to use the gns3-gui.

## gpg

Scripts and command to naviguate between ssh and gpg agent.

## ipv6

Script to discover other hosts IPv6 address on a LAN-segment.

## latex

Cleaning function for latex compilation.

## lxc

A few help to manage existing LXC instances.

## nextcloud

Functions to manage nextcloud files through cli.

## openvpn

Scripts to manage CA management for clients and servers.

## pass

Few scripts to check configuration and ciphering validity on a shared pass repository.

## path

Script and configuration to help $PATH variable management.

## pluma

Configuration for XFCE pluma text editor.  The configuration is applied through gsettings.

## python

Activate CLI history for python2.x and a lot of scripts to help with python testing.

## screen

Create a screen config file.

## terminator

Create a terminator config file.

## vim

Create a vim config file.

## work

A few function to help context switching while working on different projects at the same time.

# Code quality

## bash scripts

Every bash scripts are [shellcheck](https://www.shellcheck.net/) 0.7.1 compliant.

Every time it is possible, bash scripts use code template from this [list](./bash/template.md)

# TODO

[See related file](./TODO.md)
