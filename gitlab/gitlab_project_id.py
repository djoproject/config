#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import OrderedDict
from configparser import ConfigParser
from logging import DEBUG
from logging import getLogger

from aiofiles import open as async_open
from aiofiles.os import path as async_path

from aiohttp import ClientResponseError
from aiohttp import ClientSession

logger = getLogger(__name__)


DEFAULT_BASE_URL = "https://gitlab.com"
NAMESPACE_ROUTE = "{}/api/v4/namespaces"
NAMESPACE_DETAILS_ROUTE = "{}/api/v4/namespaces/{}"
USERS_ROUTE = "{}/api/v4/users"
USERS_DETAILS_ROUTE = "{}/api/v4/users/{}"
USERS_PROJECTS_ROUTE = "{}/api/v4/users/{}/projects"
GROUPS_ROUTE = "{}/api/v4/groups"
GROUPS_DETAILS_ROUTE = "{}/api/v4/groups/{}"
GROUPS_PROJECTS_ROUTE = "{}/api/v4/groups/{}/projects"
PROJECT_DETAIL_ROUTE = "{}/api/v4/projects/{}"

PROJECT_NAME = 0
NAMESPACE_NAME = 1
NAMESPACE_KIND = 2
NAMESPACE_URL = 3
NAMESPACE_ID = 4
NAMESPACE_KIND_URL = 5
NAMESPACE_KIND_ID = 6
NAMESPACE_KIND_PROJECTS_URL = 7
PROJECT_URL = 8
PROJECT_ID = 9

FACTS_TO_RETRIEVE = {
    "project_name": PROJECT_NAME,
    "namespace_name": NAMESPACE_NAME,
    "namespace_kind": NAMESPACE_KIND,
    "namespace_url": NAMESPACE_URL,
    "namespace_id": NAMESPACE_ID,
    "namespace_kind_url": NAMESPACE_KIND_URL,
    "namespace_kind_id": NAMESPACE_KIND_ID,
    "namespace_kind_project_url": NAMESPACE_KIND_PROJECTS_URL,
    "project_url": PROJECT_URL,
    "project_id": PROJECT_ID,
}


class GitlabProjectIDException(Exception):
    pass


async def find_namespace_and_project_name(path):
    if not await async_path.exists(path):
        exc_msg = "Path does not exist: {}".format(path)
        raise GitlabProjectIDException(exc_msg)

    async with async_open(path, mode='r') as f:
        file_content = await f.read()

    config = ConfigParser()
    config.read_string(file_content)

    if u'remote "origin"' not in config:
        exc_msg = "no part 'remote \"origin\"' in git config file."
        raise GitlabProjectIDException(exc_msg)

    if u'url' not in config[u'remote "origin"']:
        exc_msg = "no sub part 'url' in git config file."
        raise GitlabProjectIDException(exc_msg)

    url = config[u'remote "origin"'][u'url']

    if not url.endswith(".git"):
        exc_msg = "url in git config file does not ends with .git"
        raise GitlabProjectIDException(exc_msg)

    url = url[:-4]

    # https://gitlab.com/NAMESPACE/PROJECT_NAME.git
    if url.startswith("https://"):
        url = url[8:]  # remove https://
        splitted_url = url.split("/")

        if len(splitted_url) < 3:
            exc_msg = "url in config file has a not managed format (1)"
            raise GitlabProjectIDException(exc_msg)

        return splitted_url[-2], splitted_url[-1]

    # git@gitlab.com:NAMESPACE/PROJECT_NAME.git
    else:
        splitted_url = url.split(":")
        if len(splitted_url) != 2:
            exc_msg = "url in config file has a not managed format (2)"
            raise GitlabProjectIDException(exc_msg)

        splitted_url = splitted_url[1].split("/")
        if len(splitted_url) != 2:
            exc_msg = "url in config file has a not managed format (3)"
            raise GitlabProjectIDException(exc_msg)

        return splitted_url[0], splitted_url[1]


async def search(session, url, name, key_to_search):
    params = {
        "search": name,
    }
    async with session.get(url, params=params) as response:
        response.raise_for_status()

        data = await response.json()

    if len(data) == 0:
        exc_msg = "no result returned by url {} with search keyword {}"
        exc_msg = exc_msg.format(url, name)
        raise GitlabProjectIDException(exc_msg)

    for obj in data:
        if key_to_search not in obj:
            exc_msg = "key to search '{}' is not present in payload"
            exc_msg = exc_msg.format(key_to_search)
            raise GitlabProjectIDException(exc_msg)

        if obj[key_to_search] == name:
            return obj

    exc_msg = "no result found on url {} with search keyword {}"
    exc_msg = exc_msg.format(url, name)
    raise GitlabProjectIDException(exc_msg)


async def process_namespace(session, base_url, namespace, facts):
    url = NAMESPACE_ROUTE.format(base_url)
    nspace = await search(session, url, namespace, "path")

    if u"kind" not in nspace:
        exc_msg = "key 'kind' is not present if namespace payload"
        raise GitlabProjectIDException(exc_msg)

    kind = nspace[u"kind"]
    facts["namespace_kind"] = kind

    if u"id" not in nspace:
        exc_msg = "key 'id' is not present if namespace payload"
        raise GitlabProjectIDException(exc_msg)

    nid = nspace[u"id"]
    facts["namespace_id"] = nid

    url = NAMESPACE_DETAILS_ROUTE.format(base_url, nid)
    facts["namespace_url"] = url

    return kind


async def process_user(session, base_url, namespace, facts):
    url = USERS_ROUTE.format(base_url)
    user = await search(session, url, namespace, "username")

    if u"id" not in user:
        exc_msg = "key 'id' is not present if user payload"
        raise GitlabProjectIDException(exc_msg)

    uid = user[u"id"]
    facts["namespace_kind_id"] = uid

    user_details_url = USERS_DETAILS_ROUTE.format(base_url, uid)
    facts["namespace_kind_url"] = user_details_url

    url = USERS_PROJECTS_ROUTE.format(base_url, uid)
    facts["namespace_kind_project_url"] = url

    return url


async def process_group(session, base_url, namespace, facts):
    url = GROUPS_ROUTE.format(base_url)
    group = await search(session, url, namespace, "group")

    if u"id" not in group:
        exc_msg = "key 'id' is not present if group payload"
        raise GitlabProjectIDException(exc_msg)

    gid = group[u"id"]
    facts["namespace_kind_id"] = gid

    group_details_url = GROUPS_DETAILS_ROUTE.format(base_url, gid)
    facts["namespace_kind_url"] = group_details_url

    url = GROUPS_PROJECTS_ROUTE.format(base_url, gid)
    facts["namespace_kind_project_url"] = url

    return url


async def process_project(session, base_url, url, project_name, facts):
    project = await search(session, url, project_name, "name")

    if u"id" not in project:
        exc_msg = "key 'id' is not present if project payload"
        raise GitlabProjectIDException(exc_msg)

    pid = project[u"id"]
    facts["project_id"] = pid

    project_url = PROJECT_DETAIL_ROUTE.format(base_url, pid)
    facts["project_url"] = project_url


async def gather_facts(session, base_url, path, action):
    nspace, project_name = await find_namespace_and_project_name(path)

    facts = OrderedDict()
    facts["namespace_name"] = nspace
    facts["project_name"] = project_name

    if action < NAMESPACE_KIND:
        return facts

    async with ClientSession(headers={"PRIVATE-TOKEN": token}) as session:
        kind = await process_namespace(session, base_url, nspace, facts)

        if action < NAMESPACE_KIND_URL:
            return facts

        if kind == "user":
            url = await process_user(session, base_url, nspace, facts)
        elif kind == "group":
            url = await process_group(session, base_url, nspace, facts)
        else:
            exc_msg = "unknown namespace kind: {}".format(kind)
            raise GitlabProjectIDException(exc_msg)

        if action < PROJECT_URL:
            return facts

        await process_project(session, base_url, url, project_name, facts)
    return facts


async def main(base_url, token, path, action_str):
    action = FACTS_TO_RETRIEVE[action_str]
    try:
        async with ClientSession(headers={"PRIVATE-TOKEN": token}) as session:
            facts = await gather_facts(session, base_url, path, action)
    except ClientResponseError as exc:
        logger.error("{} => {} ".format(exc.status, exc.message))
        return 1
    except GitlabProjectIDException as ex:
        logger.error(str(ex))
        return 1

    if action_str not in facts:
        logger.error("info to retrieve is not present in facts")
        return 1

    if logger.level <= DEBUG:
        logger.debug("Gathered facts:")
        for fname, fvalue in facts.items():
            logger.debug("{}: {}".format(fname, fvalue))

    print(facts[action_str])  # noqa: T201
    return 0


if __name__ == "__main__":
    from sys import version_info
    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from gitlab.args import prepare_parser
    from gitlab.args import parse_args
    from argparse import ArgumentParser

    # Argument parsing
    parser = ArgumentParser()
    prepare_parser(parser)
    parser.add_argument('PATH', nargs='?', default=".git/config",
                        help="git config path (default: .git/config)")
    parser.add_argument('-i', '--info', type=str, default="project_id",
                        choices=FACTS_TO_RETRIEVE.keys(),
                        help='Info to retrieve (default: project_id)')
    args = parser.parse_args()
    url, token = parse_args(args)

    # Let's roll !
    from asyncio import run
    try:
        exit(run(main(url, token, args.PATH, args.info)))
    except Exception:
        logger.exception("unexpected error")

    exit(1)
