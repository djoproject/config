#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from json import dumps
from logging import getLogger

from aiofiles import open as async_open

from aiohttp import ClientSession


logger = getLogger(__name__)

DEFAULT_BASE_URL = "https://gitlab.com"

LINT_ROUTE = "{}/api/v4/ci/lint"


async def main(base_url, token, path):
    headers = {
        "PRIVATE-TOKEN": token,
        "Content-Type": "application/json",
    }

    async with ClientSession(headers=headers) as session:
        url = LINT_ROUTE.format(base_url)

        async with async_open(path, mode='r') as f:
            content = await f.read()

        jcontent = {
            "content": content,
            "include_merged_yaml": False,
            "include_jobs": False,
        }

        async with session.post(url, data=dumps(jcontent)) as response:
            response.raise_for_status()
            data = await response.json()

        printed = False
        for error in data[u"errors"]:
            logger.error(error)
            printed = True

        if printed:
            logger.error("")

        printed = False
        for warning in data[u"warnings"]:
            logger.warning(warning)
            printed = True

        if printed:
            logger.warning("")

        printed = False
        # XXX what's that ?
        for include in data[u"includes"]:
            logger.info(include)
            printed = True

        if printed:
            logger.info("")

        status = "Errors: {}\nWarning: {}\nIncludes: {}\nStatus: {}"
        status = status.format(
            len(data[u"errors"]),
            len(data[u"warnings"]),
            len(data[u"includes"]),
            data[u"status"]
        )
        print(status)  # noqa: T201

    return 0


if __name__ == "__main__":
    from sys import version_info
    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from gitlab.args import prepare_parser
    from gitlab.args import parse_args
    from argparse import ArgumentParser

    description = ("Lint the .gitlab-ci file provided or in the current "
                   "directory")
    parser = ArgumentParser(description=description)
    prepare_parser(parser)
    parser.add_argument(
        'CI_FILE_PATH',
        nargs='?',
        default="./.gitlab-ci.yml",
        help="Path to CI file.  (default: ./.gitlab-ci.yml)"
    )

    args = parser.parse_args()
    url, token = parse_args(args)

    # Let's roll !
    from asyncio import run
    try:
        exit(run(main(url, token, args.CI_FILE_PATH)))
    except Exception:
        logger.exception("unexpected error")

    exit(1)
