#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from logging import DEBUG
from logging import INFO
from logging import basicConfig
from logging import getLogger
from os import environ

DEFAULT_BASE_URL = "https://gitlab.com"
_LOGGER = getLogger()


def prepare_parser(parser):
    parser.add_argument('-u', '--url', type=str,
                        help="API URL (default: {})".format(DEFAULT_BASE_URL))
    parser.add_argument('-t', '--token', type=str, help='API token')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug mode')
    parser.add_argument('-c', '--corporate', action='store_true',
                        help='Use corporate env variables')


def parse_args(args):
    # Configure logging
    if args.debug:
        logging_format = ("[%(asctime)s] %(levelname)s [%(name)s:%(lineno)d] "
                          "%(message)s")
        logging_level = DEBUG
    else:
        logging_format = "[%(asctime)s] %(levelname)s %(message)s"
        logging_level = INFO

    basicConfig(level=logging_level, format=logging_format)

    # Retrieve token
    token = None
    if args.token is not None:
        token = args.token
    elif args.corporate:
        if "GITLAB_CORPORATE_TOKEN" in environ:
            token = environ["GITLAB_CORPORATE_TOKEN"]
    elif "GITLAB_TOKEN" in environ:
        token = environ["GITLAB_TOKEN"]

    if token is None:
        _LOGGER.error("no token provided or found in env variables")
        exit(1)

    # Prepare URL
    url = None
    if args.url is not None:
        url = args.url
    elif args.corporate:
        if "GITLAB_CORPORATE_URL" in environ:
            url = environ["GITLAB_CORPORATE_URL"]
    elif "GITLAB_URL" in environ:
        url = environ["GITLAB_URL"]
    else:
        url = DEFAULT_BASE_URL

    return url, token
