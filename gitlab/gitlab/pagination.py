#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from asyncio import create_task
from asyncio import wait
from logging import getLogger


logger = getLogger(__name__)


class Paginer(object):
    def __init__(self, session, url, process, page_size=100, page_max=None):
        self.session = session
        self.url = url
        self.page_size = page_size
        self.process = process
        self.page_max = page_max

    async def _get_obj_page(self, index):
        params = {
            "page": index,
            "per_page": self.page_size,
        }

        async with self.session.get(self.url, params=params) as response:
            response.raise_for_status()

            data = await response.json()

            for obj in data:
                await self.process(obj)

            return response.headers

    async def _get_all_objs(self, page_count):
        i = 2
        tasks = []

        if self.page_max is not None and self.page_max < page_count:
            page_count = self.page_max

        while i < page_count:
            tasks.append(self._get_obj_page(i))

        await wait(tasks)

    async def _get_next_objs(self, index):
        if self.page_max is not None and index >= self.page_max:
            return

        params = {
            "page": index,
            "per_page": self.page_size,
        }

        async with self.session.get(self.url, params=params) as response:
            response.raise_for_status()

            # starting next page request
            next_index = int(response.headers["x-next-page"])
            next_task = None
            if index < next_index:
                next_cor = self._get_next_objs(next_index)
                next_task = create_task(next_cor)

            # processing
            data = await response.json()

            for obj in data:
                await self.process(obj)

            return next_task

    async def pager(self):
        # Check first page to see if the pagination is known or not
        # If there is more than 10.000 objects, the api won't return header
        # `x-total-pages`.
        headers = await self._get_obj_page(1)

        if "x-total-pages" in headers:
            logger.debug("page count: {}".format(headers["x-total-pages"]))
            total_pages = int(headers["x-total-pages"])

            if total_pages > 1:
                await self._get_all_objs(total_pages)
        else:
            logger.warning("unknown page count")
            next_task = await self._get_next_objs(2)

            while next_task is not None:
                await next_task
                next_task = next_task.result()
