#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from functools import partial
from logging import getLogger

from aiohttp import ClientResponseError
from aiohttp import ClientSession

from gitlab.pagination import Paginer

DEFAULT_BASE_URL = "https://gitlab.com"

GROUP_SEARCH = "{}/api/v4/groups?search={}"
GROUP_PROJECTS_URL = "{}/api/v4/groups/{}/projects"
USER_DETAILS = "{}/api/v4/user"
USER_PROJECTS_URL = "{}/api/v4/users/{}/projects"
PROJECTS_ALL_URL = "{}/api/v4/projects"
PROJECT_LABELS = "{}/api/v4/projects/{}/labels"

logger = getLogger(__name__)


class ProjectListerException(Exception):
    pass


# # URL BUILDER ##############################################################


async def build_url_all(session, base_url):
    return PROJECTS_ALL_URL.format(base_url)


async def build_url_user(session, base_url):
    url = USER_DETAILS.format(base_url)
    async with session.get(url) as response:
        response.raise_for_status()
        data = await response.json()
        return USER_PROJECTS_URL.format(base_url, data["id"])


async def build_url_group(session, base_url, group_name):
    url = GROUP_SEARCH.format(base_url, group_name)
    async with session.get(url) as response:
        response.raise_for_status()
        data = await response.json()

        if len(data) == 0:
            exc_msg = "group named `{}` not found".format(group_name)
            raise ProjectListerException(exc_msg)

        if len(data) > 1:
            exc_msg = "multiple group found with name `{}`, be more specific"
            exc_msg = exc_msg.format(group_name)
            raise ProjectListerException(exc_msg)

        return GROUP_PROJECTS_URL.format(base_url, data["id"])


# # PROJECT FILTER ###########################################################


async def label_filter(session, project_data, base_url, label):
    url = PROJECT_LABELS.format(base_url, project_data["id"])

    async with session.get(url) as response:
        response.raise_for_status()
        data = await response.json()

        for prj_label in data:
            if prj_label["name"] == label:
                return False

        return True


# # MAIN #####################################################################


async def process_project(prj, flter=None, json=False):
    if flter is not None and await flter(project_data=prj):
        return

    if json:
        print(  # noqa: T201
            '    "{}": "{}"'.format(prj["name"], prj["ssh_url_to_repo"])
        )
    else:
        print(  # noqa: T201
            "{}: {}".format(prj["name"], prj["ssh_url_to_repo"])
        )


async def main(
    base_url, token, sub_url_cor, json=False, page_size=100, flter=None
):
    try:
        async with ClientSession(headers={"PRIVATE-TOKEN": token}) as session:
            url = await sub_url_cor(session, base_url)

            if flter is not None:
                flter = partial(flter, session=session)

            process = partial(process_project, flter=flter, json=json)
            paginer = Paginer(session, url, process, page_size)

            if json:
                print("{")  # noqa: T201

            await paginer.pager()

            if json:
                print("}")  # noqa: T201

    except ClientResponseError as exc:
        logger.error("{} => {} ".format(exc.status, exc.message))
        return 1
    except ProjectListerException as exc:
        logger.error("{}".format(exc))
        return 1

    return 0


if __name__ == "__main__":
    from sys import version_info

    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from gitlab.args import prepare_parser
    from gitlab.args import parse_args
    from argparse import ArgumentParser

    parser = ArgumentParser(description="List user personnal gitlab projects")
    prepare_parser(parser)
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help=(
            "Collect all projects available for that user,"
            " not only personnal projects"
        ),
    )
    parser.add_argument(
        "-g", "--group", type=str, help="Collect group projects"
    )
    parser.add_argument(
        "-p",
        "--page_size",
        type=int,
        default=100,
        help="Pagination page size (default: 100)",
    )
    parser.add_argument(
        "-l", "--label", type=str, help="Filter project on label"
    )
    parser.add_argument(
        "-j", "--json", action="store_true", help="Print output in json format"
    )

    args = parser.parse_args()
    url, token = parse_args(args)

    # Select url builder
    if args.all and args.group is not None:
        logger.error("all and group arguments can not be used together.")
        exit(1)

    if args.all:
        sub_url_cor = build_url_all
    elif args.group is not None:
        sub_url_cor = partial(build_url_group, group_name=args.group)
    else:
        sub_url_cor = build_url_user

    # Prepare project filter
    flter = None
    if args.label is not None:
        flter = partial(label_filter, label=args.label, base_url=url)

    # Let's roll !
    from asyncio import run

    try:
        main_cor = main(
            url, token, sub_url_cor, args.json, args.page_size, flter
        )

        exit(run(main_cor))
    except Exception:
        logger.exception("unexpected error")

    exit(1)
