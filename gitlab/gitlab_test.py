#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from logging import getLogger

from aiohttp import ClientSession

logger = getLogger(__name__)

VERSION_ROUTE = "{}/api/v4/version"


async def main(base_url, token):
    async with ClientSession(headers={"PRIVATE-TOKEN": token}) as session:
        url = VERSION_ROUTE.format(base_url)
        async with session.get(url) as response:
            if response.ok:
                log_msg = "{} => {}".format(response.status, response.reason)
                logger.info(log_msg)
            else:
                log_msg = "{} => {}".format(response.status, response.reason)
                logger.error(log_msg)
                return 1

    return 0


if __name__ == "__main__":
    from sys import version_info
    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from argparse import ArgumentParser

    from gitlab.args import prepare_parser
    from gitlab.args import parse_args
    parser = ArgumentParser()
    prepare_parser(parser)

    args = parser.parse_args()
    url, token = parse_args(args)

    # Let's roll !
    from asyncio import run
    try:
        exit(run(main(url, token)))
    except Exception:
        logger.exception("unexpected error")
        exit(1)
