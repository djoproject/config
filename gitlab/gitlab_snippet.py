#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://docs.gitlab.com/ee/api/snippets.html

from json import dumps
from logging import getLogger
from os.path import basename

from aiofiles import open as async_open

from aiohttp import ClientResponseError
from aiohttp import ClientSession

from gitlab.pagination import Paginer

logger = getLogger(__name__)


SNIPPETS_ROUTE = "{}/api/v4/snippets"
SNIPPETS_ID_ROUTE = "{}/api/v4/snippets/{}"
SNIPPETS_PUBLIC_ROUTE = "{}/api/v4/snippets/public"
SNIPPETS_SHOW_ROUTE = "{}/-/snippets/{}/raw/main/{}"


class SnippetsException(Exception):
    pass


async def check_response(response):
    if response.status > 399 and response.status < 500:
        if response.headers[u"Content-Type"] == "application/json":
            data = await response.json()

            if u"error" in data:
                raise SnippetsException(data[u"error"])

            if u"message" in data and u"error" in data[u"message"]:
                raise SnippetsException(data[u"message"][u"error"])

    response.raise_for_status()


async def process_list(snip):
    format_str = "{} {} {} {}".format(
        snip[u"id"],
        snip[u"visibility"],
        snip[u"file_name"],
        snip["web_url"],
    )

    print(format_str)  # noqa: T201


async def list(session, base_url, public):
    if public:
        url = SNIPPETS_PUBLIC_ROUTE.format(base_url)
    else:
        url = SNIPPETS_ROUTE.format(base_url)

    paginer = Paginer(session, url, process_list)
    await paginer.pager()


async def add(session, base_url, file_path, visibility="public", descr=None):
    base = basename(file_path)

    async with async_open(file_path) as file:
        content = await file.read()

    if len(content) == 0:
        raise SnippetsException("empty file not allowed.")

    if descr is None or len(descr) == 0:
        descr = base

    data = {
        "title": base,
        "description": descr,
        "visibility": visibility,
        "files": [
            {
                "file_path": base,
                "content": content,
            },
        ],
    }

    url = SNIPPETS_ROUTE.format(base_url)
    async with session.post(url, data=dumps(data)) as response:
        await check_response(response)
        data = await response.json()
        print(data["web_url"])  # noqa: T201


async def remove(session, base_url, sid):
    url = SNIPPETS_ID_ROUTE.format(base_url, sid)
    async with session.delete(url) as response:
        await check_response(response)


# TODO should it allow to change visibility ? is it even possible ?
# TODO update description ?
async def update(session, base_url, sid, file_path):
    url = SNIPPETS_ID_ROUTE.format(base_url, sid)

    async with session.get(url) as response:
        await check_response(response)
        data = await response.json()

    if len(data[u"files"]) > 1:
        raise SnippetsException("Multi files snippets, no supported.")

    old_name = data[u"files"][0]["path"]
    base = basename(file_path)

    async with async_open(file_path) as file:
        content = await file.read()

    if len(content) == 0:
        raise SnippetsException("empty file not allowed.")

    # Change file content
    data = {
        "id": sid,
        "files": [
            {
                "action": "update",
                "file_path": old_name,
                "content": content,
            }
        ]
    }

    # Change file name
    if old_name != base:
        data[u"files"].append({
            "action": "move",
            "previous_path": old_name,
            "file_path": base,
        })

    async with session.put(url, data=dumps(data)) as response:
        await check_response(response)


async def show(session, base_url, sid):
    url = SNIPPETS_ID_ROUTE.format(base_url, sid)
    async with session.get(url) as response:
        response.raise_for_status()
        data = await response.json()

    url = SNIPPETS_SHOW_ROUTE.format(base_url, sid, data[u"file_name"])
    async with session.get(url) as response:
        await check_response(response)
        content = await response.text()
        print(content)  # noqa: T201


async def main(url, token, action, public, id, path, visibility, description):
    headers = {
        "PRIVATE-TOKEN": token,
    }

    if action in ("add", "update",):
        headers[u"Content-Type"] = "application/json"

    try:
        async with ClientSession(headers=headers) as session:
            if action == "list":
                await list(session, url, public)
            elif action == "show":
                await show(session, url, id)
            elif action == "add":
                await add(session, url, path, visibility, description)
            elif action == "remove":
                await remove(session, url, id)
            elif action == "update":
                await update(session, url, id, path)
            else:
                logger.error("unknown action: {}".format(action))
                return 1
    except ClientResponseError as ex:
        logger.error("{} => {}".format(ex.status, ex.message))
        return 1
    except SnippetsException as ex:
        logger.error(str(ex))
        return 1

    return 0


if __name__ == "__main__":
    from sys import version_info
    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from gitlab.args import prepare_parser
    from gitlab.args import parse_args
    from argparse import ArgumentParser

    parser = ArgumentParser(description="Manage user gitlab snippets")
    prepare_parser(parser)
    parser.add_argument('ACTION', nargs='?', default="list",
                        choices=("list", "remove", "add", "show", "update",),
                        help="Action to do (default: list)")
    parser.add_argument('-p', '--public', action='store_true',
                        help="Retrieve public snippets")
    parser.add_argument('-i', '--id', type=int,
                        help="Snippet ID")
    parser.add_argument('-f', '--file_path', type=str,
                        help="File path to use for create or update")
    parser.add_argument('-v', '--visibility', type=str,
                        choices=("public", "internal", "private",),
                        default="public",
                        help="Visibility to use during creation")
    parser.add_argument('-e', '--description', type=str,
                        help="Description to use during creation")

    args = parser.parse_args()
    url, token = parse_args(args)

    # Process id and path
    if args.ACTION in ("show", "remove", "update") and args.id is None:
        logger.error("need an id for that action")
        exit(1)

    if args.ACTION in ("add", "update",) and args.file_path is None:
        logger.error("need a file path for that action")
        exit(1)

    # Let's roll !
    from asyncio import run
    try:
        exit(
            run(
                main(
                    url,
                    token,
                    args.ACTION,
                    args.public,
                    args.id,
                    args.file_path,
                    args.visibility,
                    args.description)))
    except Exception:
        logger.exception("unexpected error")
        exit(1)
