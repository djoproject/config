#!/usr/bin/env bash

# Copyright (C) 2020  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function latex_clear { 
    find . -name "*pdf" -exec rm -f {} \; 2> /dev/null;
    find . -name "*log" -exec rm -f {} \; 2> /dev/null;
    find . -name "*out" -exec rm -f {} \; 2> /dev/null;
    find . -name "*aux" -exec rm -f {} \; 2> /dev/null;
    find . -name "*blg" -exec rm -f {} \; 2> /dev/null;
    find . -name "*bbl" -exec rm -f {} \; 2> /dev/null;
    find . -name "*synctex.gz" -exec rm -f {} \; 2> /dev/null;
}
alias lclear="latex_clear"
