-- ### Built-in configuration ####################################################################

-- Enable syntax highlighning
vim.opt.syntax = 'on'

-- Disable visual mode on mouse selection
vim.opt.mouse=""

-- Highligh search
vim.opt.hlsearch = true

-- The width of a TAB is set to 4.
vim.opt.tabstop=4

-- Indents will have a width of 4
vim.opt.shiftwidth=4

-- Sets the number of columns for a TAB
vim.opt.softtabstop=4

-- Expand TABs to spaces
vim.opt.expandtab = true

-- Allow hidden buffer with not yet saved modification
vim.opt.hidden = true

-- Set lin length marker
vim.opt.colorcolumn = '100'

-- Print line number
vim.opt.number = true

-- Disable the swap files
vim.opt.swapfile = false

-- ### NVIM colorscheme settings #################################################################

-- Remove color limitation for colorscheme
vim.opt.termguicolors = false

-- Set the colorscheme
vim.cmd 'colorscheme vim'

-- With 'vim' colorscheme the signcolumn is grey, this line put it back to transparent
vim.cmd 'highlight! link SignColumn LineNr'

-- ### junegunn/vim-plug (Plugin manager) ########################################################
-- src: https://github.com/junegunn/vim-plug
--
-- INSTALLATION:
-- sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
-- https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
--
-- Then in nvim execute this command: PlugInstall
--
local vim = vim
local Plug = vim.fn['plug#']

vim.call('plug#begin')

Plug("nvim-lua/plenary.nvim") -- XXX ?
Plug("Shatur/neovim-session-manager")  -- manage the sessions

Plug("tpope/vim-fugitive")  -- git helpers
Plug("itchyny/lightline.vim")  -- print the bottom bar status

Plug("rust-lang/rust")  -- rust helpers

Plug("neovim/nvim-lspconfig") -- LSP helpers
Plug("hrsh7th/cmp-nvim-lsp")  -- XXX ?
Plug("hrsh7th/cmp-vsnip")  -- XXX ?
Plug("hrsh7th/cmp-path")  -- XXX ?
Plug("hrsh7th/cmp-buffer")  -- XXX ?
Plug("hrsh7th/nvim-cmp")  -- XXX ?

Plug("fatih/vim-go", {['do'] = ":GoUpdateBinaries"})

vim.call('plug#end')

-- ### LSP configuration #########################################################################
-- Inspired from: https://sharksforarms.dev/posts/neovim-rust/

local lspconfig = require('lspconfig')
lspconfig.rust_analyzer.setup {
    settings = {
        ['rust-analyzer'] = {
            -- Doc: https://rust-analyzer.github.io/manual.html#configuration
            cargo = {
                --- this setting forces to use a different target directory than anything else.
                --- It avoids rebuilding the cache too often.
                targetDir = './target-rust-analyser'
            },
            checkOnSave = true,
            check = {
                command = "clippy"
            }
        }
    }
}

lspconfig.pyright.setup {
    typeCheckingMode = 'strict'
}

lspconfig.gopls.setup({
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
      gofumpt = true,
    },
  },
})

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Disable token highlighning, it fucks up the colorscheme
    local client = vim.lsp.get_client_by_id(ev.data.client_id)
    client.server_capabilities.semanticTokensProvider = nil

    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})

-- Make the signcolumn always visible left of the line number, that's the line printing H, W, E, etc.
-- without that, the code move horizontaly every time a sign appear or disappear
vim.opt.signcolumn="yes"


-- ### Completion configuration ##################################################################

vim.opt.completeopt = "menuone,noinsert,noselect"
vim.opt.shortmess = vim.opt.shortmess + "c"

-- Setup Completion
-- See https://github.com/hrsh7th/nvim-cmp#basic-configuration
local cmp = require("cmp")
cmp.setup({
  preselect = cmp.PreselectMode.None,
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ["<C-p>"] = cmp.mapping.select_prev_item(),
    ["<C-n>"] = cmp.mapping.select_next_item(),
    -- Add tab support
    ["<S-Tab>"] = cmp.mapping.select_prev_item(),
    ["<Tab>"] = cmp.mapping.select_next_item(),
    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-Space>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.close(),
    ["<CR>"] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    }),
  },

  -- Installed sources
  sources = {
    { name = "nvim_lsp" },
    { name = "vsnip" },
    { name = "path" },
    { name = "buffer" },
  },
})

-- ### Shatur/neovim-session-manager (Session manager) ###########################################

local Path = require('plenary.path')
local config = require('session_manager.config')
require('session_manager').setup({
  -- The directory where the session files will be saved.
  sessions_dir = Path:new(vim.fn.stdpath('data'), 'sessions'),
  -- Function that replaces symbols into separators and colons to transform filename into a session directory.
  session_filename_to_dir = session_filename_to_dir,
  -- Function that replaces separators and colons into special symbols to transform session directory into a filename. Should use `vim.loop.cwd()` if the passed `dir` is `nil`.
  dir_to_session_filename = dir_to_session_filename,
  -- Define what to do when Neovim is started without arguments. Possible values: Disabled, CurrentDir, LastSession
  autoload_mode = config.AutoloadMode.CurrentDir,
  -- Automatically save last session on exit and on session switch.
  autosave_last_session = true,
  -- Plugin will not save a session when no buffers are opened, or all of them aren't writable or listed.
  autosave_ignore_not_normal = false,
  autosave_ignore_dirs = {
    '/dev/shm/',  --password-store edtion
    '.ansible/tmp/',  --ansible vault edition
  },
  -- All buffers of these file types will be closed before the session is saved.
  autosave_ignore_filetypes = {
    'gitcommit',
    'gitrebase',
  },
  -- All buffers of these bufer types will be closed before the session is saved.
  autosave_ignore_buftypes = {},
  -- Always autosaves session. If true, only autosaves after a session is active.
  autosave_only_in_session = false,
  -- Shorten the display path if length exceeds this threshold. Use 0 if don't want to shorten the path at all.
  max_path_length = 80,
})

-- ### itchyny/lightline.vim (Status bar at the bottom) ##########################################

-- Enable light line
vim.opt.laststatus = 2

-- Remove show mode to avoid duplicated information with lightline
vim.g.noshowmode = true

-- Integrate fugitive into lightline
vim.g.lightline = {
    colorscheme = 'wombat',
    active = {
        left = {{ 'mode', 'paste' }, { 'gitbranch', 'readonly', 'relativepath', 'modified' }}
    },
    component_function = {
        gitbranch = 'FugitiveHead'
    }
}

-- ###  rust-lang/rust.vim (Rust helpers) ########################################################

-- Run rustfmt on the buffer when saving
vim.g.rustfmt_autosave = 1
