#!/usr/bin/env bash

# Copyright (C) 2021  Jonathan Delvaux <config@djoproject.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function go_test (){
    if [[ -z "$(command -v go)" ]]; then
        echo "WARNING: go does not seem to be installed."
        return 0
    fi

    if ! go test -cover ./... "$@"; then
        return 1
    fi
}
alias go_t="go_test"

function go_test_verbose (){
    if [[ -z "$(command -v go)" ]]; then
        echo "WARNING: go does not seem to be installed."
        return 0
    fi

    if ! go test -v -cover ./... "$@"; then
        return 1
    fi
}
alias go_tv="go_test_verbose"

function go_test_cover (){
    if [[ -z "$(command -v go)" ]]; then
        echo "WARNING: go does not seem to be installed."
        return 0
    fi

    if ! go test -coverprofile=/tmp/c.out ./... "$@"; then
        rm /tmp/c.out 2> /dev/null
        return 1
    fi

    echo -e "\nRemaining statements in functions: "
    go tool cover -func=/tmp/c.out | grep -v "100.0%$"
    rm /tmp/c.out 2> /dev/null
}
alias go_tc="go_test_cover"

function go_test_uncover () {
    if [[ -z "$(command -v go)" ]]; then
        echo "WARNING: go does not seem to be installed."
        return 0
    fi

    if [[ -z "$(command -v uncover)" ]]; then
        echo "WARNING: uncover does not seem to be installed."
        return 0
    fi

    if ! go test -coverprofile=/tmp/c.out ./... "$@"; then
        rm /tmp/c.out 2> /dev/null
        return 1
    fi

    uncover /tmp/c.out
    rm /tmp/c.out 2> /dev/null
}
alias go_tu="go_test_uncover"

function go_lint () {
    if [[ -z "$(command -v golangci-lint)" ]]; then
        echo "WARNING: golangci-lint does not seem to be installed."
        return 0
    fi

    golangci-lint run
}
alias go_l="go_lint"

function go_build () {
    if [[ -z "$(command -v go)" ]]; then
        echo "WARNING: go does not seem to be installed."
        return 0
    fi

    GO111MODULE=off go build
}
alias go_b="go_build"
